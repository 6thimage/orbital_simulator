#include <stdio.h>

#define TEST "Float Test"
#define VERSION "1.0"

int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[])
{
    float af=6378136.49;
    double ad=6378136.49;
    float rff=298.25645;
    double rfd=298.25645;

    puts(TEST " " VERSION "\n");

    puts(
    "This is a test program used to verify whether floats keep the required\n"
    "accuracy. The following lines output the ITRF-96 constants when they\n"
    "are stored in floats(f), doubles(d) and strings(s), for comparison.\n"
    "Floats can be seen to reduce the accuracy of the constants via rounding\n");
    printf("[f] a: %.18f rf: %.18f\n", af, rff);
    printf("[d] a: %.18f rf: %.18f\n", ad, rfd);
    puts("[s] a: 6378136.49                 rf: 298.25645");
    return 0;
}

