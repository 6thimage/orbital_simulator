#include <stdio.h>
#include <stdlib.h>

#include "../src/tle.h"
#include "../src/date.h"
#include "../src/sgp4/sgp4unit.h"

#define TEST "SGP4 Test"
#define VERSION "1.0"

int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[])
{
    tle_entry tle[1];
    sgp4_work work;
    double ro[3], vo[3];
    int i;
    FILE *f;

    puts(TEST " " VERSION "\n");

    puts("**** SGP4 verification test ****\n"
         "Verifies output of SGP4 for test case.");

    /* load tle */
    tle_parse("data/verification.tle", tle, 1);

    /* init sgp4
     * sgp4init expects epoch from jan 0, 1950, 0hr (2433281.5 in Julian)
     * so we correct for this in the function call.
     */
    sgp4init(wgs72, 'a', &tle[0], &work);

    /* open file */
    f=fopen("sgp4.dat", "w");
    if(!f)
    {
        puts("FAIL - cannot open output data file");
        return -1;
    }

    /* generate and write output */
    for(i=-144; i<145; ++i)
    {
        sgp4(&tle[0], &work, i*10., ro, vo);
        fprintf(f, "%16.8f %16.8f %16.8f %16.8f %12.9f %12.9f %12.9f\n", i*10.,
                ro[0], ro[1], ro[2], vo[0], vo[1], vo[2]);
    }

    /* close file */
    fclose(f);
    
    /* verify output */
    i=system("diff -q data/verification.dat sgp4.dat");
    if(!i)
        puts("PASSED");
    else
    {
        puts("FAIL - output does not match known output");
        return -1;
    }

    return 0;
}

