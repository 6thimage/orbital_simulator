#include <stdio.h>

#include "../src/date.h"
#include "../src/coords.h"
#include "../src/nutation_data.h"

#define TEST "ECI Test"
#define VERSION "1.0"

int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[])
{
    int ret=0;
    unsigned long len;

    timedate td;
    double jd, tt;
    double zeta, theta, z;
    double epsilon_bar, delta_psi, epsilon;

    puts(TEST " " VERSION "\n");

    puts("**** nutation data check ****\n"
         "Checks the number of nutation constants.\n");

    len=sizeof(nutation_multipliers)/sizeof(int);
    printf("nutation multipliers: %ld\n", len);
    if(len!=(106*5))
        --ret;

    len=sizeof(nutation_longitude)/sizeof(double);
    printf("nutation longitude: %ld\n", len);
    if(len!=(106*2))
        --ret;

    len=sizeof(nutation_obliquity)/sizeof(double);
    printf("nutation obliquity: %ld\n", len);
    if(len!=(106*2))
        --ret;

    if(ret!=0)
    {
        puts("FAIL - invalid constant lengths");
        return ret;
    }
    else
        puts("PASSED");

    return 0;

    days_hms(2000, 179.78495062, &td);
    jd=julian_days(&td);
    tt=julian_centuries(jd);

    printf("epoch %02d%12.8f -- %02d/%02d/%04d %02d:%02d:%6.3f\n",
           0, 179.78495062, td.day, td.month, td.year, td.hour, td.minute, td.sec);
    printf("JD: %f  TT: %f\n", jd, tt);

    /*td.year=2004; td.day=6; td.month=4;
    td.hour=7; td.minute=51; td.sec=28.386009;
    jd=julian_days(&td);
    tt=julian_centuries(jd);

    printf("epoch %02d%12.8f -- %02d/%02d/%02d %02d:%02d:%6.3f\n",
           0, 179.78495062, td.day, td.month, td.year, td.hour, td.minute, td.sec);
    printf("JD: %f  TT: %f\n", jd, tt);*/

    precession_angles(tt, &zeta, &theta, &z);
    zeta=COORD_RAD_TO_DEG(zeta);
    theta=COORD_RAD_TO_DEG(theta);
    z=COORD_RAD_TO_DEG(z);
    printf("Precession zeta: %e theta: %e z: %e\n", zeta, theta, z);

    nutation_angles(tt, 0., 0., &epsilon_bar, &delta_psi, &epsilon);
    epsilon_bar=COORD_RAD_TO_DEG(epsilon_bar);
    delta_psi=COORD_RAD_TO_DEG(delta_psi);
    epsilon=COORD_RAD_TO_DEG(epsilon);
    printf("Nutation epsilon_bar: %e delta_psi: %e epsilon: %e\n", epsilon_bar, delta_psi, epsilon);

    return 0;
}

