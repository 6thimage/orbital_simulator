#include <stdio.h>
#define __USE_BSD
#include <math.h>

#include "../src/coords.h"

#define TEST "ECEF Test"
#define VERSION "1.0"

int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[])
{
    double lla[3], ecef[3];
    int ret;
    
    puts(TEST " " VERSION "\n");

    puts("**** LLA to ECEF test ****\n"
         "Tests coord_lla_to_ecef against known values to mm precision.\n");

    puts("Input <Leicester engineering building>\n"
         "lat: 52.620169 deg, long: -1.123551 deg, alt: 20 m");

    lla[0]=COORD_DEG_TO_RAD(52.620169);
    lla[1]=COORD_DEG_TO_RAD(-1.123551);
    lla[2]=20;

    do
    {
        /*** WGS-84 ***/
        puts("[WGS-84]");
        ret=coord_lla_to_ecef(lla, ecef, ECEF_WGS84);
        if(!ret)
        {
            printf("x: %24.18f km\n", ecef[0]/1000.);
            printf("y: %24.18f km\n", ecef[1]/1000.);
            printf("z: %24.18f km\n", ecef[2]/1000.);

            /* test against known values
             * from http://www.oc.nps.edu/oc2902w/coord/llhxyz.htm
             * x=3879.618 km, y=-76.088 km, z=5045.01 km
             *
             * extended precision from first run
             */
            if((round(ecef[0]*1000)!=3879618473)||
                    (round(ecef[1]*1000)!=-76087769)||
                    (round(ecef[2]*1000)!=5045009803))
            {
                puts("FAIL - Calculation does not match known values");
                ret=-1;
                break;
            }
            else
                puts("PASSED");
        }
        else
        {
            printf("FAIL - ret=%d\n", ret);
            break;
        }
        
        /*** ITRF-96 ***/
        puts("[ITRF-96]");
        ret=coord_lla_to_ecef(lla, ecef, ECEF_ITRF96);
        if(!ret)
        {
            printf("x: %24.18f km\n", ecef[0]/1000.);
            printf("y: %24.18f km\n", ecef[1]/1000.);
            printf("z: %24.18f km\n", ecef[2]/1000.);

            /* test against known values - currently not externally sourced,
             * based on first run
             */
            if((round(ecef[0]*1000)!=3879618184)||
                    (round(ecef[1]*1000)!=-76087763)||
                    (round(ecef[2]*1000)!=5045009339))
            {
                puts("FAIL - Calculation does not match known values");
                ret=-1;
                break;
            }
            else
                puts("PASSED");

        }
        else
        {
            printf("FAIL - ret=%d\n", ret);
            break;
        }

    }while(0);

    if(ret!=0)
        return ret;

    puts("\n**** ECEF to LLA test ****\n"
         "Tests coord_ecef_to_lla using known values (for Leicester engineering building).\n");

    ecef[0]=3879618.472548108547925949096679687500;
    ecef[1]=-76087.768872545813792385160923004150;
    ecef[2]=5045009.802758952602744102478027343750;

    do
    {
        ret=coord_ecef_to_lla(ecef, lla, ECEF_WGS84);
        if(!ret)
        {
            printf("lat:  %23.18f deg\n", COORD_RAD_TO_DEG(lla[0]));
            printf("long: %23.18f deg\n", COORD_RAD_TO_DEG(lla[1]));
            printf("alt:  %23.18f m\n", lla[2]);

            /* test output */
            if((round(lla[0]*10000000)!=9183952)||
                    (round(lla[1]*10000000)!=-196097)||
                    (round(lla[2]*1000)!=20000))
            {
                puts("FAIL - Calculation does not match known values");
                ret=-1;
                break;
            }
            else
                puts("PASSED");
        }
        else
        {
            printf("FAIL - ret %d\n", ret);
            break;
        }

    }while(0);

    return ret;
}

