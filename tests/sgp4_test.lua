#! ../orb_sim -f

print('SGP4 Test 1.0 (lua)\n')
print('**** SGP4 verification test ****')
print('Verifies output of SGP4 for test case.')

-- read in tle
t=tle.parse('data/verification.tle')
work=sgp4_work.new()

-- init sgp4
sgp4.init(sgp4.WGS72, t[1], work)

-- open output
f=io.open('sgp4.dat','w')
if(not f) then
    print('FAIL - cannot open output data file')
    os.exit(-1, true)
end

-- loop over range
for i=-1440, 1440, 10 do
    r,v=sgp4.single(t[1], work, i)
    fprintf(f,"%16.8f %16.8f %16.8f %16.8f %12.9f %12.9f %12.9f\n",
        i, r.x, r.y, r.z, v.x, v.y, v.z)
end
f:close()

-- verify output - os.execute is different to system in C, execute
-- returns 3 values:
--     first - true if command is successful, otherwise nil
--     second - either exit or signal, depending on what caused the
--              command to return
--     third - exit status or number of termination signal
i=os.execute("diff -q data/verification.dat sgp4.dat")
if(i) then
    print('PASSED')
    os.exit(0, true)
else
    print('FAIL - output does not match known output')
    os.exit(-1, true)
end
