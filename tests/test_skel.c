#include <stdio.h>

#define TEST "Test"
#define VERSION "1.0"

int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[])
{
    puts(TEST " " VERSION "\n");

    return 0;
}

