#! /usr/bin/lua

lfs=require("lfs") -- lua file system

-- add names of files in data to satellites
satellites={}
for file in lfs.dir('data/tle') do
    if(string.sub(file,0,1)~=".") then
        table.insert(satellites, file)
    end
end

table.sort(satellites) -- sort satellites alphabetically

-- print table
for i,s in pairs(satellites) do
    print(i,s)
end
