kepler_lua=require('kepler_lua')

-- read in tles
t=tle.parse('data/tle/ISS (ZARYA)')

file=io.open('iss_kep.dat','w')

for time=0,92,1 do
--for _,time in pairs({459.909849}) do
--for _,time in pairs({0,92.662511}) do
    -- run propagator
    r, rdot=kepler_lua.single(t[2],time)

    -- print outputs
    fprintf(file, "%16.8f %16.8f %16.8f\n", r.x, r.y, r.z)
    --print(r.x, r.y, r.z, math.sqrt(r.x^2 +r.y^2 +r.z^2))
    --print(rdot.x, rdot.y, rdot.z, math.sqrt(rdot.x^2 +rdot.y^2 +rdot.z^2))
end

file:close()
