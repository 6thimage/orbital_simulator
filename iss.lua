function radius(p)
    local r=p.x*p.x +p.y*p.y +p.z*p.z
    return math.sqrt(r)
end

function period(r)
    local t=r*1000 -- r in m
    t=t^3 -- r^3
    t=t*4*math.pi^2 -- 4*pi^2*r^3
    t=t/(6.67e-11*5.972e24) -- (4*pi^2*r^3)/GM
    return math.sqrt(t)
end

-- read tles
t=tle.parse('data/ISS (ZARYA)')
--t=tle.parse('tle_iss_dragless')
work=sgp4_work.new()

-- calculate sgp4 diff
sgp4.init(sgp4.WGS72, t[1], work)
date_diff=jdate_to_unix(t[2].epoch)-jdate_to_unix(t[1].epoch)
r1,v1 = sgp4.single(t[1], work, date_diff/60)

sgp4.init(sgp4.WGS72, t[2], work)
r2,v2 = sgp4.single(t[2], work, 0)

printf('%f s (%f mins)\n', date_diff, date_diff/60)
printf('t1 @t2 epoch   % 12.6f % 12.6f % 12.6f\n', r1.x, r1.y, r1.z)
printf('t2             % 12.6f % 12.6f % 12.6f\n', r2.x, r2.y, r2.z)

r1.r=radius(r1)
r2.r=radius(r2)
r1.p=period(r1.r)
r2.p=period(r2.r)
printf('t1 radius % 12.6f period %f s (%f mins)\n', r1.r, r1.p, r1.p/60)
printf('t2 radius % 12.6f period %f s (%f mins)\n', r2.r, r2.p, r2.p/60)

r2,v2 = sgp4.single(t[2], work, r2.p/60)
printf('t2 @ +1 period % 12.6f % 12.6f % 12.6f\n', r2.x, r2.y, r2.z)

-- print to file 200 mins around epoch
f=io.open('iss.dat', 'w')
for i=0,92,1 do
    r2,v=sgp4.single(t[2], work, i)
    fprintf(f, "%16.8f %16.8f %16.8f\n", r2.x, r2.y, r2.z)
end
f:close()
