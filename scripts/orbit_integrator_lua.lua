vec3=require('vec3')

orbit_integrator_lua={}

local mu=398600.8 -- km^3/s^2
local J2=0.001082616
local R=6378.135 -- km
local earth_rotation=7.292115e-5 -- rad/s

local function orbit_integrator_lua.accel(pos)
    local r2=pos.x^2 +pos.y^2 +pos.z^2
    local r=math.sqrt(r2)
    local r3=r2^(3/2)
    a=vec3.new()

    -- gravity
    local grav_const=mu/r3
    a.x=-grav_const*pos.x
    a.y=-grav_const*pos.y
    a.z=-grav_const*pos.z

    return a
end

-- Runge-Kutta integration
local function orbit_integrator_lua.integrate(pos, vel, step_size)
    local ka={}
    local kv={}

    -- first step
    kv[1]=vel
    ka[1]=accel(pos)

    -- second step
    kv[2]=vel + (step_size/2)*ka[1]
    ka[2]=accel(pos + (step_size/2)*kv[1])

    -- third step
    kv[3]=vel + (step_size/2)*ka[2]
    ka[3]=accel(pos + (step_size/2)*kv[2])

    -- fourth step
    kv[4]=vel + (step_size/2)*ka[3]
    ka[4]=accel(pos + (step_size/2)*kv[3])

    local vel_next=vel + step_size*((ka[1] + ka[4])/6 + (ka[2] + ka[3])/3)
    local pos_next=pos + step_size*((kv[1] + kv[4])/6 + (kv[2] + kv[3])/3)

    return pos_next, vel_next
end

function orbit_integrator_lua.run(pos, vel, time, step_size)
    -- check arguments
    if not vec3.is_vec3(pos) then
        pos=vec3.new(pos.x, pos.y, pos.z)
    end
    if not vec3.is_vec3(vel) then
        vel=vec3.new(vel.x, vel.y, vel.z)
    end

    local time_sec=time*60

    local p=pos
    local v=vel

    for i=0, time_sec, step_size do
        p, v=integrate(p, v, step_size)
    end

    return p, v
end

return orbit_integrator_lua
