local vec3={}
local vec3.vec3_mt={}

function vec3.vec3_mt.__tostring(a)
    return 'vec3('..a.x..','..a.y..','..a.z..')'
end

function vec3.vec3_mt.__add(a, b)
    return new(a.x + b.x, a.y + b.y, a.z + b.z)
end

function vec3.vec3_mt.__mul(a, b)
    if type(a)==type(b) then
        error("vec3: can't multiply two vectors")
    end
    local t, const
    if type(a)=='table' then
        t=a
        const=b
    else
        t=b
        const=a
    end
    return new(const*t.x, const*t.y, const*t.z)
end

function vec3.vec3_mt.__div(a, b)
    if type(b)=='table' then
        error("vec3: can't divide by vector")
    end
    return new(a.x/b, a.y/b, a.z/b)
end

function vec3.new(_x, _y, _z)
    r={x=_x or 0, y=_y or 0, z=_z or 0}
    setmetatable(r, vec3_mt)
    return r
end

function vec3.is_vec3(o)
    if getmetatable(o)==vec3_mt then
        return true
    else
        return false
    end
end

return vec3
