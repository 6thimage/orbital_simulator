local rot=require('rotations')

local kepler_lua={}

function kepler_lua.single(tle, time)

    -- calculate a
    local mu=398600.8 -- wgs72
    --local mu=398600.79964 -- wgs72old
    --local mu=398600.5 -- wgs84
    -- mean motion is in rads/min,[ needs to be in rads/sec]
    local a_sgp=(mu/((tle.mean_motion/60)^2))^(1/3) -- a= (mu/ n^2)^1/3

    --[[ corrections from spacetrack report 3
    -- J_2 from wgs72
    -- delta is delta_1 and delta_0, without the a_1^2 and a_0^2, respectively
    local delta = (0.001082616*(6378.135^2))/2 -- k_2 = (J_2 *a_E^2)/2
    delta=(delta*3*(3*(math.cos(tle.incl)^2)-1))/(2*(1-tle.ecc*tle.ecc)^(3/2)) -- (3*K_2*(3*math.cos(i)^2 -1))/(2*(1-e^2)^(3/2))
    local delta_1=delta/(a_sgp*a_sgp) -- delta/(a_1^2)
    local a_0=a_sgp*(1 - (delta_1/3) - (delta_1^2) - ((134*(delta_1^3))/(81))) -- a_1*(1 - delta_1/3 - delta_1^2 - 134*delta_1^3/81)
    local delta_0=delta/(a_0*a_0) -- delta/(a_0^2)
    local mean_motion=tle.mean_motion/(1+delta_0) -- n_0/(1 + delta_0)
    local a=a_0/(1-delta_0) -- a_0/(1 - delta_0) --]]

    --local mean_motion=tle.mean_motion
	--local mean_motion=math.sqrt(mu/a_sgp^3) -- classical calculation of mean motion (mu/a^3)
    local a=a_sgp

    local ecc=tle.ecc
    local incl=tle.incl
    local perigee=tle.perigee
    local raan=tle.raan
    local mean_motion=tle.mean_motion

    -- wgs72 constants
    local R=6378.135 -- in km
    local J2=0.001082616
    local J3=-0.00000253881
    local J4=-0.00000165597

    local A2=J2*R^2 -- McCuskey pg 171
    local A3=J3*R^3
    local A4=-(3/8)*J4*R^4 -- factor infront likely to be wrong

    -- from 'The motion of a close earth satellite' - Kozai, Astro Journal Nov. 1959
    -- delta_a is of the order of 1e-4 and reduces the positional error
    -- on the xy plane, the error is reduced for the majority from -8 to 8 to -4 to 4
    -- in the z axis, the error is reduced from -16 to -4 to -15 to -7, which is a much more
    -- significant improvement
    --local p=a*(1 - ecc^2)
    --local delta_a=(A2/p^2)*(1 - (3/2)*math.sin(incl)^2)*math.sqrt(1 - ecc^2)
    --a=a*(1 - delta_a)

    -- mean motion correction is currently left out as it doesn't effect the results at epoch
    --[[local delta_mean_motion=(A2/p^2)*(1 - (3/2)*math.sin(incl)^2)*math.sqrt(1 - ecc^2)
    local mean_motion=mean_motion*(1 + delta_mean_motion)--]]
    local mean_motion=tle.mean_motion

    -- calculate mean anomaly
    local M=tle.mean_anom+(mean_motion*time) -- M=M_0 +n*t assumed that M_0 is at t=0

    --[[ Kaula
    local neta=math.sqrt(1 - ecc^2)
    local theta=math.cos(incl)
    local y=(R^2 * J2)/(2* a^2 * neta^4)
    local mean_anom_delta=((y * neta^3)/8)*(1 -11*theta^2 -40*(theta^4)/(1 -5*theta^2))*math.sin(2*perigee)
    local perigee_delta=-(y/16)*((2+ ecc^2) -((2+ 3*ecc^2)*11*theta^2) -40*(theta^4)*(2 +5*ecc^2)/(1 -5*theta^2) -40*(ecc^2)*(theta^6)/((1 -5*theta^2)^2))*math.sin(2*perigee)
    local raan_delta=-(y*theta*(ecc^2)/8)*(11 +80*(theta^2)/(1- 5*theta^2) +200*(theta^4)/((1 -5*theta^2)^2))*math.sin(2*perigee)
    local ecc_delta=(y*ecc*(neta^2)/8)*(1 -11*theta^2 -40*(theta^4)/(1 -5*theta^2))*math.cos(2*perigee)
    local incl_delta=-(y*(ecc^2)/8)*(1 -11*theta^2 -40*(theta^4)/(1 -5*theta^2))*math.cos(2*perigee)/math.tan(incl)

    print(mean_anom_delta/M, perigee_delta/perigee, raan_delta/raan, ecc_delta/ecc, incl_delta/incl)
    perigee=perigee+perigee_delta
    raan=raan+raan_delta
    ecc=ecc+ecc_delta
    incl=incl+incl_delta
    M=M+mean_anom_delta--]]

    --[[ McCuskey
    A2=J2*R^2
    local cos2f=(ecc^2)*(1 +2*math.sqrt(1 -ecc^2))/((1 +math.sqrt(1 -ecc^2))^2)
    local delta_ecc=A2*(math.sin(incl)^2)*(1 -ecc^2)*cos2f*math.cos(2*perigee)/(6*ecc*(a^2)*((1 -ecc^2)^2))
    local delta_perigee=(A2*math.sin(2*perigee)/((a^2)*((1 -ecc^2)^2)))*((math.sin(incl)^2)*((1/8) + ((1 -ecc^2)/(6*ecc^2))*cos2f) + cos2f*(math.cos(incl)^2)/6)
    local delta_incl=-(A2*math.sin(2*incl)*math.cos(2*perigee)*cos2f)/(12*(a^2)*((1 -ecc^2)^2))
    local delta_raan=-(A2*math.cos(incl)*math.sin(2*perigee)*cos2f)/(6*(a^2)*((1 -ecc^2)^2))
    local delta_mean_anom=-(A2*(math.sin(incl)^2)*math.sin(2*perigee))/((a^2)*((1 -(ecc^2)^(3/2))))*((1/8) +((2 + (ecc^2))/(12*(ecc^2)))*cos2f)

    ecc=ecc+delta_ecc
    perigee=perigee+delta_perigee
    incl=incl+delta_incl
    raan=raan+delta_raan
    M=M+delta_mean_anom--]]

    --M=M+tle.mean_motion_1*time*time -- addition of mean motion first derivative
    --[[ wrap around large M
    while M>math.pi*2 do
        M=M-math.pi*2
    end--]]

    --[[ kozai
    -- useful constants
    local sin_i_2=math.sin(incl)^2

    -- mean short term periodics
    local cos2v=(ecc^2)*(1 +2*math.sqrt(1 -(ecc^2)))/((1 +math.sqrt(1 -(ecc^2)))^2)
    local de_s_m=(A2/(p^2))*sin_i_2*((1 -(ecc^2))/(6*ecc))*cos2v*math.cos(2*perigee)
    local dw_s_m=(A2/(p^2))*(sin_i_2*((1/8) +((1 -(ecc^2))/(6*(ecc^2)))*cos2v)
                             +((math.cos(incl)^2)*cos2v/6))*math.sin(2*perigee)
    local di_s_m=-(A2/(12*(p^2)))*math.sin(2*incl)*cos2v*math.cos(2*perigee)
    local draan_s_m=-(A2/(6*(p^2)))*math.cos(incl)*cos2v*math.sin(2*perigee)
    local dM_s_m=-(A2/(p^2))*math.sqrt(1 -(ecc^2))*sin_i_2*((1/8)
                                                       +((1/8) +(1 +((ecc^2)/2))/(6*(ecc^2)))*cos2v)
                                                     *math.sin(2*perigee)

    -- long term periodics
    local di_l=-(A2/(p^2))*(((ecc^2)*math.sin(2*incl))/(8*(4 -5*sin_i_2)))
                          *(((14 -15*sin_i_2)/6) - (A4/(A2^2))*((18 -21*sin_i_2)/(7)))*math.cos(2*perigee)
               -((3*A3)/(4*A2*p))*ecc*math.cos(incl)*math.sin(perigee)
    --local de_l=-((1 -(ecc^2))/ecc)*math.tan(incl)*di_1 -- equivalent to below
    local de_l=-(A2/(a*(p^2)))*((ecc*sin_i_2)/(4*(4 -5*sin_i_2)))
                              *(((14 -15*sin_i_2)/6) - (A4/(A2^2))*((18 -21*sin_i_2)/(7)))
                              *math.cos(2*perigee)
               -((3*A3)/(4*A2*a))*math.sin(incl)*math.sin(perigee)
    local draan_l=-((A2*(ecc^2)*math.cos(incl))/((p^2)*2*(4 -5*sin_i_2)))*(((7 -15*sin_i_2)/6)
                      -(A4/(A2^2))*((9 -21*sin_i_2)/7)
                      +((5*sin_i_2)/(2*(4 -5*sin_i_2)))*(((14 -15*sin_i_2)/6)
                          -(A4/(A2^2))*((18 -21*sin_i_2)/7)))*math.sin(2*perigee)
                  +((3*A3)/(4*A2*p))*ecc*math.cos(perigee)/math.tan(incl)
    local dperigee_l=-((3*A2)/(8*(p^2)))*sin_i_2*math.sin(2*perigee)
                     -(A2/(p^2))*(((1/(4 -5*sin_i_2))*(((14 -15*sin_i_2)/24)*sin_i_2
                                                      -(ecc^2)*((28 -158*sin_i_2 +135*(sin_i_2^2))/48)
                                                      -(A4/(A2^2))*(((18 -21*sin_i_2)/28)*sin_i_2
                                                   -(ecc^2)*((36 -210*sin_i_2 + 189*(sin_i_2^2))/56))))
                                  -(((ecc^2)*sin_i_2*(13 -15*sin_i_2))/((4 -5*sin_i_2)^2))
                                   *(((14 -15*sin_i_2)/24) -(A4/(A2^2))*((18 -21*sin_i_2)/28)))
                                *math.sin(2*perigee)
                     +((3*A3)/(4*A2*p))*((sin_i_2 -((ecc*math.cos(incl))^2))/math.sin(incl))*(math.cos(perigee)/ecc)

    -- add Kozai long terms
    --ecc=ecc + de_s_m + de_l
    --incl=incl + di_s_m + di_l
    --perigee=perigee + dw_s_m + dperigee_l
    --raan=raan + draan_s_m + draan_l

    -- intermediates
    local E_prime=M+ecc*math.sin(M)
    for i=0,10,1 do
        E_prime=E_prime+((M-E_prime+ecc*math.sin(E_prime))/(1-ecc*math.cos(E_prime))) -- from spherical astronomy
    end
    local v_prime=2*math.atan(math.sqrt((1+ecc)/(1-ecc))*math.tan(E_prime/2))
    local a_r_prime=(1 + ecc*math.cos(v_prime))/(1 - ecc^2) -- a/r_prime
    --local a_r_prime=1/(1 - ecc*math.cos(E_prime)) -- a/r_prime

    -- short term periodics
    local da_s=(A2/a)*((2/3)*(1 - (3/2)*sin_i_2)*(a_r_prime^3 - (1 -ecc^2)^(-3/2))
                       + (a_r_prime^3)*sin_i_2*math.cos(2*(v_prime + perigee)))
    local de_s=((1-ecc^2)/(2*ecc*a))*da_s
               - ((A2*sin_i_2)/(2*a*p*ecc))*(math.cos(2*(v_prime + perigee))
                                             + ecc*math.cos(v_prime + 2*perigee)
                                             + (ecc/3)*math.cos(3*v_prime + 2*perigee))
    local dw_s=(2 - (5/2)*sin_i_2)*(v_prime - M + ecc*math.sin(v_prime))
               +(1 - (3/2)*sin_i_2)*((1/ecc)*(1 - (ecc^2)/4)*math.sin(v_prime) + (1/2)*math.sin(2*v_prime)
                                     +(ecc/12)*math.sin(3*v_prime))
               -(math.sin(v_prime + 2*perigee)/ecc)*((sin_i_2/4) + ((1/2) - (15/16)*sin_i_2)*(ecc^2))
               +(ecc*sin_i_2*math.sin(v_prime - 2*perigee)/16)
               -(math.sin(2*(v_prime + perigee))/2)*(1 - (5/2)*sin_i_2)
               +(math.sin(3*v_prime + 2*perigee)/ecc)*((7/12)*sin_i_2 - ((ecc^2)/6)*(1 - (19/8)*sin_i_2))
               +(3/8)*sin_i_2*math.sin(4*v_prime + 2*perigee) + (ecc/16)*sin_i_2*math.sin(5*v_prime + 2*perigee)
    dw_s=(dw_s*A2)/(p^2)
    local di_s=((A2*math.sin(2*incl))/(4*(p^2)))*(math.cos(2*(v_prime + perigee)) + ecc*math.cos(v_prime + 2*perigee)
                                             + (ecc/3)*math.cos(3*v_prime + 2*perigee))
    local draan_s=-(A2*math.cos(incl)/(p^2))*(v_prime - M + ecc*math.sin(v_prime)
                                         - (1/2)*math.sin(2*(v_prime + perigee))
                                         - (ecc/2)*math.sin(v_prime + 2*perigee)
                                         - (ecc/6)*math.sin(3*v_prime + 2*perigee))
    local dM_s=-(1 -(3/2)*sin_i_2)*((1-((ecc^2)/4))*math.sin(v_prime) +(ecc/2)*math.sin(2*v_prime)
                                    +((ecc^2)/12)*math.sin(3*v_prime))
               +sin_i_2*((1 +(5/4)*(ecc^2))*(math.sin(v_prime + 2*perigee)/4)
                         -((ecc^2)/16)*math.sin(v_prime - 2*perigee)
                         -(7/12)*(1 -((ecc^2)/28))*math.sin(3*v_prime +2*perigee)
                         -(3/8)*ecc*math.sin(4*v_prime + 2*perigee)
                         -((ecc^2)/16)*math.sin(5*v_prime + 2*perigee))
    dM_s=dM_s*(A2*math.sqrt(1 -ecc^2)/(p^2))

    -- apply short term periodics
    --a=a*(1 - delta_a) + da_s
    --ecc=ecc + de_s - de_s_m
    --incl=incl + di_s - di_s_m
    --perigee=perigee + dw_s - dw_s_m
    --raan=raan + draan_s - draan_s_m
    --M=M + dM_s -- dM_s_m
    
    --apply both
    a=a*(1 - delta_a) + da_s
    ecc=ecc + de_s + de_l
    incl=incl + di_s + di_l
    --perigee=perigee + dw_s + dperigee_l
    raan=raan + draan_s + draan_l
    M=M + dM_s 
    --]]

    -- calculate eccentric anomaly
    local E=M+ecc*math.sin(M) -- from spherical astronomy
    for i=0,10,1 do
        E=E+((M-E+ecc*math.sin(E))/(1-ecc*math.cos(E))) -- from spherical astronomy
    end

    -- calculate true anomaly
    local v=2*math.atan(math.sqrt((1+ecc)/(1-ecc))*math.tan(E/2))

    -- calculate r
    --local r=a*(1-ecc*math.cos(E))
    local r=a*(1.-ecc^2)/(1.+ecc*math.cos(v))

    -- calculate q
    local q={}
    --[[
    q.x=a*(math.cos(E)-tle.ecc)
    q.y=a*math.sqrt(1-tle.ecc*tle.ecc)*math.sin(E)
    q.z=0
    --]]
    q.x=r*math.cos(v)
    q.y=r*math.sin(v)
    q.z=0--]]

    --calculate q_dot
    local qdot={}
    --[[
    qdot_const=((mean_motion/60)*a)/(1-tle.ecc*math.cos(E))
    qdot.x=qdot_const*-math.sin(E)
    qdot.y=qdot_const*math.sqrt(1-tle.ecc*tle.ecc)*math.cos(E)
    qdot.z=0
    --]]
    qdot_const=((mean_motion/60)*a)/(math.sqrt(1-ecc^2))
    qdot.x=qdot_const*-math.sin(v)
    qdot.y=qdot_const*(ecc+math.cos(v))
    qdot.z=0

    -- rotate q to ECI
    local r=rot.r3(q, -perigee)
    r=rot.r1(r, -incl)
    r=rot.r3(r, -raan)

    -- rotate qdot to ECI
    local rdot=rot.r3(qdot, -perigee)
    rdot=rot.r1(rdot, -incl)
    rdot=rot.r3(rdot, -raan)
    
    -- currently for all the cubesats, at t=0
    -- position
    -- x axis -8 to 8 km
    -- y axis -8 to 8 km
    -- z axis -6 to -15 km
    -- velocity
    -- x axis -.008 to .01 km/s
    -- y axis -.008 to .01 km/s
    -- z axis -0.006 to 0.005 km/s

    return r, rdot
end

return kepler_lua
