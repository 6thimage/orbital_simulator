-- printf
function printf(s, ...)
    return io.write(s:format(...))
end

-- sprintf
function sprintf(s, ...)
    return s:format(...)
end

-- fprintf
function fprintf(f, s, ...)
    return f:write(s:format(...))
end
