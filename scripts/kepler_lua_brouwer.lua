local rot=require('rotations')

kepler_lua={}

function kepler_lua.single(tle, time)
    -- calculate a
    local mu=398600.8 -- wgs72
    --local mu=398600.79964 -- wgs72old
    --local mu=398600.5 -- wgs84
    -- mean motion is in rads/min,[ needs to be in rads/sec]
    local a_sgp=(mu/((tle.mean_motion/60)^2))^(1/3) -- a= (mu/ n^2)^1/3

    --[[ corrections from spacetrack report 3
    -- J_2 from wgs72
    -- delta is delta_1 and delta_0, without the a_1^2 and a_0^2, respectively
    local delta = (0.001082616*(6378.135^2))/2 -- k_2 = (J_2 *a_E^2)/2
    delta=(delta*3*(3*(math.cos(tle.incl)^2)-1))/(2*(1-tle.ecc*tle.ecc)^(3/2)) -- (3*K_2*(3*math.cos(i)^2 -1))/(2*(1-e^2)^(3/2))
    local delta_1=delta/(a_sgp*a_sgp) -- delta/(a_1^2)
    local a_0=a_sgp*(1 - (delta_1/3) - (delta_1^2) - ((134*(delta_1^3))/(81))) -- a_1*(1 - delta_1/3 - delta_1^2 - 134*delta_1^3/81)
    local delta_0=delta/(a_0*a_0) -- delta/(a_0^2)
    local mean_motion=tle.mean_motion/(1+delta_0) -- n_0/(1 + delta_0)
    local a=a_0/(1-delta_0) -- a_0/(1 - delta_0) --]]

    --local mean_motion=tle.mean_motion
	--local mean_motion=math.sqrt(mu/a_sgp^3) -- classical calculation of mean motion (mu/a^3)
    local a=a_sgp
    local ecc=tle.ecc
    local incl=tle.incl
    
    local mean_motion=tle.mean_motion

    -- wgs72 constants
    local R=6378.135 -- in km
    local J2=0.001082616
    local J3=-0.00000253881
    local J4=-0.00000165597
    -- kozai 1964, via kaula (theory of satellite geodesy), pg 117
    local J5=-0.00000021

    local A3=J3*R^3
    local A5=J5*R^5 -- TODO need to find what this constant is

    -- from 'The motion of a close earth satellite' - Kozai, Astro Journal Nov. 1959
    -- delta_a is of the order of 1e-4 and reduces the positional error
    -- on the xy plane, the error is reduced for the majority from -8 to 8 to -4 to 4
    -- in the z axis, the error is reduced from -16 to -4 to -15 to -8, which is a much more
    -- significant improvement
    --local p=a*(1 - ecc^2)
    --local delta_a=(A2/p^2)*(1 - (3/2)*math.sin(incl)^2)*math.sqrt(1 - ecc^2)
    --a=a*(1 - delta_a)

    -- mean motion correction is currently left out as it doesn't effect the results at epoch
    --[[local delta_mean_motion=(A2/p^2)*(1 - (3/2)*math.sin(incl)^2)*math.sqrt(1 - ecc^2)
    local mean_motion=mean_motion*(1 + delta_mean_motion)--]]
    --local mean_motion=tle.mean_motion

    -- calculate mean anomaly
    --local M=tle.mean_anom+(mean_motion*time) -- M=M_0 +n*t assumed that M_0 is at t=0

    -- useful constants
    local g2=(J2*R^2)/(2*a^2)--((mu^2)*J2*R^2/(2*a^4))
    local g3=0--A3/(a^3)
    local g4=(-3*J4*R^4)/(8*a^4)--(-3*(mu^4)*J4*R^4)/(8*a^8)
    local g5=0--A5/(a^5)
    local n=math.sqrt(1 - ecc^2)
    local theta=math.cos(incl)
    local theta2=theta*theta
    local theta4=theta2*theta2

    -- secular terms
    --local l_dp=1 + 
    local l_dp=tle.mean_anom --+ mean_motion*time*l_dp
    local g_dp=tle.perigee
    local h_dp=tle.raan

    -- long period terms
    local delta1_e=math.cos(2*g_dp)*(((g2*ecc)/(8*n^2))*(1 -11*theta2 -40*theta4/(1 -5*theta2))
                               -((5*g4*ecc)/(12*g2*n^2))*(1 -3*theta2 -8*theta4/(1 -5*theta2)))
                  +math.sin(g_dp)*(((g3*math.sin(incl))/(4*g2))
                             +((5*g5*math.sin(incl))/(64*g2*n^8))*(4 +3*ecc^2)
                               *(1 -9*theta2 -24*theta4/(1 -5*theta2)))
                  -((35*g5*ecc^2)/(384*g2*n^4))*math.sin(incl)*math.sin(3*g_dp)
                   *(1 -5*theta2 -16*theta4/(1 -5*theta2))
    local delta1_i=-(ecc*delta1_e)/(math.tan(incl)*n^2)
    local l_p=l_dp +math.sin(2*g_dp)*((g2/(8*n))*(1 -11*theta2 -40*theta4/(1 -5*theta2))
                                -((5*g4)/(12*g2*n))*(1 -3*theta2 -8*theta4/(1 -5*theta2)))
                   +math.cos(g_dp)*(-((g3*n*math.sin(incl))/(4*g2*ecc))
                               -((5*g5*math.sin(incl))/(64*g2*ecc*n^3))*(4 +9*ecc^2)
                                *(1 -9*theta2 -24*theta4/(1 -5*theta2)))
                   +math.cos(3*g_dp)*((35*g5*ecc*math.sin(incl))/(384*g2*n^3))
                               *(1 -5*theta2 -16*theta4/(1 -5*theta2))
    local g_p=g_dp +math.sin(2*g_dp)*(-(g2/(16*n^4))*((2 +ecc^2) -11*theta2*(2 +3*ecc^2)
                                                 -40*theta4*((2 +5*ecc^2)/(1 -5*theta2))
                                                 -400*(ecc^2)*theta2*theta4/((1 -5*theta2)^2))
                                 +((5*g4)/(24*g2*n^4))*(2 +ecc^2 -3*theta2*(2 +3*ecc^2)
                                                        -8*theta4*((2 +5*ecc^2)/(1 -5*theta2))
                                                        -80*(ecc^2)*theta4*theta2/((1 -5*theta2)^2)))
                   +math.cos(g_dp)*((g3/(4*g2*n^2))*((math.sin(incl)/ecc) -((ecc*theta2)/math.sin(incl)))
                              +((5*g5)/(64*g2*n^6))*((((math.sin(incl)*n^2)/ecc) -((ecc*theta2)/math.sin(incl)))
                                                     *(4 +3*ecc^2) +ecc*math.sin(incl)*(26 +9*ecc^2))
                                                   *(1 -9*theta2 -24*theta4/(1 -5*theta2))
                               -((15*g5)/(32*g2*n^6))*ecc*theta2*math.sin(incl)*(4 +3*ecc^2)
                                                     *(3 +16*theta2/(1 -5*theta2)
                                                      +40*theta4/((1 -5*theta2)^2)))
                   +math.cos(3*g_dp)*(-((35*g5)/(1152*g2*n^6))*(ecc*math.sin(incl)*(3 +2*ecc^2)
                                                          -((theta2*ecc^3)/math.sin(incl)))
                                  *(1 -5*theta2 -16*theta4/(1 -5*theta2))
                                 +((35*g5)/(576*g2*n^6))*(ecc^3)*theta2*math.sin(incl)
                                  *(5 +32*theta2/(1 -5*theta2) +80*theta4/((1 -5*theta2)^2)))
    local h_p=h_dp +math.sin(2*g_dp)*(-(g2/(8*n^4))*(ecc^2)*theta*(11 +80*theta2/(1 -5*theta2)
                                                             +200*theta4/((1 -5*theta2)^2))
                                 +((5*g4)/(12*g2*n^4))*(ecc^2)*theta*(3 +16*theta2/(1 -5*theta2)
                                                                     +40*theta4/((1 -5*theta2)^2)))
                   +math.cos(g_dp)*(((g3*ecc*theta)/(4*g2*(n^2)*math.sin(incl)))
                              +((5*g5*ecc*theta)/(64*g2*(n^6)*math.sin(incl)))*(4 +3*ecc^2)
                               *(1 -9*theta2 -24*theta4/(1 -5*theta2))
                              +((15*g5)/(32*g2*n^6))*ecc*theta*math.sin(incl)*(4 +3*ecc^2)
                               *(3 +16*theta2/(1 -5*theta2) +40*theta4/((1 -5*theta2)^2)))
                   +math.cos(3*g_dp)*(-((35*g5*theta*ecc^3)/(1152*g2*(n^6)*math.sin(incl)))
                                  *(1 -5*theta2 -16*theta4/(1 -5*theta2))
                                 -((35*g5)/(576*g2*n^6))*(ecc^3)*theta*math.sin(incl)
                                  *(5 +32*theta2/(1 -5*theta2) +80*theta4/((1 -5*theta2)^2)))

    -- intermediate calculation
    local E_p=l_p +ecc*math.sin(l_p)
    for i=0,10,1 do
        E_p=E_p+((l_p -E_p +ecc*math.sin(E_p))/(1 -ecc*math.cos(E_p)))
    end
    local f_p=2*math.atan(math.sqrt((1 +ecc)/(1 -ecc))*math.tan(E_p/2))
    local ar_p=(1 +ecc*math.cos(f_p))/(1 -ecc^2)

    -- more useful constants
    local ar_p2=ar_p^2
    local ar_p3=ar_p^3

    -- short period terms & combination
    --local a=a*(1 +g2*((-1 +3*theta2)*(ar_p3 -(1/(n^3))) +3*(1 -theta2)*ar_p3*math.cos(2*g_p +2*f_p)))
    local e=ecc +delta1_e --+((n^2)/(2*ecc))*(g2*((-1 +3*theta2)*(ar_p3 -(1/(n^3)))
                          --                       +3*(1 -theta2)*(ar_p3 -(1/(n^4)))*math.cos(2*g_p +2*f_p))
                          --                 -(g2/n^4)*(1 -theta2)*(3*ecc*math.cos(2*g_p +f_p)
                          --                                         +ecc*math.cos(2*g_p +3*f_p)))
    incl=incl +delta1_i --+(g2/(2*n^4))*theta*math.sqrt(1 -theta2)*(3*math.cos(2*g_p +2*f_p)
                        --                                    +3*ecc*math.cos(2*g_p +f_p)
                        --                                    +ecc*math.cos(2*g_p +3*f_p))
    local M=l_p ---(g2/(4*ecc*n))*(2*(-1 +3*theta2)*(1 +ar_p +ar_p2*n^2)*math.sin(f_p)
                --                +3*(1 -theta2)*((1 -ar_p -ar_p2*n^2)*math.sin(2*g_p +f_p)
                --                               +((1/3) +ar_p +ar_p2*n^2)*math.sin(2*g_p +3*f_p)))
    local perigee=g_p ---(g2/(4*ecc*n^2))*(2*(-1 +3*theta2)*(1 +ar_p +ar_p2*n^2)*math.sin(f_p)
                      --                  +3*(1 -theta2)*((1 -ar_p -ar_p2*n^2)*math.sin(2*g_p +f_p)
                      --                                 +((1/3) +ar_p +ar_p2*n^2)*math.sin(2*g_p +3*f_p))
                      --                  +(g2/(4*n^4))*(6*(-1 +5*theta2)*(f_p -l_p +ecc*math.sin(f_p))
                      --                                +(3 -5*theta2)*(3*math.sin(2*g_p +2*f_p)
                      --                                               +3*ecc*math.sin(2*g_p +f_p)
                      --                                               +ecc*math.sin(2*g_p +3*f_p))))
    local raan=h_p ---(g2/(2*n^4))*theta*(6*(f_p -l_p +ecc*math.sin(f_p) -3*math.sin(2*g_p +2*f_p)
                   --                       -3*ecc*math.sin(2*g_p +f_p) -ecc*math.sin(2*g_p +3*f_p)))

    ecc=e

    -- calculate eccentric anomaly
    local E=M +ecc*math.sin(M) -- from spherical astronomy
    for i=0,10,1 do
        E=E+((M -E +ecc*math.sin(E))/(1 -ecc*math.cos(E))) -- from spherical astronomy
    end

    -- calculate true anomaly
    local f=2*math.atan(math.sqrt((1 +ecc)/(1 -ecc))*math.tan(E/2))

    -- calculate r
    local r=a*(1 -ecc*math.cos(E))
    --local r=a*(1.-ecc^2)/(1.+ecc*math.cos(v))

    -- calculate q
    local q={}
    --[[
    q.x=a*(math.cos(E)-tle.ecc)
    q.y=a*math.sqrt(1-tle.ecc*tle.ecc)*math.sin(E)
    q.z=0
    --]]
    q.x=r*math.cos(f)
    q.y=r*math.sin(f)
    q.z=0--]]

    --calculate q_dot
    local qdot={}
    --[[
    qdot_const=((mean_motion/60)*a)/(1-tle.ecc*math.cos(E))
    qdot.x=qdot_const*-math.sin(E)
    qdot.y=qdot_const*math.sqrt(1-tle.ecc*tle.ecc)*math.cos(E)
    qdot.z=0
    --]]
    qdot_const=((mean_motion/60)*a)/(math.sqrt(1-ecc^2))
    qdot.x=qdot_const*-math.sin(f)
    qdot.y=qdot_const*(ecc+math.cos(f))
    qdot.z=0

    -- rotate q to ECI
    local r=rot.r3(q, -perigee)
    r=rot.r1(r, -incl)
    r=rot.r3(r, -raan)

    -- rotate qdot to ECI
    local rdot=rot.r3(qdot, -perigee)
    rdot=rot.r1(rdot, -incl)
    rdot=rot.r3(rdot, -raan)
    
    -- currently for all the cubesats, at t=0
    -- position
    -- x axis -8 to 8 km
    -- y axis -8 to 8 km
    -- z axis -6 to -15 km
    -- velocity
    -- x axis -.008 to .01 km/s
    -- y axis -.008 to .01 km/s
    -- z axis -0.006 to 0.005 km/s

    return r, rdot
end
