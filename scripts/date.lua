-- date conversion functions
function unix_to_jdate(ts)
    return (ts/86400)+2440587.5
end

function jdate_to_unix(jd)
    return (jd-2440587.5)*86400
end
