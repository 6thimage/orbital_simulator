local rotations={}

function rotations.r1(r, a)
    local cos=math.cos(a)
    local sin=math.sin(a)
    local v={}
    v.x=r.x
    v.y=cos*r.y + sin*r.z
    v.z=-sin*r.y + cos*r.z
    return v
end

function rotations.r2(r, a)
    local cos=math.cos(a)
    local sin=math.sin(a)
    local v={}
    v.x=cos*r.x - sin*r.z
    v.y=r.y
    v.z=sin*r.x + cos*r.z
    return v
end

function rotations.r3(r, a)
    local cos=math.cos(a)
    local sin=math.sin(a)
    local v={}
    v.x=cos*r.x + sin*r.y
    v.y=-sin*r.x + cos*r.y
    v.z=r.z
    return v
end

return rotations
