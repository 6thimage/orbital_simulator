require "printf"
require "date"
require 'spairs'

function help(all)
    printf("Script functions\n")
    printf("    unix_to_jdate\n")
    printf("    jdate_to_unix\n")
    printf("Bindings\n")
    f=io.open("lua_bindings","r")
    line=f:read("*L")
    line_count=6
    while line do
        io.write(line)
        line=f:read("*L")
        if(all==nil and line_count>23) then
            line_count=2
            printf("Press a key to continue\r")
            io.read(1)
        else
            line_count=line_count+1
        end
    end
    f:close()
end
