local rot=require('rotations')

kepler_lua={}

function kepler_lua.single(tle, time)

    -- calculate a
    local mu=398600.8 -- wgs72
    --local mu=398600.79964 -- wgs72old
    --local mu=398600.5 -- wgs84
    -- mean motion is in rads/min,[ needs to be in rads/sec]
    local a_sgp=(mu/((tle.mean_motion/60)^2))^(1/3) -- a= (mu/ n^2)^1/3

    -- calculate corrections 
    --[[ corrections from spacetrack report 3
    -- J_2 from wgs72
    -- delta is delta_1 and delta_0, without the a_1^2 and a_0^2, respectively
    local delta = (0.001082616*(6378.135^2))/2 -- k_2 = (J_2 *a_E^2)/2
    delta=(delta*3*(3*(math.cos(tle.incl)^2)-1))/(2*(1-tle.ecc*tle.ecc)^(3/2)) -- (3*K_2*(3*math.cos(i)^2 -1))/(2*(1-e^2)^(3/2))
    local delta_1=delta/(a_sgp*a_sgp) -- delta/(a_1^2)
    local a_0=a_sgp*(1 - (delta_1/3) - (delta_1^2) - ((134*delta_1^3)/(81))) -- a_1*(1 - delta_1/3 - delta_1^2 - 134*delta_1^3/81)
    local delta_0=delta/(a_0*a_0) -- delta/(a_0^2)
    local mean_motion=tle.mean_motion/(1+delta_0) -- n_0/(1 + delta_0)
    local a=a_0/(1-delta_0) -- a_0/(1 - delta_0) --]]

    --local mean_motion=tle.mean_motion
	--local mean_motion=math.sqrt(mu/a_sgp^3) -- classical calculation of mean motion (mu/a^3)
    local a=a_sgp



    local ecc=tle.ecc
    local incl=tle.incl
    local perigee=tle.perigee
    local raan=tle.raan
    local mean_motion=tle.mean_motion

    -- wgs72 constants
    local R=6378.135 -- in km
    local J2=0.001082616
    local J3=-0.00000253881
    local J4=-0.00000165597

    local A2=(3/2)*J2*R^2
    local A3=J3*R^3
    local A4=(-3./8.)*J4*R^4
    --local A4=(35./8.)*J4*R^4

    -- from 'The motion of a close earth satellite' - Kozai, Astro Journal Nov. 1959
    -- delta_a is of the order of 1e-4 and reduces the positional error
    -- on the xy plane, the error is reduced for the majority from -8 to 8 to -4 to 4
    -- in the z axis, the error is reduced from -16 to -4 to -15 to -8, which is a much more
    -- significant improvement
    local p=a*(1 - ecc^2)
    local delta_a=(A2/p^2)*(1 - (3/2)*math.sin(incl)^2)*math.sqrt(1 - ecc^2)
    --a=a*(1 - delta_a)

    -- mean motion correction is currently left out as it doesn't effect the results at epoch
    local delta_mean_motion=(A2/p^2)*(1 - (3/2)*math.sin(incl)^2)*math.sqrt(1 - ecc^2)
    local mean_motion=mean_motion*(1 + delta_mean_motion)

    -- calculate mean anomaly
    local M=tle.mean_anom+tle.mean_motion*time -- M=M_0 +n*t assumed that M_0 is at t=0
    --M=M+tle.mean_motion_1*time*time -- addition of mean motion first derivative
    -- wrap around large M
    while M>math.pi*2 do
        M=M-math.pi*2
    end

    -- intermediates
    local E_prime=M+ecc*math.sin(M)
    for i=0,10,1 do
        E_prime=E_prime+((M-E_prime+ecc*math.sin(E_prime))/(1-ecc*math.cos(E_prime))) -- from spherical astronomy
    end
    local v_prime=2*math.atan(math.sqrt((1+ecc)/(1-ecc))*math.tan(E_prime/2))
    local a_r_prime=(1 + ecc*math.cos(v_prime))/(1 - ecc^2) -- a/r_prime
    
    -- useful constants
    local sin_i_2=math.sin(incl)^2

    -- short term periodics
    local da_s=(A2/a)*((2/3)*(1 - (3/2)*sin_i_2)*(a_r_prime^3 - (1 -ecc^2)^(-3/2)) + (a_r_prime^3)*sin_i_2*math.cos(2*(v_prime + perigee)))
    local de_s=((1-ecc^2)/(2*ecc*a))*da_s - ((A2*sin_i_2)/(2*a*p*ecc))*(math.cos(2*v_prime + 2*perigee) + ecc*math.cos(v_prime + 2*perigee) + (ecc/3)*math.cos(3*v_prime + 2*perigee))
    local dw_s=(2 - (5/2)*sin_i_2)*(v_prime - M + ecc*math.sin(v_prime))
               + (1 - (3/2)*sin_i_2)*((1/ecc)*(1 - ecc^2/4)*math.sin(v_prime) + (1/2)*math.sin(2*v_prime) + (ecc/12)*math.sin(3*v_prime))
               - (math.sin(v_prime + 2*perigee)/ecc)*(sin_i_2/4 + ((1/2) - 15*sin_i_2/16)*ecc^2)
               + ecc*sin_i_2*math.sin(v_prime - 2*perigee)/16
               - (math.sin(2*(v_prime + perigee))/2)*(1 - (5*sin_i_2/2))
               + (math.sin(3*v_prime + 2*perigee)/ecc)*((7*sin_i_2/12) - (ecc^2/6)*(1 - (19*sin_i_2/8)))
               + 3*sin_i_2*math.sin(4*v_prime + 2*perigee)/8 + ecc*sin_i_2*math.sin(5*v_prime + 2*perigee)/16
    dw_s=dw_s*A2/p^2
    local di_s=((A2*math.sin(2*incl))/(4*p^2))*(math.cos(2*(v_prime + perigee)) + ecc*math.cos(v_prime + 2*perigee) + (ecc/3)*math.cos(3*v_prime + 2*perigee))
    local draan_s=(-A2*math.cos(incl)/p^2)*(v_prime - M + ecc*math.sin(v_prime) - (1/2)*math.sin(2*(v_prime + perigee)) - (ecc/2)*math.sin(v_prime + 2*perigee) - (ecc/6)*math.sin(3*v_prime + 2*perigee))
    local dM_s=0

    -- long term periodics
    local de_1=((A2*ecc*math.cos(2*perigee)*sin_i_2)/(p*a*4*(4 - 5*sin_i_2)))*((14 - 15*sin_i_2)/6 - (A4/A2^2)*(18 - 21*sin_i_2)/7) - (3*A3*math.sin(incl)*math.sin(perigee))/(4*a*A2)
    local dw_1=(14 - 15*sin_i_2)*(sin_i_2/24) - (ecc^2/48)*(28 - 158*sin_i_2 + 135*sin_i_2^2) - (A4/A2^2)*((18 - 21*sin_i_2)*(sin_i_2/28) - (ecc^2/56)*(36 - 210*sin_i_2 + 189*sin_i_2^2))
    dw_1=dw_1/(4 - 5*sin_i_2) - ((ecc^2*sin_i_2*(13 - 15*sin_i_2)/((4 - 5*sin_i_2)^2))*((14 - 15*sin_i_2)/24 - A4*(18 - 21*sin_i_2)/(28*A2^2)))
    dw_1=-(dw_1 + 3*sin_i_2/8)*math.sin(2*perigee)*A2/p^2 --+ (3*A3*math.cos(perigee)*(sin_i_2 - (ecc*math.cos(incl))^2))/(4*ecc*A2*p*p*math.sin(incl)) -- added p to bottom of last term
    local di_1=(-A2*ecc^2*math.sin(2*incl)*math.cos(2*perigee))/(p^2*8*(4 - 5*sin_i_2))*((14 - 15*sin_i_2)/6 - A4*(18 - 21*sin_i_2)/(7*A2^2)) + (3*A3*ecc*math.cos(incl)*math.sin(perigee))/(4*A2*p)
    local draan_1=((7 - 15*sin_i_2)/6 - (A4/A2^2)*(9 - 21*sin_i_2)/7) + (5*sin_i_2/(2*(4 - 5*sin_i_2)))*((14 - 15*sin_i_2)/6 - (A4/A2^2)*(18 - 21*sin_i_2)/7)
    draan_1=-draan_1*A2*math.cos(incl)*math.sin(2*perigee)*ecc^2/(2*(4 - 5*sin_i_2)*p^2) + (3*A3*math.cos(incl)*ecc*math.cos(perigee))/(4*A2*p*math.sin(incl))

    -- long terms
    if true then
        a=a*(1 - delta_a)
        ecc=ecc + de_1
        perigee=perigee + dw_1
        incl=incl + di_1
        raan=raan + draan_1
    end
    -- short terms
    if true then
        --a=a + da_s
        --ecc=ecc + de_s
        --perigee=perigee + dw_s
        incl=incl + di_s
        --raan=raan + draan_s
        --M=M + dM_s
    end

    -- calculate eccentric anomaly
    --local E=M
    --local E=math.pi
    local E=M+ecc*math.sin(M) -- from spherical astronomy
    for i=0,10,1 do
        -- Newton-Raphson method
        --E=E-((E-tle.ecc*math.sin(E)-M)/(1-tle.ecc*math.cos(E)))
        E=E+((M-E+ecc*math.sin(E))/(1-ecc*math.cos(E))) -- from spherical astronomy
    end

    -- calculate true anomaly
    local v=2*math.atan(math.sqrt((1+ecc)/(1-ecc))*math.tan(E/2))

    -- calculate r
    local r=a*(1-ecc*math.cos(E))
    --local r=a*(1.-ecc^2)/(1.+ecc*math.cos(v))

    -- calculate q
    local q={}
    --[[
    q.x=a*(math.cos(E)-tle.ecc)
    q.y=a*math.sqrt(1-tle.ecc*tle.ecc)*math.sin(E)
    q.z=0
    --]]
    q.x=r*math.cos(v)
    q.y=r*math.sin(v)
    q.z=0--]]

    --calculate q_dot
    local qdot={}
    --[[
    qdot_const=((mean_motion/60)*a)/(1-tle.ecc*math.cos(E))
    qdot.x=qdot_const*-math.sin(E)
    qdot.y=qdot_const*math.sqrt(1-tle.ecc*tle.ecc)*math.cos(E)
    qdot.z=0
    --]]
    qdot_const=((mean_motion/60)*a)/(math.sqrt(1-ecc^2))
    qdot.x=qdot_const*-math.sin(v)
    qdot.y=qdot_const*(ecc+math.cos(v))
    qdot.z=0

    -- rotate q to ECI
    local r=rot.r3(q, -perigee)
    r=rot.r1(r, -incl)
    r=rot.r3(r, -raan)

    -- rotate qdot to ECI
    local rdot=rot.r3(qdot, -perigee)
    rdot=rot.r1(rdot, -incl)
    rdot=rot.r3(rdot, -raan)
    
    -- currently for all the cubesats, at t=0
    -- position
    -- x axis -8 to 8 km
    -- y axis -8 to 8 km
    -- z axis -4 to -16 km
    -- velocity
    -- x axis -.008 to .01 km/s
    -- y axis -.008 to .01 km/s
    -- z axis -0.006 to 0.005 km/s

    return r, rdot
end

return kepler_lua
