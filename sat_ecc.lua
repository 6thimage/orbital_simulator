lfs=require("lfs") -- lua file system

-- add names of files in data to satellites
satellites={}
for file in lfs.dir('data/tle') do
    if(string.sub(file,0,1)~=".") then
        table.insert(satellites, file)
    end
end

function get_ecc(a)
    local t=tle.parse('data/tle/'..a)
    if type(t) == 'number' then
        print('Bad file - '..a)
        return
    end
    t=t[#t]
    return t.ecc
end

function sort(a,b)
    return get_ecc(a)<get_ecc(b)
end

table.sort(satellites,sort) -- sort according to eccentricity

for i=1, 3, 1 do
    print(satellites[i], get_ecc(satellites[i]))
end

print()

for i=2, 0, -1 do
    print(satellites[#satellites-i], get_ecc(satellites[#satellites-i]))
end

