#ifndef _LUA_WRAPPER_H_
#define _LUA_WRAPPER_H_

#include <lua.h>

#define LUA_TLE "tle_entry"
int luaopen_tle(lua_State *L);

#define LUA_SGP4_WORK "sgp4_work"
int luaopen_sgp4(lua_State *L);

int luaopen_kepler(lua_State *L);

int luaopen_orb_int(lua_State *L);

#define LUA_EOP_DATA "eop_data"
typedef struct
{
    double xp[366];
    double yp[366];
    double deps[366];
    double dpsi[366];
    int num;
} lua_eop_data;

int luaopen_eop_reader(lua_State *L);

int luaopen_coords(lua_State *L);

#define LUA_AP_DATA "ap_data"
typedef struct
{
    double ap_day[366];
    double ap_3hour[8*366];
    int num;
} lua_ap_data;

int luaopen_ap_reader(lua_State *L);

#define LUA_F107_DATA "f107_data"
typedef struct
{
    double f107[366];
    int num;
} lua_f107_data;

int luaopen_f107_reader(lua_State *L);

#define LUA_NRLMSISE_DATA "nrlmsise_data"
int luaopen_nrlmsise(lua_State *L);

#define LUA_SAT_OBJ_DATA "sat_obj_data"
int luaopen_sat_obj(lua_State *L);

#endif /* !_LUA_WRAPPER_H_ */
