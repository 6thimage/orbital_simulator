#include "lua_wrapper.h"
#include "f107_reader.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>

/* methods */
static int lua_f107_file_read(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    lua_f107_data data, *lua_data;
    int i;

    i=f107_file_read(filename, data.f107, &data.num);
    if(i<0)
    {
        puts("Failed to process F10.7 file");
        lua_pushinteger(L, 0);
        return 1;
    }

    lua_data=lua_newuserdata(L, sizeof(lua_f107_data));
    memcpy(lua_data, &data, sizeof(lua_f107_data));
    luaL_setmetatable(L, LUA_F107_DATA);

    return 1;
}

/* meta */
static int f107_tostring(lua_State *L)
{
    lua_f107_data *data=luaL_checkudata(L, 1, LUA_F107_DATA);
    lua_pushfstring(L, "F107 data - %d (day) values", data->num);
    return 1;
}

static int f107_length(lua_State *L)
{
    lua_f107_data *data=luaL_checkudata(L, 1, LUA_F107_DATA);
    lua_pushinteger(L, data->num);
    return 1;
}

static const luaL_Reg f107_methods[]={
    {"file_read", lua_f107_file_read},
    {0, 0}
};

static const luaL_Reg f107_meta[]={
    {"__tostring", f107_tostring},
    {"__len", f107_length},
    {0, 0}
};

int luaopen_f107_reader(lua_State *L)
{
    luaL_newlib(L, f107_methods); /* lad library methods */
    lua_setglobal(L, "f107"); /* store under global 'f107' */
    luaL_newmetatable(L, LUA_F107_DATA); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, f107_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */

    return 1;
}
