#include "lua_wrapper.h"

#include "nrlmsise.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>
#include <stdlib.h>

/* methods */
static int nrlmsise_init(lua_State *L)
{
    int ap_len=0, f107_len=0, data_len=0;
    int y_len, yl_len, *y, *yl, *p;
    double *dp, *d3p;
    nrlmsise_data *lua_data;

    luaL_checktype(L, 1, LUA_TTABLE);
    luaL_checktype(L, 2, LUA_TTABLE);
    luaL_checktype(L, 3, LUA_TTABLE);
    luaL_checktype(L, 4, LUA_TTABLE);

    /* check data table sizes */
    if(luaL_len(L, 1) != luaL_len(L, 2))
        return luaL_error(L, "AP and f10.7 tables are of different sizes (%d and %d)",
                luaL_len(L, 1), luaL_len(L, 2));

    /* check data sizes */
    /* AP */
    lua_pushvalue(L, 1);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        lua_ap_data *data=luaL_checkudata(L, -1, LUA_AP_DATA);
        ap_len+=data->num;
        lua_pop(L, 1);
    }
    lua_pop(L, 1);

    /* F10.7 */
    lua_pushvalue(L, 2);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        lua_f107_data *data=luaL_checkudata(L, -1, LUA_F107_DATA);
        f107_len+=data->num;
        lua_pop(L, 1);
    }
    lua_pop(L, 1);

    if(ap_len!=f107_len)
        return luaL_error(L, "AP and F10.7 data are of different sizes (%d and %d)",
                ap_len, f107_len);

    /* check year data */
    y_len=luaL_len(L, 3);
    yl_len=luaL_len(L, 4);
    if(y_len != yl_len)
        return luaL_error(L, "years and year_lengths are of different sizes (%d and %d)",
                y_len, yl_len);

    /* allocate years */
    y=malloc(sizeof(int)*(y_len+1));
    if(!y)
        return luaL_error(L, "failed to allocate years");
    yl=malloc(sizeof(int)*yl_len);
    if(!yl)
        return luaL_error(L, "failed to allocate year_lengths");

    /* copy years over */
    p=y;
    lua_pushvalue(L, 3);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        if(!lua_isinteger(L, -1))
            return luaL_error(L, "non-integer year");
        *p=lua_tointeger(L, -1);
        ++p;
        lua_pop(L, 1);
    }
    *p=0;
    lua_pop(L, 1);

    /* copy year lengths over */
    p=yl;
    lua_pushvalue(L, 4);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        if(!lua_isinteger(L, -1))
            return luaL_error(L, "non-integer year length");
        *p=lua_tointeger(L, -1);
        data_len+=*p;
        ++p;
        lua_pop(L, 1);
    }
    lua_pop(L, 1);

    if(data_len!=ap_len)
    {
        free(y);
        free(yl);

        return luaL_error(L, "total year length does not match AP/F10.7 data (%d and %d)",
                data_len, ap_len);
    }

    /* create return data */
    lua_data=lua_newuserdata(L, sizeof(nrlmsise_data));
    /* transfer year and year_length pointers to lua data */
    lua_data->years=y;
    lua_data->year_lengths=yl;
    y=0;
    yl=0;
    /* copy ap and f10.7 lengths across */
    lua_data->ap_length=ap_len;
    lua_data->f107_length=f107_len;
    /* allocate memory */
    lua_data->ap_day=malloc(sizeof(double)*ap_len);
    if(!lua_data->ap_day)
    {
        free(lua_data->years);
        lua_data->years=0;
        free(lua_data->year_lengths);
        lua_data->year_lengths=0;
        return luaL_error(L, "failed to allocate ap_day");
    }
    lua_data->ap_3hour=malloc(sizeof(double)*ap_len*8);
    if(!lua_data->ap_3hour)
    {
        free(lua_data->years);
        lua_data->years=0;
        free(lua_data->year_lengths);
        lua_data->year_lengths=0;
        free(lua_data->ap_day);
        lua_data->ap_day=0;
        return luaL_error(L, "failed to allocate ap_3hour");
    }
    lua_data->f107=malloc(sizeof(double)*f107_len);
    if(!lua_data->f107)
    {
        free(lua_data->years);
        lua_data->years=0;
        free(lua_data->year_lengths);
        lua_data->year_lengths=0;
        free(lua_data->ap_day);
        lua_data->ap_day=0;
        free(lua_data->ap_3hour);
        lua_data->ap_3hour=0;
        return luaL_error(L, "failed to allocate f107");
    }
    /* combine data */
    /* AP */
    dp=lua_data->ap_day;
    d3p=lua_data->ap_3hour;
    lua_pushvalue(L, 1);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        lua_ap_data *data=luaL_checkudata(L, -1, LUA_AP_DATA);
        memcpy(dp, data->ap_day, data->num*sizeof(double));
        dp+=data->num;
        memcpy(d3p, data->ap_3hour, data->num*8*sizeof(double));
        d3p+=data->num*8;
        lua_pop(L, 1);
    }
    lua_pop(L, 1);
    /* F10.7 */
    dp=lua_data->f107;
    lua_pushvalue(L, 2);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        lua_f107_data *data=luaL_checkudata(L, -1, LUA_F107_DATA);
        memcpy(dp, data->f107, data->num*sizeof(double));
        dp+=data->num;
        lua_pop(L, 1);
    }
    lua_pop(L, 1);
    luaL_setmetatable(L, LUA_NRLMSISE_DATA);

    return 1;
}

static int nrlmsise_gc(lua_State *L)
{
    nrlmsise_data *data=luaL_checkudata(L, 1, LUA_NRLMSISE_DATA);
    if(data->f107)
    {
        free(data->f107);
        data->f107=0;
    }
    if(data->ap_day)
    {
        free(data->ap_day);
        data->ap_day=0;
    }
    if(data->ap_3hour)
    {
        free(data->ap_3hour);
        data->ap_3hour=0;
    }
    if(data->years)
    {
        free(data->years);
        data->years=0;
    }
    if(data->year_lengths)
    {
        free(data->year_lengths);
        data->year_lengths=0;
    }

    return 0;
}

/* meta */
static int nrlmsise_tostring(lua_State *L)
{
    nrlmsise_data *data=luaL_checkudata(L, 1, LUA_NRLMSISE_DATA);
    lua_pushfstring(L, "NRLMSISE data - %d AP %d F10.7",
            data->ap_length, data->f107_length);
    return 1;
}

static const luaL_Reg nrlmsise_methods[]={
    {"init", nrlmsise_init},
    {0, 0}
};

static const luaL_Reg nrlmsise_meta[]={
    {"__tostring", nrlmsise_tostring},
    {"__gc", nrlmsise_gc},
    {0, 0}
};

int luaopen_nrlmsise(lua_State *L)
{
    luaL_newlib(L, nrlmsise_methods); /* lad library methods */
    lua_setglobal(L, "nrlmsise"); /* store under global 'nrlmsise' */
    luaL_newmetatable(L, LUA_NRLMSISE_DATA); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, nrlmsise_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */

    return 1;
}
