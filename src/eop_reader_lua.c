#include "lua_wrapper.h"
#include "eop_reader.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>

/* methods */
static int lua_eop_file_read(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    lua_eop_data data, *lua_data;
    int i;


    i=eop_file_read(filename, data.xp, data.yp, data.dpsi, data.deps, &data.num);
    if(i<0)
    {
        puts("Failed to process EOP file");
        lua_pushinteger(L, 0);
        return 1;
    }

    lua_data=lua_newuserdata(L, sizeof(lua_eop_data));
    memcpy(lua_data, &data, sizeof(lua_eop_data));
    luaL_setmetatable(L, LUA_EOP_DATA);

    return 1;
}

/* meta */
static int eop_tostring(lua_State *L)
{
    lua_eop_data *data=luaL_checkudata(L, 1, LUA_EOP_DATA);
    lua_pushfstring(L, "EOP data - %d days", data->num);
    return 1;
}

static int eop_length(lua_State *L)
{
    lua_eop_data *data=luaL_checkudata(L, 1, LUA_EOP_DATA);
    lua_pushinteger(L, data->num);
    return 1;
}

static const luaL_Reg eop_methods[]={
    {"file_read", lua_eop_file_read},
    {0, 0}
};

static const luaL_Reg eop_meta[]={
    {"__tostring", eop_tostring},
    {"__len", eop_length},
    {0, 0}
};

int luaopen_eop_reader(lua_State *L)
{
    luaL_newlib(L, eop_methods); /* lad library methods */
    lua_setglobal(L, "eop"); /* store under global 'eop' */
    luaL_newmetatable(L, LUA_EOP_DATA); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, eop_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */

    return 1;
}
