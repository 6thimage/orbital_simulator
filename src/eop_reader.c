#include <stdio.h>

int eop_file_read(const char *filename, double *xp, double *yp, double *dpsi, double *deps, int *num_read)
{
    int i;
    FILE *f;
    char line[160];

    /* argument check */
    if(!xp || !yp || !dpsi || !deps || !num_read)
        return -1;

    /* open file */
    f=fopen(filename, "r");
    if(!f)
        return -2;

    /* skip header (first 14 lines) */
    for(i=0; i<14 && !feof(f); ++i)
        while(fgetc(f)!='\n') ;

    *num_read=0;

    /* read in data lines */
    while(!feof(f) && *num_read<366)
    {
        /* skip bad lines */
        if(!fgets(line, 160, f))
            continue;

        /* read in required columns */
        sscanf(line,
               "%*4d%*4d%*4d%*7d%11lf%11lf%*12f%*12f%11lf%11lf%*11f%*11f%*11f%*11f%*12f%*12f",
               &xp[*num_read], &yp[*num_read], &dpsi[*num_read], &deps[*num_read]);

        /* increment number read */
        ++*num_read;
    }

    /* close file */
    fclose(f);

    return 0;
}
