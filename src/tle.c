#include "tle.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "date.h"
#include "misc.h"

int tle_parse_entry(FILE *f, tle_entry *t, uint32_t *line_num)
{
    char line[72];
    int32_t len, num, mm2_expt, bsd_expt;
    uint32_t checksum, j, epochyr;
    double epochday;
    timedate td;

    /*** first line ***/
    /* read in line */
    len=nreadline(f, line, 72);
    ++*line_num;
    /* EOF check */
    if(!len)
        return 0;
    /* length check */
    if(len>24)
    {
        printf("[%2d] invalid satellite name '%s'\n", *line_num, line);
        return -1;
    }
    /* copy to struct */
    memcpy(t->name, line, len+1);

    /*** second line ***/
    len=nreadline(f, line, 72);
    ++*line_num;
    /* EOF check */
    if(!len)
        return 0;
    /* length check */
    if(len<68 || len>71)
    {
        printf("[%2d] 1: invalid line length %d\n", *line_num, len);
        return -1;
    }
    /* line number check */
    if(line[0]!='1')
    {
        printf("[%2d] 1: invalid line number, expected 1 got '%c'\n", *line_num, line[0]);
        return -1;
    }
    /* check checksum */
    checksum=0;
    for(j=0; j<68; ++j)
        if(isdigit(line[j]))
            checksum+=line[j]-'0';
        else if (line[j]=='-')
            checksum+=1;
    checksum%=10;
    if(checksum!=(uint32_t)(line[68]-'0'))
        printf("[%2d]1: checksum error %d\n", *line_num, checksum);
    /* remove checksum */
    line[68]='\0';
    /* parse line */
    num=sscanf(line, "%*c %5u%*c %8s %2u%12lf %10lf %6lf%2d %6lf%2d %*c %*s",
            &t->sat_num, t->designator, &epochyr, &epochday,
            &t->mean_motion_1, &t->mean_motion_2, &mm2_expt, &t->bstar_drag, &bsd_expt);
    if(num!=9)
    {
        printf("[%2d] 1: read in %d variables, expected 9\n", *line_num, num);
        return -1;
    }

    /*** third line ***/
    len=nreadline(f, line, 72);
    ++*line_num;
    /* EOF check */
    if(!len)
        return 0;
    /* length check */
    if(len<68 || len>71)
    {
        printf("[%2d] 2: invalid line length %d\n", *line_num, len);
        return -1;
    }
    /* line number check */
    if(line[0]!='2')
    {
        printf("[%2d] 2: invalid line number, expected 2 got '%c'\n", *line_num, line[0]);
        return -1;
    }
    /* check checksum */
    checksum=0;
    for(j=0; j<68; ++j)
        if(isdigit(line[j]))
            checksum+=line[j]-'0';
        else if (line[j]=='-')
            checksum+=1;
    checksum%=10;
    if(checksum!=(uint32_t)(line[68]-'0'))
        printf("[%2d] 2: checksum error %d\n", *line_num, checksum);
    /* remove checksum */
    line[68]='\0';
    /* parse line */
    num=sscanf(line, "%*c %*5s %8lf %8lf %7lf %8lf %8lf %11lf %*6s",
            &t->incl, &t->raan, &t->ecc, &t->perigee, &t->mean_anom,
            &t->mean_motion);
    if(num!=6)
    {
        printf("[%2d] 2: read in %d variables, expected 6\n", *line_num, num);
        return -1;
    }

    /* apply unit conversions */
    /* line 1 */
    /* convert epoch to Julian */
    epochyr += (epochyr<57)?2000:1900; /* turn 2 digit year into 4 */
    days_hms(epochyr, epochday, &td);
    t->epoch=julian_days(&td);
    t->mean_motion_1 *= M_PI/(1440.*720.);
    /* correctly exponate mean_motion_2 and bstar_drag - the -5 is due to assumed decimal point */
    t->mean_motion_2 *= pow(10., mm2_expt-5);
    t->bstar_drag *= pow(10., bsd_expt-5);
    /* line 2 */
    /* degree to radians */
    t->incl *= M_PI/180.;
    t->raan *= M_PI/180.;
    t->perigee *= M_PI/180.;
    t->mean_anom *= M_PI/180.;
    t->ecc *= 1e-7; /* exponate due to assumed decimal point */
    t->mean_motion *= M_PI/720.; /* convert from revs/day to rad/min - (2*pi)/1440 */

    return 1;
}

int tle_parse(char *filename, tle_entry t[], uint32_t tle_num)
{
    uint32_t i, line_num=0;
    FILE *f;

    /* open file */
    f=fopen(filename, "r");
    if(!f)
    {
        puts("Failed to open tle file");
        return -1;
    }

    /* read entries */
    for(i=0; i<tle_num && !feof(f); ++i)
    {
        if(tle_parse_entry(f, &t[i], &line_num)<0)
            break;
    }

    /* close file */
    fclose(f);

    /* return number of read entries */
    return i;
}
