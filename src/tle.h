#ifndef _TLE_H_
#define _TLE_H_

#include <stdint.h>
#include <stdio.h>

/* from http://www.celestrak.com/NORAD/documentation/tle-fmt.asp
 *
 * TLE format:
 * AAAAAAAAAAAAAAAAAAAAAAAA
 * 1 NNNNNU NNNNNAAA NNNNN.NNNNNNNN +.NNNNNNNN +NNNNN-N +NNNNN-N N NNNNN
 * 2 NNNNN NNN.NNNN NNN.NNNN NNNNNNN NNN.NNNN NNN.NNNN NN.NNNNNNNNNNNNNN
 */

typedef struct{
    /*** title line ***/
    char name[25]; /* 1-24: satellite name - 24 characters long */
    /*** line 1 ***/
                            /* 1: line number */
    unsigned int sat_num;   /* 3-7: satellite number */
                            /* 8: classification */
    char designator[9];     /* 10-17: international designator - 10-11 launch year,
                                      12-14 number in year, 15-17 piece of launch */
    double epoch;           /* 19-32: epoch - 19-20 year */
                            /*                21-32 day (and fraction) of year */
    double mean_motion_1;   /* 34-43: first time derivative of mean motion */
    double mean_motion_2;   /* 45-52: second time derivative of mean motion (decimal point assumed) */
    double bstar_drag;      /* 54-61: BSTAR drag term (decimal point assumed) */
                            /* 63: ephemeris type */
                            /* 65-68: element number */
                            /* 69: modulo 10 checksum (letters, blanks, periods and plus are 0, minus is 1 */
    /*** line 2 ***/
                            /* 1: line number */
                            /* 3-7: satellite number */
    double incl;            /* 9-16: inclination (deg) */
    double raan;            /* 18-25: right ascension of the ascending node (deg) */
    double ecc;             /* 27-33: eccentricity (decimal point assumed) */
    double perigee;         /* 35-42: argument of perigee (deg) */
    double mean_anom;       /* 44-51: mean anomaly (deg) */
    double mean_motion;     /* 53-63: mean motion (revs/day) */
                            /* 64-68: revolution number at epoch (revs) */
                            /* 69: modulo 10 checksum */

}tle_entry;

int tle_parse(char *filename, tle_entry t[], uint32_t tle_num);

int tle_parse_entry(FILE *f, tle_entry *t, uint32_t *line_num);

#endif /* !_TLE_H_ */
