#include "lua_wrapper.h"
#include "sat_object.h"
#include "quat.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <stdlib.h>

/* methods */
static int lua_sat_obj_init(lua_State *L)
{
    double mass, rot[3];
    int faces_len;
    sat_face *faces, *fp;
    sat_object *lua_data;

    luaL_checktype(L, 1, LUA_TTABLE); /* faces */
    luaL_checktype(L, 2, LUA_TTABLE); /* object rotation */
    mass=luaL_checknumber(L, 3);

    faces_len=luaL_len(L, 1);

    /* check there are at least one face */
    if(faces_len==0)
        return luaL_error(L, "At least one face is required (%d)", faces_len);

    /* allocate memory for faces */
    faces=malloc(sizeof(sat_face)*faces_len);
    if(!faces)
        return luaL_error(L, "Failed to allocate memory for faces");

    /* iterate over faces */
    fp=faces;
    lua_pushvalue(L, 1);
    lua_pushnil(L);
    while(lua_next(L, -2)!=0)
    {
        luaL_checktype(L, -1, LUA_TTABLE);
        lua_getfield(L, -1, "area");
        lua_getfield(L, -2, "x");
        lua_getfield(L, -3, "y");
        lua_getfield(L, -4, "z");

        fp->area=luaL_checknumber(L, -4);
        fp->normal[0]=luaL_checknumber(L, -3);
        fp->normal[1]=luaL_checknumber(L, -2);
        fp->normal[2]=luaL_checknumber(L, -1);
        lua_pop(L, 5);

        ++fp;
    }
    lua_pop(L, 1);

    /* create return data */
    lua_data=lua_newuserdata(L, sizeof(sat_object));
    lua_data->mass=mass;
    lua_data->num_faces=faces_len;
    lua_data->faces=faces;
    faces=0;

    /* get object rotation */
    lua_getfield(L, 2, "yaw");
    lua_getfield(L, 2, "pitch");
    lua_getfield(L, 2, "roll");
    rot[0]=luaL_checknumber(L, -3);
    rot[1]=luaL_checknumber(L, -2);
    rot[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);
    /* convert rotation to quaternion */
    quat_ypr(lua_data->rotation, rot[0], rot[1], rot[2]);

    luaL_setmetatable(L, LUA_SAT_OBJ_DATA);
    return 1;
}

/* meta */
static int sat_obj_tostring(lua_State *L)
{
    double rot[3];
    sat_object *data=luaL_checkudata(L, 1, LUA_SAT_OBJ_DATA);
    quat_to_euler(&rot[0], &rot[1], &rot[2], data->rotation);
    lua_pushfstring(L, "faces: %d object rotation yaw: %f pitch: %f roll: %f mass: %f",
                    data->num_faces, rot[0], rot[1], rot[2], data->mass);
    return 1;
}

static int sat_obj_gc(lua_State *L)
{
    sat_object *data=luaL_checkudata(L, 1, LUA_SAT_OBJ_DATA);
    if(data->faces)
    {
        free(data->faces);
        data->faces=0;
    }
    return 0;
}

static const luaL_Reg sat_obj_methods[]={
    {"init", lua_sat_obj_init},
    {0, 0}
};

static const luaL_Reg sat_obj_meta[]={
    {"__tostring", sat_obj_tostring},
    {"__gc", sat_obj_gc},
    {0, 0}
};

int luaopen_sat_obj(lua_State *L)
{
    luaL_newlib(L, sat_obj_methods); /* load library methods */
    lua_setglobal(L, "sat_obj"); /* store under global 'sat_obj' */
    luaL_newmetatable(L, LUA_SAT_OBJ_DATA); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, sat_obj_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */

    return 1;
}
