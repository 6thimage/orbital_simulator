#ifndef _EOP_READER_H_
#define _EOP_READER_H_

/* reads in an EOP 08 C04 data file - e.g. from
 * https://datacenter.iers.org/web/guest/eop/-/somos/5Rgv/version/212
 * arguments:
 *   filename   name of the EOP data file
 *   xp         pointer to an array of doubles that is at least 366 in size
 *   yp         pointer to an array of doubles that is at least 366 in size
 *   dpsi       pointer to an array of doubles that is at least 366 in size
 *   deps       pointer to an array of doubles that is at least 366 in size
 *   num_read   used to return the number of days read
 * returns:
 *   0  on success
 *   -1 if either of the eop or num_read pointers are zero
 *   -2 failed to open file
 */
int eop_file_read(const char *filename, double *xp, double *yp, double *dpsi, double *deps, int *num_read);

#endif /* ! _EOP_READER_H_ */
