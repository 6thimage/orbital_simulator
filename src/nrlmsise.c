#include "nrlmsise.h"
#include "nrlmsise-00/nrlmsise-00.h"

int nrlmsise_input_init(struct nrlmsise_input *input, nrlmsise_data *data,
                        int year, int day, int hour)
{
    int i, *py, *pl, year_length, day_offset, hour_t=hour/3;

    /* pointer checks */
    if(!input || !data->f107 || !data->ap_day || !data->ap_3hour || !data->years || !data->year_lengths)
        return -1;

    /* check time is not ridiculous */
    if(hour<0 || hour>23)
        return -2;

    /* search for year length */
    day_offset=-1; /* to account for doy starting at 1 */
    for(py=data->years, pl=data->year_lengths; *py!=0; ++py, ++pl)
    {
        if(*py==year)
        {
            year_length=*pl;
            break;
        }

        day_offset+=*pl;
    }
    if(*py==0)
        return -3;

    /* check if we are within the dataset */
    if((day+day_offset-40)<0 || (day+day_offset-3)<=0 ||
       (day+day_offset+40)>=data->f107_length || (day+day_offset)>=data->ap_length)
        return -4;

    input->year=0; /* ignored by gtd7d */
    input->doy=day;

    /* ap values */
    input->ap=input->ap_a->a[0]=data->ap_day[day+day_offset]; /* daily ap */
    hour_t+=(day+day_offset)*8;
    input->ap_a->a[1]=data->ap_3hour[hour_t];      /* 3hr AP index for current time */
    input->ap_a->a[2]=data->ap_3hour[hour_t-1];    /* 3hr AP index for 3 hrs before time */
    input->ap_a->a[3]=data->ap_3hour[hour_t-2];    /* 3hr AP index for 6 hrs before time */
    input->ap_a->a[4]=data->ap_3hour[hour_t-3];    /* 3hr AP index for 9 hrs before time */

    /* ap average of 8 3hr indicies from 12 to 33 hours before time */
    for(i=0, input->ap_a->a[5]=0.; i<8; ++i)
        input->ap_a->a[5]+=data->ap_3hour[hour_t-4-i]/8.;

    /* ap average of 8 3hr indicies from 36 to 57 hours before time */
    for(i=0, input->ap_a->a[6]=0.; i<8; ++i)
        input->ap_a->a[6]+=data->ap_3hour[hour_t-12-i]/8.;

    /* previous day's f10.7 value */
    input->f107=data->f107[day+day_offset-1];

    /* 81 day average of f10.7 around day */
    for(i=0, input->f107A=0.; i<81; ++i)
        input->f107A+=data->f107[day+day_offset-40+i]/81.;

    return 0;
}

int nrlmsise_input_update(struct nrlmsise_input *input, double sec, double lat, double lon)
{
    /* check for invalid seconds */
    if(sec<0. || sec>=86400.)
        return -1;

    input->sec=sec;
    input->g_lat=lat;
    input->g_long=lon;

    /* local solar time */
    input->lst=((input->sec/3600.) + (input->g_long/15.));

    return 0;
}
