#include "lua_wrapper.h"
#include "tle.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>

/* methods */
static int lua_tle_parse(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    tle_entry tle, *table_tle;
    FILE *f;
    uint32_t line_num=0;

    f=fopen(filename, "r");
    if(!f)
    {
        puts("Failed to open tle file");
        lua_pushinteger(L, 0);
        return 1;
    }

    while(!feof(f))
    {
        int ret=tle_parse_entry(f, &tle, &line_num);
        if(!ret)
            continue;
        else if(ret<0)
        {
            fclose(f);
            lua_pushinteger(L, 0);
            return 1;
        }
        if(line_num==3)
            lua_newtable(L);
        /* add entry to table */
        lua_pushinteger(L, line_num/3); /* sat number is line/3 */
        table_tle=lua_newuserdata(L, sizeof(tle_entry));
        luaL_setmetatable(L, LUA_TLE);
        memcpy(table_tle, &tle, sizeof(tle_entry));
        lua_settable(L, -3);
    }

    /* close file */
    fclose(f);

    return 1;
}

/* meta */
static int tle_tostring(lua_State *L)
{
    tle_entry *tle=luaL_checkudata(L, 1, LUA_TLE);
    lua_pushfstring(L, "tle name %s (%d:%s) epoch %f", tle->name[0]?tle->name:"?",
            tle->sat_num, tle->designator[0]?tle->designator:"?", tle->epoch);
    return 1;
}

static int tle_index(lua_State *L)
{
    tle_entry *tle=luaL_checkudata(L, 1, LUA_TLE);
    const char *var=luaL_checkstring(L, 2);
    if(!strcmp(var, "name"))
        lua_pushstring(L, tle->name);
    else if(!strcmp(var, "num"))
        lua_pushinteger(L, tle->sat_num);
    else if(!strcmp(var, "designator"))
        lua_pushstring(L, tle->designator);
    else if(!strcmp(var, "epoch"))
        lua_pushnumber(L, tle->epoch);
    else if(!strcmp(var, "mean_motion_1"))
        lua_pushnumber(L, tle->mean_motion_1);
    else if(!strcmp(var, "mean_motion_2"))
        lua_pushnumber(L, tle->mean_motion_2);
    else if(!strcmp(var, "bstar_drag"))
        lua_pushnumber(L, tle->bstar_drag);
    else if(!strcmp(var, "incl"))
        lua_pushnumber(L, tle->incl);
    else if(!strcmp(var, "raan"))
        lua_pushnumber(L, tle->raan);
    else if(!strcmp(var, "ecc"))
        lua_pushnumber(L, tle->ecc);
    else if(!strcmp(var, "perigee"))
        lua_pushnumber(L, tle->perigee);
    else if(!strcmp(var, "mean_anom"))
        lua_pushnumber(L, tle->mean_anom);
    else if(!strcmp(var, "mean_motion"))
        lua_pushnumber(L, tle->mean_motion);
    else
        return luaL_error(L, "Invalid index '%s'", var);

    return 1;
}

static int tle_newindex(lua_State *L)
{
    return luaL_error(L, "TLEs are read-only to the Lua environment");
}

static const luaL_Reg tle_methods[] = {
    {"parse", lua_tle_parse},
    {0, 0}
};

static const luaL_Reg tle_meta[] = {
    {"__tostring", tle_tostring},
    {"__index", tle_index},
    {"__newindex", tle_newindex},
    {0, 0}
};

int luaopen_tle(lua_State *L)
{
    luaL_newlib(L, tle_methods); /* load the library */
    lua_setglobal(L, "tle"); /* store under the global tle */
    luaL_newmetatable(L, LUA_TLE); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, tle_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */
    return 1;
}
