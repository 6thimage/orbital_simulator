#ifndef _MISC_H_
#define _MISC_H_

#include <stdint.h>
#include <stdio.h>

/* reads a line from f into buf until \n or buf_len.
 * return -1 on error, 0 on EOF or the number of characters read
 */
int32_t nreadline(FILE *f, char *buf, uint32_t buf_len);

#endif /* !_MISC_H_ */
