#ifndef _COORDS_H_
#define _COORDS_H_

#include <math.h>

/* earth centred, earth fixed (ECEF) systems */
typedef enum{
    ECEF_WGS84,
    ECEF_ITRF96
} ecef_system;

/*** macros ***/
#define COORD_DEG_TO_RAD(d) ((d)*(M_PI/180.))
#define COORD_RAD_TO_DEG(r) ((r)*(180./M_PI))

/*** functions ***/
/* ecef and eci variables are [x, y, z]
 * lla is [latitude, longitude, altitude]
 */

/* lla <-> ecef */
int coord_lla_to_ecef(double *lla, double *ecef, ecef_system sys);
int coord_ecef_to_lla(double *ecef, double *lla, ecef_system sys);

/* convert ecef_in to new system sys and place in ecef_out *
int coord_ecef_convert(double *ecef_in, double *ecef_out, ecef_system sys); */

/* eci -> ecef */
int coord_eci_to_ecef(double *eci, double *ecef, int year, double days, double d_eps, double d_psi,
                      double xp, double yp);

/* teme <-> eci */
int coord_teme_to_eci(int n, double *teme, double *eci, int year, double days, double d_eps, double d_psi);
int coord_eci_to_teme(int n, double *eci, double *teme, int year, double days, double d_eps, double d_psi);

/* calculation of precession and nutation angles */
void precession_angles(double tt, double *zeta, double *theta, double *z);
void nutation_angles(double tt, double d_eps, double d_psi,
                     double *epsilon_bar, double *delta_psi, double *epsilon);

#endif /* !_COORDS_H_ */
