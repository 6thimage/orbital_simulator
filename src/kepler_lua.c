#include "lua_wrapper.h"
#include "kepler.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

static int lua_kepler_single(lua_State *L)
{
    tle_entry *tle=luaL_checkudata(L, 1, LUA_TLE);
    double since=luaL_checknumber(L, 2);
    double r[3], v[3];

    /* run kepler */
    kepler(tle, since, r, v);

    /* push result */
    /* r */
    lua_newtable(L);
    lua_pushstring(L, "x");
    lua_pushnumber(L, r[0]);
    lua_settable(L, -3);
    lua_pushstring(L, "y");
    lua_pushnumber(L, r[1]);
    lua_settable(L, -3);
    lua_pushstring(L, "z");
    lua_pushnumber(L, r[2]);
    lua_settable(L, -3);
    /* v */
    lua_newtable(L);
    lua_pushstring(L, "x");
    lua_pushnumber(L, v[0]);
    lua_settable(L, -3);
    lua_pushstring(L, "y");
    lua_pushnumber(L, v[1]);
    lua_settable(L, -3);
    lua_pushstring(L, "z");
    lua_pushnumber(L, v[2]);
    lua_settable(L, -3);

    return 2;
}

static const luaL_Reg kepler_methods[] = {
    {"single", lua_kepler_single},
    {0, 0}
};

int luaopen_kepler(lua_State *L)
{
    luaL_newlib(L, kepler_methods);
    lua_setglobal(L, "kepler");

    return 1;
}
