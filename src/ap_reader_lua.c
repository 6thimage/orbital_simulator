#include "lua_wrapper.h"
#include "ap_reader.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <string.h>

/* methods */
static int lua_ap_file_read(lua_State *L)
{
    const char *filename = luaL_checkstring(L, 1);
    lua_ap_data data, *lua_data;
    int i;

    i=ap_file_read(filename, data.ap_day, data.ap_3hour, &data.num);
    if(i<0)
    {
        puts("Failed to process AP file");
        lua_pushinteger(L, 0);
        return 1;
    }

    lua_data=lua_newuserdata(L, sizeof(lua_ap_data));
    memcpy(lua_data, &data, sizeof(lua_ap_data));
    luaL_setmetatable(L, LUA_AP_DATA);

    return 1;
}

/* meta */
static int ap_tostring(lua_State *L)
{
    lua_ap_data *data=luaL_checkudata(L, 1, LUA_AP_DATA);
    lua_pushfstring(L, "AP data - %d (day) values", data->num);
    return 1;
}

static int ap_length(lua_State *L)
{
    lua_ap_data *data=luaL_checkudata(L, 1, LUA_AP_DATA);
    lua_pushinteger(L, data->num);
    return 1;
}

static const luaL_Reg ap_methods[]={
    {"file_read", lua_ap_file_read},
    {0, 0}
};

static const luaL_Reg ap_meta[]={
    {"__tostring", ap_tostring},
    {"__len", ap_length},
    {0, 0}
};

int luaopen_ap_reader(lua_State *L)
{
    luaL_newlib(L, ap_methods); /* lad library methods */
    lua_setglobal(L, "ap"); /* store under global 'ap' */
    luaL_newmetatable(L, LUA_AP_DATA); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, ap_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */

    return 1;
}
