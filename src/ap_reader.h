#ifndef _AP_READER_H_
#define _AP_READER_H_

/* reads in a kp/ap data file - e.g. from ftp://ftp.ngdc.noaa.gov/STP/GEOMAGNETIC_DATA/INDICES/KP_AP
 * arguments:
 *   filename   name of the kp/ap data file
 *   ap_day     pointer to an array of doubles that is at least 366 in length
 *   ap_3hour   pointer to an array of doubles that is at least 2928 (8*366) in length
 *   num_read   used to return the number of values read into ap_day
 *
 * returns:
 *   0  success
 *   -1 if any of the ap_day, ap_3hour or num_read pointers are zero
 *   -2 failed to open file
 *   >0 number of lines skipped because they were too short (less than 72 characters)
 */
int ap_file_read(const char *filename, double *ap_day, double *ap_3hour, int *num_read);

#endif /* ! _AP_READER_H_ */
