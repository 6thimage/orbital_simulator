#ifndef _ORBIT_INTEGRATOR_H_
#define _ORBIT_INTEGRATOR_H_

#include "sat_object.h"
#include "nrlmsise.h"

typedef int (*int_accel_fn)(double *, double *, double *, void *);

typedef struct{
    sat_object *sat;
    /* atmospheric data */
    nrlmsise_data *atmos_data;
    struct nrlmsise_input atmos_input;
    struct ap_array AP;
    /* time data */
    int year, day, hour;
    double sec;
    /* EOP data */
    double d_eps, d_psi;
    double xp, yp;
    /* atmospheric data cache */
    double atmos_cache_position[3];
    double atmos_cache_temperature, atmos_cache_density;
} drag_opt_type;

int orb_int_run(double *pos, double *vel, double time, double step_size);

int orb_int_drag_run(double *pos, double *vel, double time, double step_size, drag_opt_type *opt);

#endif /* ! _ORBIT_INTEGRATOR_H_ */
