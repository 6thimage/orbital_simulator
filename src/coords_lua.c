#include "lua_wrapper.h"
#include "coords.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

static int lua_lla_to_ecef(lua_State *L)
{
    int ret;
    ecef_system sys;
    double lla[3], ecef[3];

    luaL_checktype(L, 1, LUA_TTABLE);
    sys=luaL_checknumber(L, 2);

    /* get lla values */
    lua_getfield(L, 1, "lat");
    lua_getfield(L, 1, "long");
    lua_getfield(L, 1, "alt");
    lla[0]=luaL_checknumber(L, -3);
    lla[1]=luaL_checknumber(L, -2);
    lla[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* perform conversion */
    ret=coord_lla_to_ecef(lla, ecef, sys);

    if(!ret)
    {
        /* return ecef as table */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, ecef[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, ecef[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, ecef[2]);
        lua_settable(L, -3);

        return 1;
    }

    lua_pushinteger(L, ret);
    return 1;
}

static int lua_ecef_to_lla(lua_State *L)
{
    int ret;
    ecef_system sys;
    double ecef[3], lla[3];

    luaL_checktype(L, 1, LUA_TTABLE);
    sys=luaL_checknumber(L, 2);

    /* get ecef values */
    lua_getfield(L, 1, "x");
    lua_getfield(L, 1, "y");
    lua_getfield(L, 1, "z");
    ecef[0]=luaL_checknumber(L, -3);
    ecef[1]=luaL_checknumber(L, -2);
    ecef[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* perform conversion */
    ret=coord_ecef_to_lla(ecef, lla, sys);

    if(!ret)
    {
        /* return lla as table */
        lua_newtable(L);
        lua_pushstring(L, "lat");
        lua_pushnumber(L, lla[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "long");
        lua_pushnumber(L, lla[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "alt");
        lua_pushnumber(L, lla[2]);
        lua_settable(L, -3);

        return 1;
    }

    lua_pushinteger(L, ret);
    return 1;
}

static int lua_eci_to_ecef(lua_State *L)
{
    int ret, year, day;
    double days, eci[3], ecef[3];
    lua_eop_data *eop_data;

    luaL_checktype(L, 1, LUA_TTABLE);
    luaL_checktype(L, 2, LUA_TTABLE);
    year=luaL_checkinteger(L, 3);
    days=luaL_checknumber(L, 4);

    /* check transform is within EOP data */
    lua_geti(L, 1, year);
    if(lua_isnil(L, -1))
        return luaL_error(L, "requested year is not in EOP data (%d)", year);
    /* get year values */
    eop_data=luaL_checkudata(L, -1, LUA_EOP_DATA);
    lua_pop(L, 1);
    /* look for day values */
    day=(int)days;
    if(day>eop_data->num)
        return luaL_error(L, "requested day is not in EOP data (%d > %d)", day, eop_data->num);
    --day;

    /* get eci values */
    lua_getfield(L, 2, "x");
    lua_getfield(L, 2, "y");
    lua_getfield(L, 2, "z");
    eci[0]=luaL_checknumber(L, -3);
    eci[1]=luaL_checknumber(L, -2);
    eci[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* perform conversion */
    ret=coord_eci_to_ecef(eci, ecef, year, days,
                          eop_data->deps[day], eop_data->dpsi[day],
                          eop_data->xp[day], eop_data->yp[day]);

    if(!ret)
    {
        /* return ecef as table */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, ecef[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, ecef[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, ecef[2]);
        lua_settable(L, -3);

        return 1;
    }

    lua_pushinteger(L, ret);
    return 1;
}

static int lua_teme_to_eci(lua_State *L)
{
    int ret, year, day;
    double days, teme[3], eci[3];
    lua_eop_data *eop_data;

    luaL_checktype(L, 1, LUA_TTABLE);
    luaL_checktype(L, 2, LUA_TTABLE);
    year=luaL_checkinteger(L, 3);
    days=luaL_checknumber(L, 4);

    /* check transform is within EOP data */
    lua_geti(L, 1, year);
    if(lua_isnil(L, -1))
        return luaL_error(L, "requested year is not in EOP data (%d)", year);
    /* get year values */
    eop_data=luaL_checkudata(L, -1, LUA_EOP_DATA);
    lua_pop(L, 1);
    /* look for day values */
    day=(int)days;
    if(day>eop_data->num)
        return luaL_error(L, "requested day is not in EOP data (%d > %d)", day, eop_data->num);
    --day;

    /* get eci values */
    lua_getfield(L, 2, "x");
    lua_getfield(L, 2, "y");
    lua_getfield(L, 2, "z");
    teme[0]=luaL_checknumber(L, -3);
    teme[1]=luaL_checknumber(L, -2);
    teme[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* perform conversion */
    ret=coord_teme_to_eci(1, teme, eci, year, days, eop_data->deps[day], eop_data->dpsi[day]);

    if(!ret)
    {
        /* return eci as table */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, eci[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, eci[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, eci[2]);
        lua_settable(L, -3);

        return 1;
    }

    lua_pushinteger(L, ret);
    return 1;
}

static int lua_eci_to_teme(lua_State *L)
{
    int ret, year, day;
    double days, eci[3], teme[3];
    lua_eop_data *eop_data;

    luaL_checktype(L, 1, LUA_TTABLE);
    luaL_checktype(L, 2, LUA_TTABLE);
    year=luaL_checkinteger(L, 3);
    days=luaL_checknumber(L, 4);

    /* check transform is within EOP data */
    lua_geti(L, 1, year);
    if(lua_isnil(L, -1))
        return luaL_error(L, "requested year is not in EOP data (%d)", year);
    /* get year values */
    eop_data=luaL_checkudata(L, -1, LUA_EOP_DATA);
    lua_pop(L, 1);
    /* look for day values */
    day=(int)days;
    if(day>eop_data->num)
        return luaL_error(L, "requested day is not in EOP data (%d > %d)", day, eop_data->num);
    --day;

    /* get eci values */
    lua_getfield(L, 2, "x");
    lua_getfield(L, 2, "y");
    lua_getfield(L, 2, "z");
    eci[0]=luaL_checknumber(L, -3);
    eci[1]=luaL_checknumber(L, -2);
    eci[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* perform conversion */
    ret=coord_eci_to_teme(1, eci, teme, year, days, eop_data->deps[day], eop_data->dpsi[day]);

    if(!ret)
    {
        /* return eci as table */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, teme[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, teme[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, teme[2]);
        lua_settable(L, -3);

        return 1;
    }

    lua_pushinteger(L, ret);
    return 1;
}

static const luaL_Reg coords_methods[] = {
    {"lla_to_ecef", lua_lla_to_ecef},
    {"ecef_to_lla", lua_ecef_to_lla},
    {"eci_to_ecef", lua_eci_to_ecef},
    {"teme_to_eci", lua_teme_to_eci},
    {"eci_to_teme", lua_eci_to_teme},
    {0, 0}
};

int luaopen_coords(lua_State *L)
{
    luaL_newlib(L, coords_methods);
    lua_pushinteger(L, ECEF_WGS84);
    lua_setfield(L, -2, "ECEF_WGS84");
    lua_pushinteger(L, ECEF_ITRF96);
    lua_setfield(L, -2, "ECEF_ITRF96");
    lua_setglobal(L, "coords");

    return 1;
}
