#define _GNU_SOURCE
#include "coords.h"

#include "date.h"
#include "vec3.h"
#include <math.h>

int coord_ecef_model(ecef_system sys, double *a, double *e_2)
{
    int ret=0;

    /* Derived values in this function are calculated using arbitrary precision,
     * in particular bc with scale=50, to avoid float/double rounding errors.
     */
    switch(sys)
    {
    /* WGS 84 parameters from http://earth-info.nga.mil/GandG/coordsys/csat_pubs.html
     * (Datums, Ellipsoids, Grids and Grid Reference Systems - TM8358.1)
     * Chapter 2.5 Projections
     *
     * semi-major axis (a) = 6378137 m
     * semi-minor axis (b) = 6356752.3142 m
     * inverse flattening (1/f) = 298.257223563
     *
     * derived - e^2 = (a^2 - b^2)/a^2
     *               = .00669438000426080651558348727666635271835128297130 
     */
    case ECEF_WGS84:
        *a=6378137.;
        *e_2=.00669438000426080651558348727666635271835128297130;
        break;

    /* ITRF-96 parameters from IERS Conventions July 1996 - Technical Note 21
     *
     * semi major axis (a) = 6378136.49 m
     * inverse flattening (1/f) = 298.25645
     *
     * derived - e^2 = (a^2 - b^2)/a^2
     *               = 2*f - f^2
     *               = .00669439732364328003992468314303390939803861205522
     */
    case ECEF_ITRF96:
        *a=6378136.49;
        *e_2=.00669439732364328003992468314303390939803861205522;
        break;
    default:
        ret=-1;
        break;
    }

    return ret;
}

int coord_lla_to_ecef(double *lla, double *ecef, ecef_system sys)
{
    double sin_lat, sin_long, cos_lat, cos_long;
    double a, e_2, N;

    /* error if missing lla or ecef */
    if(!lla || !ecef)
        return -1;

    /* get model parameters */
    if(coord_ecef_model(sys, &a, &e_2)!=0)
        return -2;

    /* calculate trig functions */
    sincos(lla[0], &sin_lat, &cos_lat);
    sincos(lla[1], &sin_long, &cos_long);

    /* calculate N */
    N=a/sqrt(1.-(e_2*sin_lat*sin_lat));

    /* calculate ecef */
    ecef[0]=(N+lla[2])*cos_lat*cos_long;
    ecef[1]=(N+lla[2])*cos_lat*sin_long;
    ecef[2]=(N*(1.-e_2)+lla[2])*sin_lat;

    return 0;
}

int coord_ecef_to_lla(double *ecef, double *lla, ecef_system sys)
{
    double a, e_2;
    double deltaz, sin_lat, last_lat;
    unsigned int i;

    /* error if missing arguments */
    if(!ecef || !lla)
        return -1;

    /* get model parameters */
    if(coord_ecef_model(sys, &a, &e_2)!=0)
        return -2;

    /* calculate longitude */
    lla[1]=atan2(ecef[1], ecef[0]); /* long = arctan(y/x) */

    /* As the latitude and height depend on each other, they have to be found
     * iteratively. There are two methods,
     *     tan(lat)=[z/sqrt(x^2 + y^2)] * 1/[1-e^2(N/(N+h)]
     *     h=[sqrt(x^2 + y^2)/cos(lat)] -N
     * and
     *     tan(lat)=sqrt(x^2 + y^2)/(z + deltaz)
     *     deltaz=e^2 * N * sin(lat)
     * the first method (according to Xu pg. 5 and my own tests) is not numerically
     * stable for large lat or h. The second method (referenced from an unattainable
     * source in Xu) is stable and converges with 6-8 iterations.
     */
    /* generate first guess - for latitude assume deltaz=0 */
    lla[0]=atan2(ecef[2],sqrt((ecef[0]*ecef[0])+(ecef[1]*ecef[1])));
    sin_lat=sin(lla[0]);
    deltaz=(a*e_2*sin_lat)/sqrt(1.-(e_2*sin_lat*sin_lat));

    /* set last latitude to known value */
    last_lat=1.;

    /* iterate */
    for(i=0; i<10; ++i)
    {
        /* update values */
        lla[0]=atan2((ecef[2]+deltaz),sqrt((ecef[0]*ecef[0])+(ecef[1]*ecef[1])));
        sin_lat=sin(lla[0]);
        deltaz=(a*e_2*sin_lat)/sqrt(1.-(e_2*sin_lat*sin_lat));

        /* check for convergence - tolerance of 1e-16 is excessive,
         * 1e-11 will give better than 10 cm convergence.
         */
        if(fabs((lla[0]/last_lat)-1.)<1e-16)
            break;
        last_lat=lla[0];
    }

    /* i will equal the maximum number of loops if convergence has failed */
    if(i==10)
        return -3;

    /* calculate h */
    lla[2]=sqrt((ecef[0]*ecef[0])+(ecef[1]*ecef[1])+((ecef[2]+deltaz)*(ecef[2]+deltaz)))
        -(a/sqrt(1.-(e_2*sin_lat*sin_lat)));

    return 0;
}

//int coord_teme_to_ecef(coord_teme *teme, coord_ecef *ecef)
//{
//    /* Earth rotation rate from "Fundamentals of Astrodynamics and Applications"
//     * D. A. Vallado, 4th ed, page 222
//     */
//    const double omega=7.292115146706979e-5;
//
//    double gmst;
//
//    /* check arguments */
//    if(!teme || !ecef)
//        return -1;
//
//    gmst=gstime(teme->time);
//
//    /* excess length of day - latest value is 0.87 ms (for 2008)
//     * http://www.iers.org/IERS/EN/Science/EarthRotation/LODsince1623.html
//     */
//
//}

int coord_eci_to_ecef(double *eci, double *ecef, int year, double days, double d_eps, double d_psi,
                      double xp, double yp)
{
    /* time */
    timedate td;
    double jd, tt;
    /* nutation angles */
    double epsilon_bar, delta_psi, epsilon;
    /* precession angles */
    double zeta, theta, z;
    /* earth rotation angle */
    double gast, moon_laan;
    /* temporary vectors */
    double ta[3], tb[3];

    /* check arguments */
    if(!eci || !ecef)
        return -1;

    /* calculate time */
    days_hms(year, days, &td);
    jd=julian_days(&td);
    tt=julian_centuries(jd);

    /* calculate nutation */
    nutation_angles(tt, d_eps, d_psi, &epsilon_bar, &delta_psi, &epsilon);

    /* calculate precession */
    precession_angles(tt, &zeta, &theta, &z);

    /* calculate earth rotation */
    moon_laan=    2.18243862436099433544513937493402521081945270374929
              -  33.75704593375351192447177549130833553240691886030886*tt
              +    .00003614285992671590832212809633078502160754613869*tt*tt
              +    .00000003878509448876287948719312818863583807650826*tt*tt*tt;
    moon_laan=fmod(moon_laan, 2.*M_PI);

    gast=gmstime(tt);
    gast+=delta_psi*cos(epsilon_bar)
          +.00000001279908118129175023077373230224982656524772*sin(moon_laan)
          +.00000000030543261909900767596164588448550722485250*sin(2.*moon_laan);

    /* apply precession rotations */
    vec3_r3(ta, eci, -zeta);
    vec3_r2(tb, ta, theta);
    vec3_r3(ta, tb, -z);

    /* apply nutation rotations */
    vec3_r1(tb, ta, epsilon_bar);
    vec3_r3(ta, tb, -delta_psi);
    vec3_r1(tb, ta, -epsilon);

    /* apply earth rotation */
    vec3_r3(ta, tb, gast);

    /* apply polar motion */
    vec3_r1(tb, ta, -yp);
    vec3_r2(ecef, tb, -xp);

    return 0;
}

int coord_teme_to_eci(int n, double *teme, double *eci, int year, double days, double d_eps, double d_psi)
{
    int i;
    /* time */
    timedate td;
    double jd, tt;
    /* nutation angles */
    double epsilon_bar, delta_psi, epsilon;
    /* precession angles */
    double zeta, theta, z;
    /* temporary vectors */
    double ta[3], tb[3];

    /* check arguments */
    if(!teme || !eci)
        return -1;

    /* calculate time */
    days_hms(year, days, &td);
    jd=julian_days(&td);
    tt=julian_centuries(jd);

    /* calculate nutation */
    nutation_angles(tt, d_eps, d_psi, &epsilon_bar, &delta_psi, &epsilon);

    /* calculate precession */
    precession_angles(tt, &zeta, &theta, &z);

    for(i=0; i<n; ++i)
    {
        /* apply nutation rotations */
        vec3_r1(ta, &teme[i*3], epsilon);
        vec3_r3(tb, ta, delta_psi);
        vec3_r1(ta, tb, -epsilon_bar);

        /* apply precession rotations */
        vec3_r3(tb, ta, z);
        vec3_r2(ta, tb, -theta);
        vec3_r3(&eci[i*3], ta, zeta);
    }

    return 0;
}

int coord_eci_to_teme(int n, double *eci, double *teme, int year, double days, double d_eps, double d_psi)
{
    int i;
    /* time */
    timedate td;
    double jd, tt;
    /* nutation angles */
    double epsilon_bar, delta_psi, epsilon;
    /* precession angles */
    double zeta, theta, z;
    /* temporary vectors */
    double ta[3], tb[3];

    /* check arguments */
    if(!teme || !eci)
        return -1;

    /* calculate time */
    days_hms(year, days, &td);
    jd=julian_days(&td);
    tt=julian_centuries(jd);

    /* calculate nutation */
    nutation_angles(tt, d_eps, d_psi, &epsilon_bar, &delta_psi, &epsilon);

    /* calculate precession */
    precession_angles(tt, &zeta, &theta, &z);

    for(i=0; i<n; ++i)
    {
        /* apply precession rotations */
        vec3_r3(ta, &eci[i*3], -zeta);
        vec3_r2(tb, ta, theta);
        vec3_r3(ta, tb, -z);

        /* apply nutation rotations */
        vec3_r1(tb, ta, epsilon_bar);
        vec3_r3(ta, tb, -delta_psi);
        vec3_r1(&teme[i*3], ta, -epsilon);
    }

    return 0;
}

void precession_angles(double tt, double *zeta, double *theta, double *z)
{
    double tt2=tt*tt, tt3=tt*tt*tt;

    /* angle    tt term     tt^2 term   tt^3 term
     * zeta     2306.2181"   0.30188"    0.017998"
     * theta    2004.3109"  -0.42665"   -0.041833"
     * z        2306.2181"   1.09468"    0.018203"
     */
    *zeta= .01118086086502439991018543880303152301008906795067*tt
          +.00000146355554053346725744923269219817334981703934*tt2
          +.00000008725676632609428812631274014238347671262446*tt3;
    *theta= .00971717345516967085894594965419750829842256847449*tt
           -.00000206845757045383531665136851771018503941778136*tt2
           -.00000020281210721855219219846876643940037678182127*tt3;
    *z= .01118086086502439991018543880303152301008906795067*tt
       +.00000530715840436986861463007169569198490319900832*tt2
       +.00000088250634372368836913172064052217270063334991*tt3;
}

#include "nutation_data.h"
void nutation_angles(double tt, double d_eps, double d_psi,
                     double *epsilon_bar, double *delta_psi, double *epsilon)
{
    int i;
    double tt2=tt*tt, tt3=tt*tt*tt;
    double moon_anom, sun_anom, moon_diff, moon_elon, moon_laan;
    double delta_epsilon;

    /* angle        tt^0 term   tt term     tt^2 term   tt^3 term
     * epsilon_bar  84381.448"  -46.8150"   -0.00059"   0.001813"
     */
    *epsilon_bar= .40909280422232893747235670152583864519866276422341
                 -.00022696552481142927539911828701887334494396679833*tt
                 -.00000000286040071854626236218049320391189305814248*tt2
                 +.00000000878967203851588756378514267574959680408868*tt3;

   /* from IERS TN13
    *
    * angle             tt^0 term           tt term             tt^2 term   tt^3 term
    * moon anomaly      134.96298139        1325r + 198.8673981  0.0086972   0.000017777
    * sun anomaly       357.52772333          99r + 359.0503400 -0.0001603  -0.000003333
    * moon long - LAAN   93.27191028        1342r +  82.0175353 -0.0036825   0.000003056
    * moon elongation   297.85036306        1236r + 307.1114800 -0.0019142   0.000005278
    * moon LAAN         125.04452222       -(  5r + 134.1362608) 0.0020708   0.000002222
    *
    * r = 360 deg
    */

    moon_anom=    2.35554839354394068351440506663283779661875929617487
              +8328.69142288389569562539658995472197935673583983369765*tt
              +    .00015179516355539571959300210544827351127193421897*tt2
              +    .00000031028075591010303589754502550908670461206611*tt3;

    sun_anom=   6.24003593932602279880523697521750777695619048038397
             +628.30195602418415834226846313414854787993939829758328*tt
             -   .00000279737494000202268301380437060535982126815855*tt2
             -   .00000005817764173314431923078969228295375711476239*tt3;

    moon_diff=    1.62790193397196109918700555816214823391047312885879
              +8433.46615826997287630359894065742179911204460483503200*tt
              -    .00006427174970469118667021491254959316317253375729*tt2
              +    .00000005332950492204895929489055125937427735519886*tt3;

    moon_elon=    5.19846951357992274650264917412065078536963966955703
              +7771.37714617064160055942252865665988187112531806343581*tt
              -    .00003340851076525812531828098079348619502315230606*tt2
              +    .00000009211459941081183878208367944801011543170712*tt3;

    moon_laan=    2.18243862436099433544513937493402521081945270374929
              -  33.75704593375351192447177549130833553240691886030886*tt
              +    .00003614285992671590832212809633078502160754613869*tt2
              +    .00000003878509448876287948719312818863583807650826*tt3;

    *delta_psi=0.;
    delta_epsilon=0.;

    for(i=0; i<106; ++i)
    {
        double a, s, c;
        const int *mult_row=&nutation_multipliers[i*5];
        const double *long_row=&nutation_longitude[i*2],
                     *obli_row=&nutation_obliquity[i*2];

        /* calculate angle */
        a= mult_row[0]*moon_anom
          +mult_row[1]*sun_anom
          +mult_row[2]*moon_diff
          +mult_row[3]*moon_elon
          +mult_row[4]*moon_laan;

        /* reduce angle to within +/- 2*PI */
        a=fmod(a, 2.*M_PI);
        sincos(a, &s, &c);

        *delta_psi   +=(long_row[0]+long_row[1]*tt)*s;
        delta_epsilon+=(obli_row[0]+obli_row[1]*tt)*c;
    }

    /* add EOP corrections */
    *delta_psi+=d_psi;
    delta_epsilon+=d_eps;

    /* true obliquity of ecliptic */
    *epsilon=*epsilon_bar + delta_epsilon;

    /* reduce to with +/- 2*PI */
    *delta_psi=fmod(*delta_psi, 2.*M_PI);
    *epsilon=fmod(*epsilon, 2.*M_PI);
}
