#include "orbit_integrator.h"

#include "coords.h"
#include "nrlmsise.h"
#include "vec3.h"
#include "quat.h"
#include "drag.h"

#include <stdint.h>
#include <math.h>
#include <stdio.h>

#if 0
/* wgs 72 constants */
#define mu (398600.8)
#define J2 (0.001082616)
#define J3 (-0.00000253881)
#define J4 (-0.00000165597)
#define R (6378.135)
#else
/* wgs 84 constants */
#define mu (398600.5)
#define J2 (0.00108262998905)
#define J3 (-0.00000253215306)
#define J4 (-0.00000161098761)
#define R (6378.137)
#endif

static int grav_accel(double *pos, double *vel, double *acc, void *opt)
{
    #pragma unused(vel, opt)

    unsigned int i;
    double r, r2=0., r3, grav_const, harm_const, z_hat, z_hat2, z_hat4, jacc[3];

    for(i=0; i<3; ++i)
        r2+=pos[i]*pos[i];

    r=sqrt(r2);
    r3=r2*r;

    grav_const=-mu/r3;

    for(i=0; i<3; ++i)
        acc[i]=grav_const*pos[i];

    z_hat=pos[2]/r;
    z_hat2=z_hat*z_hat;
    z_hat4=z_hat2*z_hat2;

    /* J4 */
    harm_const=35.*mu*J4*R*R*R*R/(8.*r*r3*r3);

    jacc[0]=harm_const*pos[0]*((3./7.) -6.*z_hat2 +9.*z_hat4);
    jacc[1]=harm_const*pos[1]*((3./7.) -6.*z_hat2 +9.*z_hat4);
    jacc[2]=harm_const*pos[2]*((15./7.) -10.*z_hat2 +9.*z_hat4);

    /* J3 */
    harm_const=-5.*mu*J3*R*R*R/(2.*r2*r3);

    jacc[0]+=harm_const*pos[0]*(3. -7.*z_hat2)*z_hat/r;
    jacc[1]+=harm_const*pos[1]*(3. -7.*z_hat2)*z_hat/r;
    jacc[2]+=harm_const*(-(3./5.) +6.*z_hat2 -7.*z_hat4);

    /* J2 */
    harm_const=-3.*mu*J2*R*R/(2.*r2*r3);

    jacc[0]+=harm_const*pos[0]*(1. -5.*z_hat2);
    jacc[1]+=harm_const*pos[1]*(1. -5.*z_hat2);
    jacc[2]+=harm_const*pos[2]*(3. -5.*z_hat2);

    for(i=0; i<3; ++i)
        acc[i]+=jacc[i];

    return 0;
}

/* nrlmsise flags - output in metres [0], using daily ap (ap_a struct) [9] */
static struct nrlmsise_flags atmos_flags={.switches={1, 1, 1, 1, 1, 1, 1, 1, 1, -1,
                                                     1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                                     1, 1, 1, 1}};

static int drag_accel(double *pos, double *vel, double *acc, void *opt)
{
    int i;
    double days, eci[3], ecef[3], lla[3];
    double cache_diff[3], cache_diff_mag;
    struct nrlmsise_output atmos_output;
    double atmos_temperature, atmos_density;
    sat_object *sat_obj;
    double q_vel_rot[4], q_obj_conj_rot[4];
    double unit_vel[3], vel_mag, virt_vel[3]={0., 0., 1.}, vel_obj_frame[3];
    double acc_obj[3], virt_acc[3];

    /* sanity check */
    if(!opt)
        return -1;

    /* reset acceleration */
    for(i=0; i<3; ++i)
    {
        acc[i]=0.;
        acc_obj[i]=0.;
    }

    /* convert option pointer to correct type */
    drag_opt_type *drag_opt=opt;
    sat_obj=drag_opt->sat;

    /* calculate ecef position */
    for(i=0; i<3; ++i)
        eci[i]=pos[i]*1000.;
    days=drag_opt->day + drag_opt->hour/24. + drag_opt->sec/(24*60*60);
    coord_eci_to_ecef(eci, ecef, drag_opt->year, days,
                      drag_opt->d_eps, drag_opt->d_psi,
                      drag_opt->xp, drag_opt->yp);

    /* calculate difference between position and cache */
    vec3_sub(cache_diff, drag_opt->atmos_cache_position, ecef);
    vec3_mag(&cache_diff_mag, cache_diff);

    /* if the difference is below the cache threshold */
    if(cache_diff_mag<=10.)
    {
        /* use the cached data */
        atmos_temperature=drag_opt->atmos_cache_temperature;
        atmos_density=drag_opt->atmos_cache_density;
    }
    else
    {
        /* otherwise generate new atmospheric data */

        /* convert to lla */
        i=coord_ecef_to_lla(ecef, lla, ECEF_WGS84);
        if(i<0)
            return -2;

        /* calculate atmospheric density from model */
        i=nrlmsise_input_update(&drag_opt->atmos_input, drag_opt->hour*3600. + drag_opt->sec, lla[0], lla[1]);
        if(i<0)
            return -3;
        drag_opt->atmos_input.alt=lla[2]/1000.; /* lla[2] is m, alt is km */
        gtd7d(&drag_opt->atmos_input, &atmos_flags, &atmos_output);
        /* nan check */
        if(isnan(atmos_output.t[1]))
        {
            printf("integrate temperature nan - %e %e %e %e\n", lla[0], lla[1], lla[2],
                    drag_opt->hour*3600. + drag_opt->sec);
            return -4;
        }
        if(isnan(atmos_output.d[5]))
        {
            printf("integrate density nan - %e %e %e %e\n", lla[0], lla[1], lla[2],
                    drag_opt->hour*3600. + drag_opt->sec);
            return -5;
        }
        atmos_temperature=atmos_output.t[1];
        atmos_density=atmos_output.d[5]/1000.; /* convert to kg/m^3 */

        /* store in cache */
        for(i=0; i<3; ++i)
            drag_opt->atmos_cache_position[i]=ecef[i];
        drag_opt->atmos_cache_temperature=atmos_temperature;
        drag_opt->atmos_cache_density=atmos_density;
    }

    /* calculate rotation needed to transform the virtual unit vector into the velocity */
    vec3_normalise(unit_vel, vel);
    quat_get_rotation(q_vel_rot, unit_vel, virt_vel);

    /* the velocity always enters the object frame along (0, 0, 1)
     * to make the object's pitch, yaw and roll rotations make more sense
     * so we take the velocity magnitude and apply it to this unit vector
     */
    vec3_mag(&vel_mag, vel);
    vel_mag*=1000.; /* convert velocity into m/s */
    vec3_mul_s(virt_vel, virt_vel, vel_mag);

    /* we then rotate this virtual velocity into the object frame */
    quat_conj(q_obj_conj_rot, sat_obj->rotation);
    quat_mul_vec(vel_obj_frame, q_obj_conj_rot, virt_vel);

    /* iterate over each face */
    for(i=0; i<sat_obj->num_faces; ++i)
    {
        double dot, pressure, a[3];
        sat_face *face=&sat_obj->faces[i];

        /* magnitude in directon of face's normal */
        vec3_dot(&dot, vel_obj_frame, face->normal);

        /* backface culling */
        if(dot<=0)
            continue;

        /* calculate pressure on face */
        pressure=(2. -.9)*fmd_pressure(dot, atmos_temperature, atmos_density);
        pressure+=.9*fmd_reflected_pressure(dot, atmos_temperature, 293., atmos_density);

        /* acceleration = pressure * area / mass
         * this will be in m/s^2, so we divide by 1000 to get km/s^2
         */
        vec3_mul_s(a, face->normal, -(pressure*face->area/sat_obj->mass)/1000.);
        vec3_add(acc_obj, acc_obj, a);
    }

    /* rotate virtual acceleration into world frame */
    quat_mul_vec(virt_acc, sat_obj->rotation, acc_obj);

    /* apply previously stored virtaul-velocity rotation */
    quat_mul_vec(acc, q_vel_rot, virt_acc);

    return 0;
}

static int accel_grav_drag(double *pos, double *vel, double *acc, void *opt)
{
    int i;
    double a[3];

    grav_accel(pos, vel, acc, 0);

    i=drag_accel(pos, vel, a, opt);
    if(i<0)
        return i;

    for(i=0; i<3; ++i)
        acc[i]+=a[i];

    return 0;
}

/* Runge-Kutta 4th order */
#if 0
static int integrate(double *pos, double *vel, int_accel_fn accel, void *opt, double step_size)
{
    int i;
    double step_size_2=step_size/2.;
    double ptemp[3];
    double kv0[3], kv1[3], kv2[3], kv3[3];
    double ka0[3], ka1[3], ka2[3], ka3[3];

    /* first step */
    for(i=0; i<3; ++i)
        kv0[i]=vel[i];
    i=accel(pos, kv0, ka0, opt);
    if(i<0)
        return i;

    /* second step */
    for(i=0; i<3; ++i)
        kv1[i]=vel[i] +step_size_2*ka0[i];
    for(i=0; i<3; ++i)
        ptemp[i]=pos[i] +step_size_2*kv0[i];
    i=accel(ptemp, kv1, ka1, opt);
    if(i<0)
        return i;

    /* third step */
    for(i=0; i<3; ++i)
        kv2[i]=vel[i] +step_size_2*ka1[i];
    for(i=0; i<3; ++i)
        ptemp[i]=pos[i] +step_size_2*kv1[i];
    i=accel(ptemp, kv2, ka2, opt);
    if(i<0)
        return i;

    /* fourth step */
    for(i=0; i<3; ++i)
        kv3[i]=vel[i] +step_size*ka2[i];
    for(i=0; i<3; ++i)
        ptemp[i]=pos[i] +step_size*kv2[i];
    i=accel(ptemp, kv3, ka3, opt);
    if(i<0)
        return i;

    /* produce new position and velocity */
    for(i=0; i<3; ++i)
        vel[i]+=step_size*((ka0[i] + ka3[i])/6. +(ka1[i] + ka2[i])/3.);
    for(i=0; i<3; ++i)
        pos[i]+=step_size*((kv0[i] + kv3[i])/6. +(kv1[i] + kv2[i])/3.);

    return 0;
}
#else

/* leap frog integration
 * 2nd order method, stable if step size is less than 2/w, where w is the
 * oscillation frequency
 */
static int integrate(double *pos, double *vel, int_accel_fn accel, void *opt, double step_size)
{
    int i;
    double a[3], a_n[3];

    i=accel(pos, vel, a, opt);
    if(i<0)
        return i;
    for(i=0; i<3; ++i)
        pos[i]+=step_size*vel[i] + 0.5*step_size*step_size*a[i];

    i=accel(pos, vel, a_n, opt);
    if(i<0)
        return i;
    for(i=0; i<3; ++i)
        vel[i]+=0.5*step_size*(a[i] + a_n[i]);

    return 0;
}
#endif

int orb_int_run(double *pos, double *vel, double time, double step_size)
{
    double total_steps=ceil(time/step_size);
    uint64_t s, num_steps;

    while(total_steps>0)
    {
        if(total_steps>(double)(UINT64_MAX-1))
            num_steps=(UINT64_MAX-1);
        else
            num_steps=total_steps;

        if(num_steps==0)
            break;

        for(s=0; s<num_steps; ++s)
        {
            int i;

            i=integrate(pos, vel, grav_accel, 0, step_size);
            if(i<0)
                return i;
        }
        if(s!=num_steps)
            break;

        total_steps-=num_steps;
    }

    return 0;
}

int orb_int_drag_run(double *pos, double *vel, double time, double step_size, drag_opt_type *opt)
{
    int i;
    double total_steps=ceil(time/step_size);
    uint64_t s, num_steps;

    /* initialise atmospheric model */
    i=nrlmsise_input_init(&opt->atmos_input, opt->atmos_data, opt->year, opt->day, opt->hour);
    if(i<0)
        return -1;

    while(total_steps>0)
    {
        if(total_steps>(double)(UINT64_MAX-1))
            num_steps=(UINT64_MAX-1);
        else
            num_steps=total_steps;

        if(num_steps==0)
            break;

        for(s=0; s<num_steps; ++s)
        {
            i=integrate(pos, vel, accel_grav_drag, opt, step_size);
            if(i<0)
                return i;

            /* increment seconds counter */
            opt->sec+=step_size;

            /* check number of seconds is still valid */
            if(opt->sec>=3600.)
            {
                opt->sec-=3600.;
                ++opt->hour;

                /* check hours */
                if(opt->hour>23)
                {
                    opt->hour-=24;
                    ++opt->day;
                }
            }
        }
        if(s!=num_steps)
            break;

        total_steps-=num_steps;
    }

    /* round seconds to ms precision */
    opt->sec=round(opt->sec*1e3)/1e3;

    /* check number of seconds is still valid */
    if(opt->sec>=3600.)
    {
        opt->sec-=3600.;
        ++opt->hour;

        /* check hours */
        if(opt->hour>23)
        {
            opt->hour-=24;
            ++opt->day;
        }
    }

    return 0;
}
