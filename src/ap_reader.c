#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int ap_file_read(const char *filename, double *ap_day, double *ap_3hour, int *num_read)
{
    FILE *f;
    double *apd, *ap3h;
    int ret=0;

    /* check pointers */
    if(!ap_day || !ap_3hour || !num_read)
        return -1;

    apd=ap_day;
    ap3h=ap_3hour;

    /* open file */
    f=fopen(filename, "r");
    if(!f)
        return -2;

    /* read in lines */
    *num_read=0;
    while(!feof(f) && *num_read<366)
    {
        char val[4], line[80], *sp;
        int i;

        /* skip bad lines */
        if(!fgets(line, 80, f))
            continue;

        /* test the length of the line */
        if(strlen(line)<72)
        {
            ++ret;
            continue;
        }

        /* we ignore the date and kp values */

        /* read in the ap values
         *
         * we do this by using a temporary buffer so that we can use the stdlib
         * float reading routines
         *
         * the format being read is %3d, so we copy the 3 characters to val
         * and then use strtod to convert it to a double, the 4th character of
         * val (i.e. val[3]) is set to the null terminator so that the string
         * is always correctly terminated - this is only done on the following
         * line as the strncpy will not touch the fourth character
         */
        val[3]='\0';
        sp=&line[31];

        /* read in the 8 3-hour values */
        for(i=0; i<8; ++i)
        {
            strncpy(val, sp, 3);
            ap3h[i]=strtod(val, 0);
            sp+=3;
        }

        /* read in the day value */
        strncpy(val, sp, 3);
        *apd=strtod(val, 0);

        /* increment the number read counter */
        ++*num_read;

        /* increment the ap pointers */
        ap3h+=8;
        ++apd;
    }

    fclose(f);

    return ret;
}
