#include "drag.h"
#include <math.h>

static double fmd_volumeless_number_density(double u1, double temperature)
{
    const double Rt=R_CONST*temperature;
    const double u1_sqrt_2rt=u1/sqrt(2.*Rt);
    double vnd;

    vnd=0.5 * u1 * (1. + erf(u1_sqrt_2rt));
    vnd+=sqrt(Rt/(2.*M_PI)) * exp(-u1*u1/(2.*Rt));

    return vnd;
}

double fmd_number_density(double u1, double temperature, double density, double particle_mass)
{
    double num_den;

    num_den=fmd_volumeless_number_density(u1, temperature);
    num_den*=density/particle_mass;

    return num_den;
}

double fmd_pressure(double u1, double temperature, double density)
{
    const double tRt=2.*R_CONST*temperature;
    const double u1_2_2rt=u1*u1/(tRt);
    double press;

    press=1. + erf(u1/sqrt(tRt));
    press*=0.5 + u1_2_2rt;
    press+=exp(-u1_2_2rt) * u1 / sqrt(tRt*M_PI);
    press*=density*R_CONST*temperature;

    return press;
}

double fmd_reflected_pressure(double u1, double temperature, double reflection_temperature, double density)
{
    double press;

    press=fmd_volumeless_number_density(u1, temperature);
    press*=density*sqrt(M_PI_2*R_CONST*reflection_temperature);

    return press;
}

double fmd_shear(double u1, double u2, double temperature, double density)
{
    double shear;

    shear=fmd_volumeless_number_density(u1, temperature);
    shear*=density * u2;

    return shear;
}

void fmd_shear2d(double u1, double u2, double u3, double temperature, double density, double *s2, double *s3)
{
    double shear;

    shear=density * fmd_volumeless_number_density(u1, temperature);
    *s2=shear * u2;
    *s3=shear * u3;
}

