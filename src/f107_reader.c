#include <stdio.h>

int f107_file_read(const char *filename, double *f107, int *num_read)
{
    int i, offset, ret=0;
    FILE *f;
    char lines[31][90];

    if(!f107 || !num_read)
        return -1;

    /* open file */
    f=fopen(filename, "r");
    if(!f)
        return -2;

    /* skip the header (first 5 lines) */
    for(i=0; i<5 && !feof(f); ++i)
        while(fgetc(f)!='\n') ;

    /* read in data lines */
    i=0;
    while(!feof(f))
    {
        char *line=lines[i];

        /* skip bad lines */
        if(!fgets(line, 90, f))
            continue;

        if(line[0]=='-')
            break;

        /* skip blank lines */
        if(line[4]==' ')
            continue;

        /* check if last line */
        if(i==30)
            break;

        /* increment line counter */
        ++i;
    }

    /* file not used beyond this point */
    fclose(f);

    /* check that we have read in 31 lines */
    if(i!=30)
        return -3;

    /* convert to data in day order */
    offset=7;
    *num_read=0;
    while(offset<80)
    {
        /* loop over each line with the same offset
         * so that we read in the values in day order
         */
        for(i=0; i<31; ++i)
        {
            char *data=&lines[i][offset];

            /* if value not valid, go to next line */
            if(data[0]=='\n' || data[3]==' ')
                continue;
            /* no values are marked as '----', so find and set to zero */
            else if(data[0]=='-')
            {
                f107[*num_read]=0.;
                ++ret;
            }
            else
            {
                /* otherwise read in the value */
                sscanf(data, "%lf", &f107[*num_read]);
                /* and reduce by 10 */
                f107[*num_read]/=10.;
            }

            /* increment the current day value */
            ++*num_read;
        }

        /* check if incrementing the offset will cause us to exceed the read string */
        for(i=0; i<6; ++i)
        {
            char *data=&lines[i][offset];

            if(data[i]=='\n')
                break;
        }
        /* if so, break out of the loop */
        if(i!=6)
            break;

        /* otherwise add to the offset */
        offset+=6;
    }

    return ret;
}
