#include "misc.h"

int32_t nreadline(FILE *f, char *buf, uint32_t buf_len)
{
    uint32_t i;

    /* return if no file */
    if(!f)
        return -1;

    /* check for end of file */
    if(feof(f))
        return 0;

    /* read line */
    i=0;
    do
        buf[i]=(char)fgetc(f);
    while(i<buf_len && buf[i++]!='\n' && !feof(f));

    /* undo last increment */
    --i;

    /* remove carriage return */
    if(i && buf[i-1]=='\r')
        --i;

    /* remove trailing whitespace */
    for(; i && buf[i-1]==' '; --i)
        ;

    /* null terminate */
    if(i<buf_len)
        buf[i]='\0';
    else
        buf[buf_len-1]='\0';

    return i;
}
