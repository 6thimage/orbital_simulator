#include <stdint.h>
#include <math.h>

#include "date.h"

void days_hms(int year, double days, timedate *t)
{
    static uint8_t months[]={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    double h, m;

    if(!(year%4))
        months[1]=29;
    t->year=year;

    /* find day and month */
    t->day=(uint32_t)days;
    for(t->month=0; t->day>months[t->month]; t->day-=months[(t->month)++])
        ;
    ++t->month;
    
    /* find time - this method of using temporary floats, is more efficient
     * as the entire calculation can stay on the FPU
     */
    h=(days-floor(days))*24.;
    m=(h-floor(h))*60.;
    t->sec=(m-floor(m))*60.;
    
    t->hour=h;
    t->minute=m;
}

double julian_days(timedate *t)
{
    int y, m;
    double j;

    if(t->month<3)
    {
        y=t->year+4800-t->month;
        m=t->month+9;
    }
    else
    {
        y=t->year+4800;
        m=t->month-3;
    }

    /* divisions in the next line should be truncated,
     * but use of floor increases the execution time
     */
    j=t->day + (((153*m)+2)/5) + (365*y) + (y/4) - (y/100) + (y/400) -32045;
    j+=((t->hour-12)/24.) + (t->minute/1440.) + (t->sec/86400.);

    return j;
}

double julian_centuries(double julian_day)
{
    return (julian_day - 2451545.)/36525.;
}

double gmstime(double tt)
{
    double gmst;

    gmst=      4.89496121282305875137570443117595285758074525056567
         +230121.67531542323699689966365167167885590115579445170691*tt
         +      .00000677071394490333587207930438789015825301604767*tt*tt
         -      .00000000045087672343186847403862011519289161763940*tt*tt*tt;

    /* reduce to +/- 2*PI */
    gmst=fmod(gmst, 2.*M_PI);

    /* ensure result is +ve */
    if(gmst<0.)
        gmst+=2.*M_PI;

    return gmst;
}
