#include "quat.h"

#include "vec3.h"

#define _GNU_SOURCE
#include <math.h>

void quat_identity(double *r)
{
    /* identity */
    r[0]=1.;
    r[1]=0.;
    r[2]=0.;
    r[3]=0.;
}

void quat_dot(double *dot, double *a, double *b)
{
    /* dot product */
    *dot=a[0]*b[0];
    *dot+=a[1]*b[1];
    *dot+=a[2]*b[2];
    *dot+=a[3]*b[3];
}

void quat_mul(double *r, double *a, double *b)
{
    r[0]=a[0]*b[0] - a[1]*b[1] - a[2]*b[2] - a[3]*b[3];
    r[1]=a[0]*b[1] + a[1]*b[0] + a[2]*b[3] - a[3]*b[2];
    r[2]=a[0]*b[2] + a[2]*b[0] + a[3]*b[1] - a[1]*b[3];
    r[3]=a[0]*b[3] + a[3]*b[0] + a[1]*b[2] - a[2]*b[1];
}

void quat_mag(double *mag, double *q)
{
    /* magnitude = sqrt(q.q) */
    quat_dot(mag, q, q);

    *mag=sqrt(*mag);
}

void quat_conj(double *r, double *q)
{
    /* only vector negated */
    r[0]=q[0];
    r[1]=-q[1];
    r[2]=-q[2];
    r[3]=-q[3];
}

void quat_inv(double *r, double *q)
{
    int i;
    double abs;

    /* r = q* */
    quat_conj(r, q);

    /* abs = q.q */
    quat_dot(&abs, q, q);

    /* r = r/abs = q* / q.q */
    for(i=0; i<4; ++i)
        *r/=abs;
}

void quat_pitch(double *r, double pitch)
{
    double pitch_2=pitch/2.;

    r[0]=cos(pitch_2);
    r[1]=sin(pitch_2);
    r[2]=0.;
    r[3]=0.;
}

void quat_yaw(double *r, double yaw)
{
    double yaw_2=yaw/2.;

    r[0]=cos(yaw_2);
    r[1]=0.;
    r[2]=sin(yaw_2);
    r[3]=0.;
}

void quat_roll(double *r, double roll)
{
    double roll_2=roll/2.;

    r[0]=cos(roll_2);
    r[1]=0.;
    r[2]=0.;
    r[3]=sin(roll_2);
}

void quat_ypr(double *r, double yaw, double pitch, double roll)
{
    double ys, yc, ps, pc, rs, rc;

    sincos(yaw/2., &ys, &yc);
    sincos(pitch/2., &ps, &pc);
    sincos(roll/2., &rs, &rc);

    r[0]=yc*pc*rc + ys*ps*rs;
    r[1]=yc*ps*rc + ys*pc*rs;
    r[2]=ys*pc*rc - yc*ps*rs;
    r[3]=yc*pc*rs - ys*ps*rc;
}

void quat_from_axis_angle(double *r, double *axis, double angle)
{
    double s, c;

    sincos(angle/2., &s, &c);

    r[0]=c;
    r[1]=axis[0]*s;
    r[2]=axis[1]*s;
    r[3]=axis[2]*s;
}

void quat_get_rotation(double *r, double *src_v, double *dest_v)
{
    double dot, angle, axis[3];

    vec3_dot(&dot, src_v, dest_v);

    if(fabs(dot-1.)<1e-14)
    {
        /* src and dest are in opposite directions */
        r[0]=M_PI;
        r[1]=0.;
        r[2]=1.;
        r[3]=0.;
        return;
    }
    if(fabs(dot+1.)<1e-14)
    {
        /* src and dest are in the same direction */
        quat_identity(r);
        return;
    }

    angle=acos(dot);
    vec3_cross(axis, src_v, dest_v);
    vec3_normalise(axis, axis);
    quat_from_axis_angle(r, axis, angle);
}

void quat_look_at(double *r, double *src_v, double *dest_v)
{
    double new_forward[3], forward[3]={0., 0., 1.};

    vec3_sub(new_forward, dest_v, src_v);
    vec3_normalise(new_forward, new_forward);

    quat_get_rotation(r, forward, new_forward);
}

void quat_to_mat3(double *m, double *q)
{
    double x2=q[1]*q[1], y2=q[2]*q[2], z2=q[3]*q[3];

    m[0]=1. - 2.*(y2 + z2);
    m[1]=2.*(q[1]*q[2] + q[0]*q[3]);
    m[2]=2.*(q[1]*q[3] - q[0]*q[2]);

    m[3]=2.*(q[1]*q[2] - q[0]*q[3]);
    m[4]=1. - 2.*(x2 + z2);
    m[5]=2.*(q[2]*q[3] + q[0]*q[1]);

    m[6]=2.*(q[1]*q[3] + q[0]*q[2]);
    m[7]=2.*(q[2]*q[3] - q[0]*q[1]);
    m[8]=1. - 2.*(x2 + y2);
}

void quat_to_euler(double *yaw, double *pitch, double *roll, double *q)
{
    double ps=-2.*(q[2]*q[3] - q[0]*q[1]);

    if(fabs(ps)>(1.-1e-14))
    {
        /* gimbal lock */
        *pitch=M_PI_2*ps; /* look straight up or down */
        *yaw=atan2(-q[1]*q[3] + q[0]*q[2], 0.5 - (q[2]*q[2] + q[3]*q[3]));;
        *roll=0.;
    }
    else
    {
        *pitch=asin(ps);
        *yaw=atan2(q[1]*q[3] + q[0]*q[2], 0.5 - (q[1]*q[1] + q[2]*q[2]));
        *roll=atan2(q[1]*q[2] + q[0]*q[3], 0.5 - (q[1]*q[1] + q[3]*q[3]));
    }
}

void quat_mul_vec(double *r, double *q, double *v)
{
    double m[9];

    quat_to_mat3(m, q);

    r[0]=v[0]*m[0] + v[1]*m[1] + v[2]*m[2];
    r[1]=v[0]*m[3] + v[1]*m[4] + v[2]*m[5];
    r[2]=v[0]*m[6] + v[1]*m[7] + v[2]*m[8];
}
