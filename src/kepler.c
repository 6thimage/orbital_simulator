#include "kepler.h"

#include <math.h>
#include "vec3.h"

void kepler(tle_entry *tle, double since, double *r, double *v)
{
    double a, M, E, q[3], qdot[3];
    double mean_motion_s;
    unsigned int i;

    /* useful values */
    mean_motion_s=tle->mean_motion/60.; /* mean motion in rad/s rather than rad/min */

    /* calculate semi-major axis
     * mu is from WGS72
     */
    a=398600.8/(mean_motion_s*mean_motion_s); /* a = mu/n^2 */
    a=pow(a, 1./3.); /* a = (mu/n^2)^1/3 */

    /* calculate mean anomaly */
    M=tle->mean_anom + tle->mean_motion*since;
    /* wrap large M */
    while(M>=2.*M_PI)
        M-=2.*M_PI;

    /* calculate eccentric anomaly */
    E=M;
    for(i=0; i<10; i++)
        /* Newton-Raphson method */
        E=E-
            (
             (E - (tle->ecc*sin(E)) - M)
             /
             (1. - (tle->ecc*cos(E)) )
            );

    /* calculate q */
    q[0]=a*(cos(E) - tle->ecc);
    q[1]=a*(sqrt(1. - (tle->ecc*tle->ecc))
            *sin(E));
    q[2]=0;

    /* calculate qdot */
    qdot[0]=-(mean_motion_s*a*sin(E))
             /
             (1. - tle->ecc*cos(E));
    qdot[1]=-qdot[0]*sqrt(1. - (tle->ecc*tle->ecc)); /* TODO: this line is wrong */
    qdot[2]=0;

    /* rotate to ECI */
    /* q->r */
    vec3_r3(r, q, -tle->perigee);
    vec3_r1(q, r, -tle->incl);
    vec3_r3(r, q, -tle->raan);

    /* qdot->v */
    vec3_r3(v, qdot, -tle->perigee);
    vec3_r1(qdot, v, -tle->incl);
    vec3_r3(v, qdot, -tle->raan);
}
