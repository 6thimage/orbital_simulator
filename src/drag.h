#ifndef _DRAG_H_
#define _DRAG_H_

#define R_CONST (8.3144598)

double fmd_number_density(double u1, double temperature, double density, double particle_mass);
double fmd_pressure(double u1, double temperature, double density);
double fmd_reflected_pressure(double u1, double temperautre, double reflection_temperatire, double density);
double fmd_shear(double u1, double u2, double temperature, double density);
void fmd_shear2d(double u1, double u2, double u3, double temperature, double density, double *s2, double *s3);

#endif /* ! _DRAG_H_ */

