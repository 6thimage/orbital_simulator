#include "lua_wrapper.h"
#include "orbit_integrator.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

static int lua_orb_int_run(lua_State *L)
{
    int i;
    double pos[3], vel[3], time, step_size;

    /* check arguments */
    luaL_checktype(L, 1, LUA_TTABLE);
    luaL_checktype(L, 2, LUA_TTABLE);
    time=luaL_checknumber(L, 3);
    step_size=luaL_checknumber(L, 4);

    /* get the pos values */
    lua_getfield(L, 1, "x");
    lua_getfield(L, 1, "y");
    lua_getfield(L, 1, "z");
    pos[0]=luaL_checknumber(L, -3);
    pos[1]=luaL_checknumber(L, -2);
    pos[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* get the vel values */
    lua_getfield(L, 2, "x");
    lua_getfield(L, 2, "y");
    lua_getfield(L, 2, "z");
    vel[0]=luaL_checknumber(L, -3);
    vel[1]=luaL_checknumber(L, -2);
    vel[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* run the integration */
    i=orb_int_run(pos, vel, time, step_size);
    if(i<0)
        return luaL_error(L, "integration run returned %d\n", i);

    /* return the results */
    /* pos */
    lua_newtable(L);
    lua_pushstring(L, "x");
    lua_pushnumber(L, pos[0]);
    lua_settable(L, -3);
    lua_pushstring(L, "y");
    lua_pushnumber(L, pos[1]);
    lua_settable(L, -3);
    lua_pushstring(L, "z");
    lua_pushnumber(L, pos[2]);
    lua_settable(L, -3);
    /* vel */
    lua_newtable(L);
    lua_pushstring(L, "x");
    lua_pushnumber(L, vel[0]);
    lua_settable(L, -3);
    lua_pushstring(L, "y");
    lua_pushnumber(L, vel[1]);
    lua_settable(L, -3);
    lua_pushstring(L, "z");
    lua_pushnumber(L, vel[2]);
    lua_settable(L, -3);

    return 2;
}

static int lua_orb_int_drag_run(lua_State *L)
{
    /* drag_run(pos, vel, time, stepsize, eop, atmos, sat_obj, year, day, hour, sec)
     * day - contains sub-day timing ie (day - floor(day))*24*60*60 will give number of seconds into day
     *
     * this function will find the correct eop and atmos data before calling the integrator
     */
    int i;
    double pos[3], vel[3], time, step_size;
    lua_eop_data *eop_data;
    drag_opt_type opt;

    /* drag opt fixed set up */
    opt.atmos_input.ap_a=&opt.AP; /* link the ap_a array to an actual array */
    /* reset the atmospheric data cache */
    for(i=0; i<3; ++i)
        opt.atmos_cache_position[i]=0.;

    /* check arguments */
    luaL_checktype(L, 1, LUA_TTABLE);
    luaL_checktype(L, 2, LUA_TTABLE);
    time=luaL_checknumber(L, 3);
    step_size=luaL_checknumber(L, 4);
    luaL_checktype(L, 5, LUA_TTABLE);
    opt.atmos_data=luaL_checkudata(L, 6, LUA_NRLMSISE_DATA);
    opt.sat=luaL_checkudata(L, 7, LUA_SAT_OBJ_DATA);
    opt.year=luaL_checkinteger(L, 8);
    opt.day=luaL_checkinteger(L, 9);
    opt.hour=luaL_checkinteger(L, 10);
    opt.sec=luaL_checknumber(L, 11);

    if(opt.day==0 || opt.day>366)
        return luaL_error(L, "invalid day (%d)\n", opt.day);

    /* get the pos values */
    lua_getfield(L, 1, "x");
    lua_getfield(L, 1, "y");
    lua_getfield(L, 1, "z");
    pos[0]=luaL_checknumber(L, -3);
    pos[1]=luaL_checknumber(L, -2);
    pos[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* get the vel values */
    lua_getfield(L, 2, "x");
    lua_getfield(L, 2, "y");
    lua_getfield(L, 2, "z");
    vel[0]=luaL_checknumber(L, -3);
    vel[1]=luaL_checknumber(L, -2);
    vel[2]=luaL_checknumber(L, -1);
    lua_pop(L, 3);

    /* get eop data */
    lua_geti(L, 5, opt.year);
    if(lua_isnil(L, -1))
        return luaL_error(L, "requeted year is not in EOP data (%d)", opt.year);
    /* get year values */
    eop_data=luaL_checkudata(L, -1, LUA_EOP_DATA);
    lua_pop(L, 1);
    /* look for day values */
    if(opt.day>eop_data->num)
        return luaL_error(L, "requested day is not in EOP data (%d > %d)", opt.day, eop_data->num);
    --opt.day;
    /* copy eop data to drag opt */
    opt.d_eps=eop_data->deps[opt.day];
    opt.d_psi=eop_data->dpsi[opt.day];
    opt.xp=eop_data->xp[opt.day];
    opt.yp=eop_data->yp[opt.day];
    ++opt.day;

    /* run the integration */
    i=orb_int_drag_run(pos, vel, time, step_size, &opt);
    if(i<0)
        return luaL_error(L, "integration run returned %d\n", i);

    /* return the results */
    /* pos */
    lua_newtable(L);
    lua_pushstring(L, "x");
    lua_pushnumber(L, pos[0]);
    lua_settable(L, -3);
    lua_pushstring(L, "y");
    lua_pushnumber(L, pos[1]);
    lua_settable(L, -3);
    lua_pushstring(L, "z");
    lua_pushnumber(L, pos[2]);
    lua_settable(L, -3);
    /* vel */
    lua_newtable(L);
    lua_pushstring(L, "x");
    lua_pushnumber(L, vel[0]);
    lua_settable(L, -3);
    lua_pushstring(L, "y");
    lua_pushnumber(L, vel[1]);
    lua_settable(L, -3);
    lua_pushstring(L, "z");
    lua_pushnumber(L, vel[2]);
    lua_settable(L, -3);
    /* day */
    lua_pushinteger(L, opt.day);
    /* hour */
    lua_pushinteger(L, opt.hour);
    /* seconds */
    lua_pushnumber(L, opt.sec);

    return 5;
}

static const luaL_Reg orb_int_methods[] = {
    {"run", lua_orb_int_run},
    {"drag_run", lua_orb_int_drag_run},
    {0, 0}
};

int luaopen_orb_int(lua_State *L)
{
    luaL_newlib(L, orb_int_methods);
    lua_setglobal(L, "orb_int");

    return 1;
}
