#ifndef _QUAT_H_
#define _QUAT_H_

/* quaternions are stored as:
 *   0 1 2 3
 *   w x y z
 *
 * where w is the scalar, and {x, y, z} is the vector
 */

void quat_identity(double *r);

void quat_dot(double *dot, double *a, double *b);

void quat_mul(double *r, double *a, double *b);

void quat_mag(double *mag, double *q);

void quat_conj(double *r, double *q);

void quat_inv(double *r, double *q);

/* pitch - x axis
 * yaw   - y axis
 * roll  - z axis
 */

void quat_pitch(double *r, double pitch);

void quat_yaw(double *r, double yaw);

void quat_roll(double *r, double roll);

void quat_ypr(double *r, double yaw, double pitch, double roll);

void quat_from_axis_angle(double *r, double *axis, double angler);

/* rotation between src and destination */
void quat_get_rotation(double *r, double *src_v, double *dest_v);

/* rotation for source to look at destination */
void quat_look_at(double *r, double *src_v, double *dest_v);

/* convert quaternion to 3x3 matrix (row-major) */
void quat_to_mat3(double *m, double *q);

void quat_to_euler(double *yaw, double *pitch, double *roll, double *q);

/* both r and v are 3 element vectors */
void quat_mul_vec(double *r, double *q, double *v);

#endif /* ! _QUAT_H_ */
