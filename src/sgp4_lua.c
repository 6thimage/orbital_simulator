#include "lua_wrapper.h"
#include "date.h"
#include "tle.h"
#include "sgp4/sgp4unit.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

/* sgp4 methods */
static int lua_sgp4_init(lua_State *L)
{
    int gravconst=luaL_checkinteger(L, 1);
    tle_entry *tle=luaL_checkudata(L, 2, LUA_TLE);
    sgp4_work *work=luaL_checkudata(L, 3, LUA_SGP4_WORK);

    /* check gravconst */
    if(gravconst<0 || gravconst>2)
        return luaL_error(L, "Incorrect gravity constant (%d)", gravconst);

    /* init sgp4
     * sgp4init expects epoch from jan 0, 1950, 0hr (2433281.5 in Julian)
     * so we correct for this in the function call.
     */
    sgp4init(gravconst, 'i', tle, work);

    /* return nothing */
    return 0;
}

static int lua_sgp4_init_dragless(lua_State *L)
{
    int gravconst=luaL_checkinteger(L, 1);
    tle_entry *tle=luaL_checkudata(L, 2, LUA_TLE);
    sgp4_work *work=luaL_checkudata(L, 3, LUA_SGP4_WORK);
    double bstar_drag;

    /* check gravconst */
    if(gravconst<0 || gravconst>2)
        return luaL_error(L, "Incorrect gravity constant (%d)", gravconst);

    /* save drag parameter */
    bstar_drag=tle->bstar_drag;
    tle->bstar_drag=0.;

    /* init sgp4
     * sgp4init expects epoch from jan 0, 1950, 0hr (2433281.5 in Julian)
     * so we correct for this in the function call.
     */
    sgp4init(gravconst, 'i', tle, work);

    /* restore drag parameter */
    tle->bstar_drag=bstar_drag;

    /* return nothing */
    return 0;
}

static int lua_sgp4_single(lua_State *L)
{
    tle_entry *tle=luaL_checkudata(L, 1, LUA_TLE);
    sgp4_work *work=luaL_checkudata(L, 2, LUA_SGP4_WORK);
    double since=luaL_checknumber(L, 3);
    double ro[3], vo[3];
    int ret;

    /* run sgp4 */
    ret=sgp4(tle, work, since, ro, vo);

    if(!ret)
    {
        /* successful return tables of ro and vo */
        /* ro */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, ro[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, ro[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, ro[2]);
        lua_settable(L, -3);
        /* vo */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, vo[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, vo[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, vo[2]);
        lua_settable(L, -3);
        return 2;
    }
    else
    {
        /* unsuccessful return ret (which will be -1) */
        lua_pushinteger(L, ret);
        /* return error */
        switch(work->error)
        {
        case 1:
            lua_pushstring(L, "Mean elements (a < 0.95Er or ecc >= 1.0 or ecc < -0.001)");
            break;
        case 2:
            lua_pushstring(L, "Mean motion less than 0.0");
            break;
        case 3:
            lua_pushstring(L, "Pert elements (ecc < 0.0 or ecc > 1.0)");
            break;
        case 4:
            lua_pushstring(L, "Semi-latus rectum < 0.0");
            break;
        case 5:
            lua_pushstring(L, "Epoch elements are sub-orbital");
            break;
        case 6:
            lua_pushstring(L, "Satellite has decayed");
            break;
        default:
            lua_pushstring(L, "Unknown");
            break;
        }
        return 2;
    }
}

static int lua_sgp4_single_dragless(lua_State *L)
{
    tle_entry *tle=luaL_checkudata(L, 1, LUA_TLE);
    sgp4_work *work=luaL_checkudata(L, 2, LUA_SGP4_WORK);
    double since=luaL_checknumber(L, 3);
    double ro[3], vo[3];
    int ret;
    double bstar_drag;

    /* save drag parameter */
    bstar_drag=tle->bstar_drag;
    tle->bstar_drag=0.;

    /* run sgp4 */
    ret=sgp4(tle, work, since, ro, vo);

    /* restore drag parameter */
    tle->bstar_drag=bstar_drag;

    if(!ret)
    {
        /* successful return tables of ro and vo */
        /* ro */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, ro[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, ro[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, ro[2]);
        lua_settable(L, -3);
        /* vo */
        lua_newtable(L);
        lua_pushstring(L, "x");
        lua_pushnumber(L, vo[0]);
        lua_settable(L, -3);
        lua_pushstring(L, "y");
        lua_pushnumber(L, vo[1]);
        lua_settable(L, -3);
        lua_pushstring(L, "z");
        lua_pushnumber(L, vo[2]);
        lua_settable(L, -3);
        return 2;
    }
    else
    {
        /* unsuccessful return ret (which will be -1) */
        lua_pushinteger(L, ret);
        /* return error */
        switch(work->error)
        {
        case 1:
            lua_pushstring(L, "Mean elements (a < 0.95Er or ecc >= 1.0 or ecc < -0.001)");
            break;
        case 2:
            lua_pushstring(L, "Mean motion less than 0.0");
            break;
        case 3:
            lua_pushstring(L, "Pert elements (ecc < 0.0 or ecc > 1.0)");
            break;
        case 4:
            lua_pushstring(L, "Semi-latus rectum < 0.0");
            break;
        case 5:
            lua_pushstring(L, "Epoch elements are sub-orbital");
            break;
        case 6:
            lua_pushstring(L, "Satellite has decayed");
            break;
        default:
            lua_pushstring(L, "Unknown");
            break;
        }
        return 2;
    }
}

static const luaL_Reg sgp4_methods[] = {
    {"init", lua_sgp4_init},
    {"single", lua_sgp4_single},
    {"init_dragless", lua_sgp4_init_dragless},
    {"single_dragless", lua_sgp4_single_dragless},
    {0, 0}
};

/* work methods */
static int work_new(lua_State *L)
{
    lua_newuserdata(L, sizeof(sgp4_work));
    luaL_setmetatable(L, LUA_SGP4_WORK);
    return 1;
}

/* work meta */
static int work_tostring(lua_State *L)
{
    lua_pushstring(L, "SGP4 work variable");
    return 1;
}

static const luaL_Reg work_methods[] = {
    {"new", work_new},
    {0, 0}
};

static const luaL_Reg work_meta[] = {
    {"__tostring", work_tostring},
    {0, 0}
};

int luaopen_sgp4(lua_State *L)
{
    luaL_newlib(L, sgp4_methods); /* load the library */
    /* add sgp4 grav constants */
    lua_pushinteger(L, wgs72old);
    lua_setfield(L, -2, "WGS72_old");
    lua_pushinteger(L, wgs72);
    lua_setfield(L, -2, "WGS72");
    lua_pushinteger(L, wgs72_no_j);
    lua_setfield(L, -2, "WGS72_no_j");
    lua_pushinteger(L, wgs84);
    lua_setfield(L, -2, "WGS84");
    lua_setglobal(L, "sgp4"); /* store under the global sgp4 */

    luaL_newlib(L, work_methods); /* load library */
    lua_setglobal(L, "sgp4_work"); /* store under global */

    luaL_newmetatable(L, LUA_SGP4_WORK); /* create metatable */
    lua_pushvalue(L, -1); /* push onto stack */
    lua_setfield(L, -2, "__index"); /* metatable.__index=metatable */
    luaL_setfuncs(L, work_meta, 0); /* add metatable functions */
    lua_pop(L, 1); /* pop metatable */
    return 1;
}
