#ifndef _SAT_OBJECT_H_
#define _SAT_OBJECT_H_

typedef struct{
    double area;
    double normal[3];
} sat_face;

typedef struct{
    double rotation[4];
    double mass;
    int num_faces;
    sat_face *faces;
} sat_object;

#endif /* ! _SAT_OBJECT_H_ */
