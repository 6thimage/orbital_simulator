#ifndef _DATE_H_
#define _DATE_H_

typedef struct{
    int day, month, year, hour, minute;
    double sec;
}timedate;

void days_hms(int year, double days, timedate *t);

double julian_days(timedate *t);

/* number of Julian centuries since the J2000 epoch */
double julian_centuries(double julian_day);

double gmstime(double tt);

#endif /* !_DATE_H_ */
