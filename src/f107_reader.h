#ifndef _F107_READER_H_
#define _F107_READER_H_

/* reads in an f10.7 data file - e.g. from http://www.ngdc.noaa.gov/stp/space-weather/solar-data/solar-features/solar-radio/noontime-flux/penticton/penticton_observed/tables/
 * arguments:
 *   filename   name of the f10.7 data file
 *   f107       pointer to an array of doubles that is at least 366 in size
 *   num_read   used to return the number of values read into f107
 *
 * returns:
 *   0  success
 *   -1 if either of the f107 and num_read pointers are zero
 *   -2 failed to open file
 *   -3 malformed file - does not contain 31 data lines
 *   >0 number of missing entries (those where the value is '----')
 */
int f107_file_read(const char *filename, double *f107, int *num_read);

#endif /* ! _F107_READER_H_ */
