#ifndef _VEC3_H_
#define _VEC3_H_

void vec3_add(double *r, double *a, double *b);
void vec3_sub(double *r, double *a, double *b);

/* mul/div by scalar */
void vec3_mul_s(double *r, double *a, double s);
void vec3_div_s(double *r, double *a, double s);

void vec3_dot(double *dot, double *a, double *b);
void vec3_cross(double *r, double *a, double *b);

void vec3_mag(double *mag, double *v);
void vec3_normalise(double *r, double *v);

/* vector rotations
 * r is output, v is input, alpha is rotation angle (rad)
 */
void vec3_r1(double *r, double *v, double alpha);
void vec3_r2(double *r, double *v, double alpha);
void vec3_r3(double *r, double *v, double alpha);

#endif /* ! _VEC3_H_ */
