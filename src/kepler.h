#ifndef _KEPLER_H_
#define _KEPLER_H_

#include "tle.h"

void kepler(tle_entry *tle, double since, double *r, double *v);

#endif /* !_KEPLER_H_ */
