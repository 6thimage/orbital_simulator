#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include "linenoise/linenoise.h"
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "lua_wrapper.h"

void command_completion(const char *buf, linenoiseCompletions *lc)
{
    if(buf[0]=='h')
        linenoiseAddCompletion(lc, "help");
}

int main(int argc, char *argv[])
{
    char *line;
    int error, c;
    lua_State *L;

    /* options */
    char *file=0;
    int nobanner=0;

    /* disable getopt from printing 'invalid option' to stdout
     * (this is disabled as we allow the arguments to be processed using lua at
     * a latter point in the programs execution) */
    opterr=0;

    /* parse arguments */
    while((c=getopt(argc, argv, "-f:"))!=-1)
    {
        switch(c)
        {
        case 'f':
            file=optarg;
            nobanner=1;
            break;
        default:
            break;
        }
    }

    /* print banner */
    if(!nobanner)
        puts("Orbital simulator by Ian Griffiths, version 0.1");

    /* create a lua state */
    L=luaL_newstate();
    /* open standard libs */
    luaL_openlibs(L);

    luaopen_tle(L);
    luaopen_sgp4(L);
    luaopen_kepler(L);
    luaopen_orb_int(L);
    luaopen_eop_reader(L);
    luaopen_coords(L);
    luaopen_ap_reader(L);
    luaopen_f107_reader(L);
    luaopen_nrlmsise(L);
    luaopen_sat_obj(L);

    /* find scripts dir */
    {
        char *path, *scripts;
        size_t path_len;

        /* copy program path */
        path=strdup(argv[0]);
        /* remove program name */
        *(strrchr(path,'/')+1)='\0';
        path_len=strlen(path);

        /* create scripts path */
        scripts=malloc(path_len+29+15);
        strcpy(scripts, "package.path=package.path..\";");
        strcpy(scripts+29, path);
        free(path);
        strcpy(scripts+path_len+29, "scripts/?.lua\"");

        /* load defaults */
        error=luaL_dostring(L, scripts)||luaL_dostring(L, "require \"defaults\"");
        if(error)
            puts(lua_tostring(L, -1));

        free(scripts);
    }

    /* load argv into global variable */
    {
        int i;

        lua_newtable(L);
        for(i=0; i<argc; ++i)
        {
            lua_pushnumber(L, i+1);
            lua_pushstring(L, argv[i]);
            lua_settable(L, -3);
        }
        lua_setglobal(L, "args");
    }

    /* run file */
    if(file)
    {
        error=luaL_dofile(L, file);
        if(error)
            puts(lua_tostring(L, -1));
        lua_close(L);
        return error;
    }

    /* linenoise setup */
    linenoiseSetCompletionCallback(command_completion);
    linenoiseHistoryLoad(".history");

    /* main loop */
    while((line=linenoise("orb sim> ")))
    {
        /* handle non-lua commands */
        if(!strcmp(line, "help"))
        {
            puts("[Help]\n"
                 "Currently nothing to see.");
        }
        else
        {
            /* send string to lua */
            error=luaL_loadbuffer(L, line, strlen(line), "line") ||
                    lua_pcall(L, 0, 0, 0);
            if(error)
            {
                /* print error */
                puts(lua_tostring(L, -1));
                /* remove error from stack */
                lua_pop(L, 1);
            }
        }

        /* add line to history */
        linenoiseHistoryAdd(line);

        /* free line received from linenoise */
        free(line);

        /* flush stdout so that io.writes from lua are actually printed */
        fflush(stdout);
    }

    /* save history */
    linenoiseHistorySave(".history");

    /* destroy lua state */
    lua_close(L);

    return 0;
}
