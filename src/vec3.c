#define _GNU_SOURCE
#include <math.h>

void vec3_add(double *r, double *a, double *b)
{
    r[0]=a[0]+b[0];
    r[1]=a[1]+b[1];
    r[2]=a[2]+b[2];
}

void vec3_sub(double *r, double *a, double *b)
{
    r[0]=a[0]-b[0];
    r[1]=a[1]-b[1];
    r[2]=a[2]-b[2];
}

void vec3_mul_s(double *r, double *a, double s)
{
    r[0]=a[0]*s;
    r[1]=a[1]*s;
    r[2]=a[2]*s;
}

void vec3_div_s(double *r, double *a, double s)
{
    r[0]=a[0]/s;
    r[1]=a[1]/s;
    r[2]=a[2]/s;
}

void vec3_dot(double *dot, double *a, double *b)
{
    *dot=a[0]*b[0];
    *dot+=a[1]*b[1];
    *dot+=a[2]*b[2];
}

void vec3_cross(double *r, double *a, double *b)
{
    r[0]=a[1]*b[2] - a[2]*b[1];
    r[1]=a[2]*b[0] - a[0]*b[2];
    r[2]=a[0]*b[1] - a[1]*b[0];
}

void vec3_mag(double *mag, double *v)
{
    vec3_dot(mag, v, v);

    *mag=sqrt(*mag);
}

void vec3_normalise(double *r, double *v)
{
    double mag;

    vec3_mag(&mag, v);
    vec3_div_s(r, v, mag);
}

void vec3_r1(double *r, double *v, double alpha)
{
    double s, c;

    sincos(alpha, &s, &c);
    r[0]=v[0];
    r[1]=c*v[1] + s*v[2];
    r[2]=-s*v[1] + c*v[2];
}

void vec3_r2(double *r, double *v, double alpha)
{
    double s, c;

    sincos(alpha, &s, &c);
    r[1]=v[1];
    r[0]=c*v[0] - s*v[2];
    r[2]=s*v[0] + c*v[2];
}

void vec3_r3(double *r, double *v, double alpha)
{
    double s, c;

    sincos(alpha, &s, &c);
    r[2]=v[2];
    r[0]=c*v[0] + s*v[1];
    r[1]=-s*v[0] + c*v[1];
}
