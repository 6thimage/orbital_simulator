#ifndef _NRLMSISE_H_
#define _NRLMSISE_H_

#include "nrlmsise-00/nrlmsise-00.h"

typedef struct
{
    double *f107;
    double *ap_day;
    double *ap_3hour;
    int f107_length;
    int ap_length;
    int *years;
    int *year_lengths;
} nrlmsise_data;

/* initialises an nrlmsise input struct
 * arguments:
 *   input          input struct to fill
 *   data
 *     f107           f10.7 data
 *     f107_length    length of f107
 *     ap_day         ap daily data
 *     ap_3hour       ap 3 hour data
 *     ap_length      length of ap_day
 *     years          arrays of years, followed by a zero
 *     year_lengths   number of days in each year (array matching years)
 *   year           year to calculate for
 *   day            day to calculate for
 *   hour           hour to calculate for
 *
 * returns:
 *   0  on success
 *   -1 if pointers are bad
 *   -2 invalid time
 *   -3 year not in data set
 *   -4 day not in data set
 */
int nrlmsise_input_init(struct nrlmsise_input *input, nrlmsise_data *data,
                        int year, int day, int hour);

/* updates the seconds and lat/long values of the nrlmsise input struct
 * arguments:
 *   input  input struct to update
 *   sec    seconds into hour (i.e. between 0 and 3600)
 *   lat    latitude to calculate for
 *   lon    longitude to calculate for
 * returns:
 *   0  on success
 *   -1 if sec is not hour constrained (either <0 or >=3600)
 */
int nrlmsise_input_update(struct nrlmsise_input *input, double sec, double lat, double lon);

#endif /* ! _NRLMSISE_H_ */
