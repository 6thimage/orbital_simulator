# vim: tw=0
SIMIMAGE = orb_sim

C_DIR = src
C_SOURCE = main.c ap_reader.c ap_reader_lua.c coords.c coords_lua.c date.c drag.c eop_reader.c eop_reader_lua.c f107_reader.c f107_reader_lua.c kepler.c kepler_lua.c misc.c nrlmsise.c nrlmsise_lua.c orbit_integrator.c orbit_integrator_lua.c quat.c sat_object_lua.c sgp4_lua.c tle.c tle_lua.c vec3.c sgp4/sgp4unit.c linenoise/linenoise.c nrlmsise-00/nrlmsise-00.c nrlmsise-00/nrlmsise-00_data.c

TEST_DIR = tests

WORK_DIR = work

LIBS = m
PKG_LIBS = lua5.3

#SFLAGS = -fsanitize=undefined,integer
#SFLAGS += -fsanitize=bounds,float-cast-overflow,float-divide-by-zero
#SFLAGS += -fsanitize=address -fno-omit-frame-pointer
#SFLAGS += -fsanitize=memory -fno-omit-frame-pointer -fsanitize-memory-track-origins

# optimisation level
# 0 - reduce compilation time and make debugging work (default)
# 1 - tries to reduce code size and execution time, without extremely extending compilation time
# 2 - optimise more than 1. Enables all optimisations that don't have a speed-space tradeoff
# 3 - optimise even more than 2. Very aggressive
# s - optimise for speed, enables all level 2 (with some exceptions) and a few others
# fast - optimise for speed, disregards strict standards compliance and enables level 3
OPTIM = 3

# debugging mode (1 enabled, 0 disabled)
DEBUG_MODE = 1

CC = clang

# objects
WORK_OBJS = $(addprefix $(WORK_DIR)/,$(C_SOURCE:.c=.o))

# compiler flags
CFLAGS = -c -Wall -Wextra -std=gnu11 -pedantic -O$(OPTIM) $(shell pkg-config --cflags $(PKG_LIBS)) -ftree-vectorize
ifeq ($(DEBUG_MODE),1)
CFLAGS += -ggdb
endif # DEBUG_MODE
CFLAGS += $(SFLAGS)

# linker flags
LK = $(addprefix -l,$(LIBS)) $(shell pkg-config --libs $(PKG_LIBS))
LK += $(SFLAGS)

# rules
defualt: sim

.PHONY: all dirs clean sim tests

all: sim tests

tests:
	@echo Making tests
	@make -C $(TEST_DIR)

run-tests:
	@make -C $(TEST_DIR) run-tests

dirs:
	@if [ ! -d $(WORK_DIR) ]; then\
		echo Creating work folder;\
		mkdir $(WORK_DIR);\
		mkdir $(WORK_DIR)/sgp4;\
		mkdir $(WORK_DIR)/linenoise;\
		mkdir $(WORK_DIR)/nrlmsise-00;\
		fi

clean:
	@if [ -d $(WORK_DIR) ]; then\
		echo Removing work folder;\
		rm -r $(WORK_DIR);\
		fi
	@if [ -f $(SIMIMAGE) ]; then rm $(SIMIMAGE); fi
	@make -C $(TEST_DIR) clean

sim: $(SIMIMAGE)

$(SIMIMAGE): dirs $(WORK_OBJS)
	@echo Linking ... 
	@$(CC) $(WORK_OBJS) $(LK) -o $(SIMIMAGE)

$(addprefix $(WORK_DIR)/,%.o):$(addprefix $(C_DIR)/,%.c)
	@echo $<
	@$(CC) $(CFLAGS) -c $< -o $@

