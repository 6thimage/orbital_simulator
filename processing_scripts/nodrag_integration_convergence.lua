-- find arguments
for i in pairs(args) do
    if args[i]:lower() == '-s' then
        step=args[i+1]
    end
    if args[i]:lower() == '-d' then
        out_fol=args[i+1]
    end
end

if not step or not out_fol then
    print('Error - failed to find arguments')
    os.exit(-1)
end


-- circ 15 500
p={x=6878.084778, y=-24.582284, z=-10.681252}
v={x=0.029340,    y=7.353069,   z=1.970612}

-- circ 15 100
--p={x=6478.087815, y=-23.152689, z=-10.060081}
--v={x=0.030232,    y=7.576681,   z=2.030540}

filename=out_fol..'step_'..step

file=io.open(filename, 'w')

t_before=os.time()

for i=0, 24*7*60, 1 do
    -- run integration
    p, v=orb_int.run(p, v, 60, step)

    fprintf(file, '% 21.15f % 21.15f % 21.15f % 21.15f % 21.15f % 21.15f\n',
                    p.x, p.y, p.z, v.x, v.y, v.z)

    file:flush()
end

t_after=os.time()

file:close()

printf("time for step %s is %d\n", step, os.difftime(t_after, t_before))
