--orb_int=require('orbit_integrator_lua')

function period(r)
    local t=r*1000 -- r in m
    t=t^3 -- r^3
    t=t*4*math.pi^2 -- 4*pi^2*r^3
    t=t/(6.67e-11*5.972e24) -- (4(pi^2*r^3)/GM
    return math.sqrt(t)/60 --return in minutes
end

t=tle.parse('data/tle/'..'STRAND-1')
--t=tle.parse('data/tle/'..'BEESAT')
print(#t)

work=sgp4_work.new()
sgp4.init(sgp4.WGS72, t[330], work)
p0,v0=sgp4.single(t[330], work, 0)

pr0=period(math.sqrt(p0.x^2 +p0.y^2 +p0.z^2))

print(pr0)

epoch_diff=(jdate_to_unix(t[331].epoch)-jdate_to_unix(t[330].epoch))/60
print(epoch_diff)

file=io.open('integrate_test.dat', 'w')

p=p0
v=v0

step=.0001

-- run over first tle
off=0
for i=1, epoch_diff, 1 do
    sp, sv=sgp4.single(t[330], work, i)
    p, v=orb_int.run(p, v, 60, step)

    fprintf(file, '%7.2f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n',
            i, sp.x, sp.y, sp.z, p.x, p.y, p.z)
    off=i
end
-- run over any fractional time
sp, sv=sgp4.single(t[330], work, epoch_diff)
p, v=orb_int.run(p, v, (epoch_diff-off)*60, step)

fprintf(file, '%7.2f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n',
        epoch_diff, sp.x, sp.y, sp.z, p.x, p.y, p.z)
off=epoch_diff

fprintf(file, '\n\n');

epoch_diff=(jdate_to_unix(t[332].epoch)-jdate_to_unix(t[331].epoch))/60
print(epoch_diff)

-- run over second tle
sgp4.init(sgp4.WGS72, t[331], work)
for i=1, epoch_diff, 1 do
    sp, sv=sgp4.single(t[331], work, i)
    p, v=orb_int.run(p, v, 60, step)

    fprintf(file, '%7.2f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n',
            i+off, sp.x, sp.y, sp.z, p.x, p.y, p.z)
    off2=i
end
-- run over any fractional time
sp, sv=sgp4.single(t[331], work, epoch_diff)
p, v=orb_int.run(p, v, (epoch_diff-off2)*60, step)

fprintf(file, '%7.2f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n',
        epoch_diff+off, sp.x, sp.y, sp.z, p.x, p.y, p.z)
off2=epoch_diff

fprintf(file, '\n\n');

off=off+off2
-- run over third tle
sgp4.init(sgp4.WGS72, t[332], work)
for i=1, 3*pr0, 1 do
    sp, sv=sgp4.single(t[332], work, i)
    p, v=orb_int.run(p, v, 60, step)

    fprintf(file, '%7.2f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n',
            i+off, sp.x, sp.y, sp.z, p.x, p.y, p.z)
end

file:close()
