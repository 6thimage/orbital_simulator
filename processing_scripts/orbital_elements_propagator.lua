-- INFO
--
-- !!! do not use to produce results !!!
--
-- using the orbital elements produced by this (i.e. from sgp4 position->orbital elements)
-- results in less accurate positions being calculated by the propagator

-- get orbital position
t=tle.parse('../data/tle/ISS (ZARYA)')
t=t[2]
work=sgp4_work.new()
sgp4.init(sgp4.WGS72, t, work)
r,v=sgp4.single(t, work, 0)
printf("r x % 12.6f, y % 12.6f, z % 12.6f\n",r.x,r.y,r.z)
printf("v x % 12.6f, y % 12.6f, z % 12.6f\n",v.x,v.y,v.z)
printf("tle: incl %f, raan %f, ecc %f, mean_motion %f\n",t.incl, t.raan, t.ecc, t.mean_motion)
printf("     perigee %f\n", t.perigee)

function magnitude(r)
    return math.sqrt(r.x^2 +r.y^2 +r.z^2)
end

function cross(a,b)
    local r={}
    r.x=a.y*b.z - a.z*b.y
    r.y=a.z*b.x - a.x*b.z
    r.z=a.x*b.y - a.y*b.x
    return r
end

function dot(a,b)
    return (a.x*b.x + a.y*b.y + a.z*b.z)
end

function r1(r, a)
    local cos=math.cos(a)
    local sin=math.sin(a)
    local v={}
    v.x=r.x
    v.y=cos*r.y + sin*r.z
    v.z=-sin*r.y + cos*r.z
    return v
end
function r3(r, a)
    local cos=math.cos(a)
    local sin=math.sin(a)
    local v={}
    v.x=cos*r.x + sin*r.y
    v.y=-sin*r.x + cos*r.y
    v.z=r.z
    return v
end

-- magnitudes
r_mag=magnitude(r)
v_mag=magnitude(v)

-- h
h=cross(r,v)
h_mag=magnitude(h)

-- incl
i=math.acos(h.z/h_mag)
printf("incl %f (%f)\n", i, i*180/math.pi)

-- raan
z={}
z.x=0
z.y=0
z.z=1
s=cross(z,h)
s_mag=magnitude(s)
raan=math.acos(s.x/s_mag)
printf("raan %f (%f)\n", raan, raan*180/math.pi)

-- a
a=1/((2/r_mag) - (v_mag^2/398600.8))

-- ecc
ecc = math.sqrt(1-(h_mag^2/(398600.8*a)))
printf("ecc %f\n", ecc)

-- mean_motion
mean_motion=math.sqrt(398600.8/a^3)
printf("mean_motion %f (%f)\n", mean_motion, mean_motion*60)

-- perigee
p=r3(r, raan)
p=r1(p, i)
cos_w_f=p.x/r_mag

p_dot=r3(v, raan)
p_dot=r1(p_dot, i)
perigee=p_dot.y*(math.sqrt(1-ecc^2)/(mean_motion*a))
perigee=math.acos((perigee-cos_w_f)/ecc)
printf("perigee %f\n", perigee)

-- mean_anom
f=math.acos(cos_w_f)-perigee
E=math.acos((r_mag*math.cos(f)/a)+ecc)
mean_anom=E-ecc*math.sin(E)
E=nil

-- last minute conversions
mean_motion=mean_motion*60

-- calculate orbit
file=io.open('iss_orb.dat','w')
for time=0,92,1 do
    -- calculate tp
    --tp=-(t.mean_anom/t.mean_motion) -- tp= t_0 -M/n
    -- calculate mean anomaly
    M=mean_anom+mean_motion*time -- M=M_0 +n*t assumed that M_0 is at t=0
    -- wrap around large M
    while M>math.pi*2 do
        M=M-math.pi*2
    end

    -- calculate eccentric anomaly
    E=M
    E_old=1
    for i=0,10,1 do
        E=M+ecc*math.sin(E)
        if (math.abs((E_old/E)-1))<1e-14 then
            break
        end
        E_old=E
    end
    E_old=nil -- remove iterative variable

    -- calculate q
    q={}
    q.x=a*(math.cos(E)-ecc)
    q.y=a*(math.sqrt(1-ecc*ecc)*math.sin(E))
    q.z=0

    -- rotate q to ECI
    p=r3(q, -t.perigee)
    p=r1(p, -t.incl)
    p=r3(p, -t.raan)

    fprintf(file, "%16.8f %16.8f %16.8f\n", p.x, p.y, p.z)
end
file:close()

