-- find arguments
for i in pairs(args) do
    if args[i]:lower() == '-s' then
        step=args[i+1]
    end
    if args[i]:lower() == '-d' then
        out_fol=args[i+1]
    end
end

if not step or not out_fol then
    print('Error - failed to find arguments')
    os.exit(-1)
end

-- load earth orientation data
eop_data={}
eop_data[2015]=eop.file_read('data/eop/eopc04_08.15')
eop_data[2016]=eop.file_read('data/eop/eopc04_08.16')

-- load atmospheric data
ap_2015=ap.file_read('data/ap/2015')
ap_2016=ap.file_read('data/ap/2016')
fp_2015=f107.file_read('data/f10.7/drao_noontime-flux-observed_2015.txt')
fp_2016=f107.file_read('data/f10.7/drao_noontime-flux-observed_2016.txt')
atmos=nrlmsise.init({ap_2015, ap_2016}, {fp_2015, fp_2016}, {2015, 2016}, {#ap_2015, #ap_2016})


function deg2rad(d)
    return d*(math.pi/180)
end

-- create satellite object
--
-- satellite orientation - positive z axis is ram direction
-- rotations - pitch around x
--             yaw   around y
--             roll  around z
--
--        y                  ----------------
--        |                 /               /
--        |                /               /|
--        |               /               / /
--        0------ x      /               / /
--       /              ----------------- /
--      /               |               |/
--     /                -----------------
--    z
--
sat_faces={{area=10e-2*2.5e-2, x=0,  y=0,  z=1},  -- front
           {area=10e-2*2.5e-2, x=0,  y=0,  z=-1}, -- back
           {area=10e-2*2.5e-2, x=1,  y=0,  z=0},  -- left
           {area=10e-2*2.5e-2, x=-1,  y=0,  z=0}, -- right
           {area=10e-2*10e-2,  x=0,  y=1,  z=0},  -- up
           {area=10e-2*10e-2,  x=0,  y=-1,  z=0}} -- down
sat_orientation={yaw=0, pitch=0, roll=0}
--sat_orientation.yaw=deg2rad(90)
sat_mass=250e-3
sat=sat_obj.init(sat_faces, sat_orientation, sat_mass)

print(sat)

-- circ 15 500
p={x=6878.084778, y=-24.582284, z=-10.681252}
v={x=0.029340,    y=7.353069,   z=1.970612}

-- circ 15 100
--p={x=6478.087815, y=-23.152689, z=-10.060081}
--v={x=0.030232,    y=7.576681,   z=2.030540}

d=1
h=0
s=0

filename=out_fol..'step_'..step

file=io.open(filename, 'w')

t_before=os.time()

for i=0, 24*7*60, 1 do
    -- run integration
    --p, v=orb_int.run(p, v, 60, step)
    p, v, d, h, s=orb_int.drag_run(p, v, 60, step, eop_data, atmos, sat, 2016, d, h, s)

    fprintf(file, '%3d %2d %8.3f % 21.15f % 21.15f % 21.15f % 21.15f % 21.15f % 21.15f\n',
                    d, h, s, p.x, p.y, p.z, v.x, v.y, v.z)

    file:flush()
end

t_after=os.time()

file:close()

printf("time for step %s is %d\n", step, os.difftime(t_after, t_before))
