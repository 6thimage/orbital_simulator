-- This script will output the differences between the tles at epoch
-- and the kepler propagator, to compare the accuracy of the kepler
-- propagator to SGP4.

--[
cubesats={'AAU CUBESAT','AAUSAT-II','AAUSAT3','AUBIESAT-1','BEESAT',
          'BEESAT-2','BEESAT-3','BELL','BRITE-AUSTRIA','CANX-1',
          'CANX-2','CAPE1','CLOUDSAT','COMPASS-1','CSTB1','CUBEBUG 1',
          'CUBESAT XI-IV (CO-57)','CUBESAT XI-V (CO-58)',
          'CUTE-1 (CO-55)','CUTE-1.7+APD II (CO-65)','DELFI-C3 (DO-64)',
          'DICE-F','DICE-Y','DOVE-2','DTUSAT','E-ST@R','ESTCUBE 1',
          'F-1','FITSAT-1 (NIWAKA)','GOLIAT','ITUPSAT 1','LIBERTAD-1',
          'M-CUBED & EXP-1 PRIME','MASAT 1','MAST','NCUBE-2',
          'NEE-01 PEGASUS','NTS (CANX-6)','OSSI-1','POLYSAT CP3',
          'POLYSAT CP4','PW-SAT','QUAKESAT','RAIKO','RAX-2','ROBUSTA',
          'SEEDS II (CO-66)','SOMP','STRAND 1','SWISSCUBE','TECHEDSAT',
          'TISAT 1','TURKSAT-3USAT','UNIBRITE','UNICUBESAT GG','UWE-1',
          'UWE-2','XATCOBEO'}--]
--cubesats={'AAU CUBESAT', 'AAUSAT3', 'BEESAT', 'CANX-1', 'CANX-2', 'STRAND 1'}

kepler_lua=require('kepler_lua')

-- open output
file=io.open('plots/kepler orbit differences/kep_oribt_diffs.dat','w')

-- loop through satellites
print(#cubesats)
for _,satellite in pairs(cubesats) do
    -- write name
    fprintf(file,'"%s" \n', satellite)

    -- load tles
    t=tle.parse('data/tle/'..satellite)
    --num_tles=#t
    num_tles=1

    -- setup work
    work=sgp4_work.new()

    -- loop through tles
    for i=1,num_tles,1 do
        -- tle
        sgp4.init(sgp4.WGS72, t[i], work)

        -- loop through times
        for time=0,10,1 do
            r1,v1=sgp4.single(t[i], work, time)
            if type(r1)~="table" then
                printf("sgp4 failed with '%s' for %s (tle %d)\n",
                       v1, satellite, i)
            else
                -- kepler
                r2,v2=kepler_lua.single(t[i], time)
                
                -- print difference
                fprintf(file, "%3d %12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n",
                        i, time,
                        r1.x, r1.y, r1.z,
                        r2.x, r2.y, r2.z)
                --fprintf(file, " % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e\n",
                --        (r1.x/r2.x)-1, (r1.y/r2.y)-1, (r1.z/r2.z)-1,
                --        (v1.x/v2.x)-1, (v1.y/v2.y)-1, (v1.z/v2.z)-1)
            end
        end
        fprintf(file, "\n")
    end
    fprintf(file, "\n\n")
end
file:close()
