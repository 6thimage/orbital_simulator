-- This script will output a data file to be plotted, with gnuplot,
-- that shows the differences between each new tle and the previous
-- tle at the new epoch - i.e. the difference between the positions
-- and velocities that caused NORAD to update them.

cubesats={'AAU CUBESAT','AAUSAT-II','AAUSAT3','AUBIESAT-1','BEESAT',
          'BEESAT-2','BEESAT-3','BELL','BRITE-AUSTRIA','CANX-1',
          'CANX-2','CAPE1','CLOUDSAT','COMPASS-1','CSTB1','CUBEBUG 1',
          'CUBESAT XI-IV (CO-57)','CUBESAT XI-V (CO-58)',
          'CUTE-1 (CO-55)','CUTE-1.7+APD II (CO-65)','DELFI-C3 (DO-64)',
          'DICE-F','DICE-Y','DOVE-2','DTUSAT','E-ST@R','ESTCUBE 1',
          'F-1','FITSAT-1 (NIWAKA)','GOLIAT','ITUPSAT 1','LIBERTAD-1',
          'M-CUBED & EXP-1 PRIME','MASAT 1','MAST','NCUBE-2',
          'NEE-01 PEGASUS','NTS (CANX-6)','OSSI-1','POLYSAT CP3',
          'POLYSAT CP4','PW-SAT','QUAKESAT','RAIKO','RAX-2','ROBUSTA',
          'SEEDS II (CO-66)','SOMP','STRAND 1','SWISSCUBE','TECHEDSAT',
          'TISAT 1','TURKSAT-3USAT','UNIBRITE','UNICUBESAT GG','UWE-1',
          'UWE-2','XATCOBEO'}

-- open output
file=io.open('cubesat_tle_diffs.dat','w')

-- loop through satellites
print(#cubesats)
for _,satellite in pairs(cubesats) do
    -- write name
    fprintf(file,'"%s" \n', satellite)

    -- load tles
    t=tle.parse('data/tle/'..satellite)
    num_tles=#t

    -- setup work
    work=sgp4_work.new()

    -- loop through tles
    for i=1,num_tles-1,1 do
        epoch_diff=(jdate_to_unix(t[i+1].epoch)-jdate_to_unix(t[i].epoch))/60
        -- current tle
        sgp4.init(sgp4.WGS72, t[i], work)
        r1,v1=sgp4.single(t[i], work, epoch_diff)
        -- next tle
        sgp4.init(sgp4.WGS72, t[i+1], work)
        r2,v2=sgp4.single(t[i+1], work, 0)

        -- print difference
        fprintf(file, "%3d %12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f",
                i, epoch_diff,
                r2.x-r1.x, r2.y-r1.y, r2.z-r1.z,
                v2.x-v1.x, v2.y-v1.y, v2.z-v1.z)
        fprintf(file, " % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e\n",
                (r2.x/r1.x)-1, (r2.y/r1.y)-1, (r2.z/r1.z)-1,
                (v2.x/v1.x)-1, (v2.y/v1.y)-1, (v2.z/v1.z)-1)
    end
    fprintf(file, "\n\n")
end
file:close()
