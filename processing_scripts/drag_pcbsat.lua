-- find arguments
for i in pairs(args) do
    if args[i]:lower() == '-c' then
        config=args[i+1]
    end
    if args[i]:lower() == '-d' then
        out_fol=args[i+1]
    end
end

if not config or not out_fol then
    print('Error - failed to find arguments')
    os.exit(-1)
end

if tonumber(config)<0 or tonumber(config)>244 then
    print('Error - config must be between 0 and 244')
    os.exit(-1)
end

-- load earth orientation data
eop_data={}
eop_data[2015]=eop.file_read('data/eop/eopc04_08.15')
eop_data[2016]=eop.file_read('data/eop/eopc04_08.16')

-- load atmospheric data
ap_2015=ap.file_read('data/ap/2015')
ap_2016=ap.file_read('data/ap/2016')
fp_2015=f107.file_read('data/f10.7/drao_noontime-flux-observed_2015.txt')
fp_2016=f107.file_read('data/f10.7/drao_noontime-flux-observed_2016.txt')
atmos=nrlmsise.init({ap_2015, ap_2016}, {fp_2015, fp_2016}, {2015, 2016}, {#ap_2015, #ap_2016})


function deg2rad(d)
    return d*(math.pi/180)
end

-- circ 15 500
p500={x=6878.084778, y=-24.582284, z=-10.681252}
v500={x=0.029340,    y=7.353069,   z=1.970612}

-- circ 15 400
p400={x=6778.085537, y=-24.224886, z=-10.525959}
v400={x=0.029556,    y=7.407115,   z=1.985096}

-- circ 15 300
p300={x=6678.086296, y=-23.867489, z=-10.370666}
v300={x=0.029776,    y=7.462364,   z=1.999903}

-- circ 15 200
p200={x=6578.087056, y=-23.510091, z=-10.215374}
v200={x=0.030002,    y=7.518871,   z=2.015047}

-- circ 15 100
p100={x=6478.087815, y=-23.152689, z=-10.060081}
v100={x=0.030232,    y=7.576681,   z=2.030540}

-- set configuration - range 0->244
ypr_config=config%49
orbit_config=math.floor(config/49)

roll=0
-- pitch
p_config=ypr_config%7
if p_config==0 then
    pitch=0
elseif p_config==1 then
    pitch=15
elseif p_config==2 then
    pitch=30
elseif p_config==3 then
    pitch=45
elseif p_config==4 then
    pitch=60
elseif p_config==5 then
    pitch=75
elseif p_config==6 then
    pitch=90
end

-- yaw
y_config=math.floor(ypr_config/7)%7
if y_config==0 then
    yaw=0
elseif y_config==1 then
    yaw=15
elseif y_config==2 then
    yaw=30
elseif y_config==3 then
    yaw=45
elseif y_config==4 then
    yaw=60
elseif y_config==5 then
    yaw=75
elseif y_config==6 then
    yaw=90
end

-- orbit
if orbit_config==0 then
    orbit=100
    p, v = p100, v100
elseif orbit_config==1 then
    orbit=200
    p, v = p200, v200
elseif orbit_config==2 then
    orbit=300
    p, v = p300, v300
elseif orbit_config==3 then
    orbit=400
    p, v = p400, v400
elseif orbit_config==4 then
    orbit=500
    p, v = p500, v500
end

-- create satellite object
--
-- satellite orientation - positive z axis is ram direction
-- rotations - pitch around x
--             yaw   around y
--             roll  around z
--
--        y                  ----------------
--        |                 /               /
--        |                /               /|
--        |               /               / /
--        0------ x      /               / /
--       /              ----------------- /
--      /               |               |/
--     /                -----------------
--    z
--
sat_faces={{area=10e-2*2.5e-2, x=0,  y=0,  z=1},  -- front
           {area=10e-2*2.5e-2, x=0,  y=0,  z=-1}, -- back
           {area=10e-2*2.5e-2, x=1,  y=0,  z=0},  -- left
           {area=10e-2*2.5e-2, x=-1, y=0,  z=0},  -- right
           {area=10e-2*10e-2,  x=0,  y=1,  z=0},  -- up
           {area=10e-2*10e-2,  x=0,  y=-1, z=0}}  -- down
sat_mass=250e-3
sat_orientation={yaw=deg2rad(yaw), pitch=deg2rad(pitch), roll=deg2rad(roll)}
sat=sat_obj.init(sat_faces, sat_orientation, sat_mass)

-- rk4
--   no drag - 0.1
--   drag - 0.1 (might need to be lower)
step=0.1

print('yaw', yaw)
print('pitch', pitch)
print('roll', roll)
print('orbit', orbit)
print('step', step)
print(sat)

d=1
h=0
s=0

filename=out_fol..'orbit'..orbit..'_y'..yaw..'_p'..pitch..'_r'..roll

file=io.open(filename, 'w')

t_before=os.time()

for i=0, #ap_2016*5*24*60, 1 do
    -- run integration
    --p, v=orb_int.run(p, v, 60, step)
    p, v, d, h, s=orb_int.drag_run(p, v, 60, step, eop_data, atmos, sat, 2016, d, h, s)

    fprintf(file, '%3d %2d %8.3f % 21.15f % 21.15f % 21.15f % 21.15f % 21.15f % 21.15f\n',
                    d, h, s, p.x, p.y, p.z, v.x, v.y, v.z)

    file:flush()

    if d>#ap_2016 then
        break
    end

    -- altitude check
    r=math.sqrt(p.x^2 + p.y^2 + p.z^2)
    alt=(r-6378.135)*1e3
    if alt<0 then
        fprintf(file, "# negative altitude - %f m\n", alt)
        break
    end
end

t_after=os.time()

file:close()

printf("time: %d\n", os.difftime(t_after, t_before))
