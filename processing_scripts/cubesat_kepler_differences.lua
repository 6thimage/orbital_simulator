-- This script will output the differences between the tles at epoch
-- and after certain periods to compare the accuracy of the kepler
-- propagator to SGP4.

cubesats={'AAU CUBESAT','AAUSAT-II','AAUSAT3','AUBIESAT-1','BEESAT',
          'BEESAT-2','BEESAT-3','BELL','BRITE-AUSTRIA','CANX-1',
          'CANX-2','CAPE1','CLOUDSAT','COMPASS-1','CSTB1','CUBEBUG 1',
          'CUBESAT XI-IV (CO-57)','CUBESAT XI-V (CO-58)',
          'CUTE-1 (CO-55)','CUTE-1.7+APD II (CO-65)','DELFI-C3 (DO-64)',
          'DICE-F','DICE-Y','DOVE-2','DTUSAT','E-ST@R','ESTCUBE 1',
          'F-1','FITSAT-1 (NIWAKA)','GOLIAT','ITUPSAT 1','LIBERTAD-1',
          'M-CUBED & EXP-1 PRIME','MASAT 1','MAST','NCUBE-2',
          'NEE-01 PEGASUS','NTS (CANX-6)','OSSI-1','POLYSAT CP3',
          'POLYSAT CP4','PW-SAT','QUAKESAT','RAIKO','RAX-2','ROBUSTA',
          'SEEDS II (CO-66)','SOMP','STRAND 1','SWISSCUBE','TECHEDSAT',
          'TISAT 1','TURKSAT-3USAT','UNIBRITE','UNICUBESAT GG','UWE-1',
          'UWE-2','XATCOBEO'}

periods={.2,.5,1,2,3,4,5,6,7,8,9,10}

kepler_lua=require('kepler_lua')

function radius(p)
    local r=p.x*p.x +p.y*p.y +p.z*p.z
    return math.sqrt(r)
end

function period(r)
    local t=r*1000 -- r in m
    t=t^3 -- r^3
    t=t*4*math.pi^2 -- 4*pi^2*r^3
    t=t/(6.67e-11*5.972e24) -- (4(pi^2*r^3)/GM
    return math.sqrt(t)/60 --return in minutes
end

-- open output
file=io.open('cubesat_kep_diffs.dat','w')

-- loop through satellites
print(#cubesats)
for _,satellite in pairs(cubesats) do
    -- write name
    fprintf(file,'"%s" \n', satellite)

    -- load tles
    t=tle.parse('data/tle/'..satellite)
    num_tles=#t

    -- setup work
    work=sgp4_work.new()

    -- loop through tles
    --for i=1,num_tles,1 do
    for i=1,1,1 do
        -- tle
        sgp4.init(sgp4.WGS72, t[i], work)
        r1,v1=sgp4.single(t[i], work, 0)
        --[[--  kepler
        r2,v2=kepler.single(t[i], 0)

        -- print difference
        fprintf(file, "%3d     0.000000 % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f",
                i, r2.x-r1.x, r2.y-r1.y, r2.z-r1.z,
                   v2.x-v1.x, v2.y-v1.y, v2.z-v1.z)
        fprintf(file, " % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e\n",
                (r2.x/r1.x)-1, (r2.y/r1.y)-1, (r2.z/r1.z)-1,
                (v2.x/v1.x)-1, (v2.y/v1.y)-1, (v2.z/v1.z)-1)--]]

        -- calculate period from SGP4 epoch 0
        sat_period=period(radius(r1))
        --for __,num_periods in pairs(periods) do
        for mins=0,600,.1 do
            -- tle
            r1,v1=sgp4.single(t[i], work, mins)-- num_periods*sat_period)
            if type(r1)~="table" then
                printf("sgp4 failed with '%s' for %s (tle %d, period %.1f)\n",
                       v1, satellite, i, mins)--num_periods)
            else
                -- kepler
                r2,v2=kepler_lua.single(t[i], mins)--num_periods*sat_period)

                -- print difference
                fprintf(file, "%3d %12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f",
                        i, mins/sat_period,--num_periods*sat_period,
                        r2.x-r1.x, r2.y-r1.y, r2.z-r1.z,
                        v2.x-v1.x, v2.y-v1.y, v2.z-v1.z)
                fprintf(file, " % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e % 16.6e\n",
                        (r2.x/r1.x)-1, (r2.y/r1.y)-1, (r2.z/r1.z)-1,
                        (v2.x/v1.x)-1, (v2.y/v1.y)-1, (v2.z/v1.z)-1)
            end

        end
        fprintf(file, "\n")
    end
    fprintf(file, "\n")
end
file:close()
