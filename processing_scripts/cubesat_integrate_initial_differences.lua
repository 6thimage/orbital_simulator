-- find arguments
for i in pairs(args) do
    if args[i]:lower() == '-t' then
        tle_num=args[i+1]
    end
    if args[i]:lower() == '-d' then
        out_fol=args[i+1]
    end
end
if not tle_num or not out_fol then
    print('ERROR - failed to find arguments')
    os.exit(-1)
end

cubesats={'AAU CUBESAT','AAUSAT-II','AAUSAT3','AUBIESAT-1','BEESAT',
          'BEESAT-2','BEESAT-3','BELL','BRITE-AUSTRIA','CANX-1',
          'CANX-2','CAPE1','CLOUDSAT','COMPASS-1','CSTB1','CUBEBUG-1 (CAPITAN BETO)',
          'CUBESAT XI-IV (CO-57)','CUBESAT XI-V (CO-58)',
          'CUTE-1 (CO-55)','CUTE-1.7+APD II (CO-65)','DELFI-C3 (DO-64)',
          'DICE-F','DICE-Y','DOVE-2','DTUSAT','E-ST@R','ESTCUBE 1',
          'F-1','FITSAT-1 (NIWAKA)','GOLIAT','ITUPSAT 1','LIBERTAD-1',
          'M-CUBED & EXP-1 PRIME','MASAT 1 (MO-72)','MAST','NCUBE-2',
          'NEE-01 PEGASUS','NTS (CANX-6)','OSSI-1','POLYSAT CP3',
          'POLYSAT CP4','PW-SAT','QUAKESAT','RAIKO','RAX-2','ROBUSTA',
          'SEEDS II (CO-66)','SOMP','STRAND-1','SWISSCUBE','TECHEDSAT-3P',
          'TISAT 1','TURKSAT-3USAT','UNIBRITE','UNICUBESAT GG','UWE-1',
          'UWE-2','XATCOBEO'}

sat_name=cubesats[tonumber(tle_num)]
if not sat_name then
    print('ERROR - failed to find satellite name')
    os.exit(-1)
end

file_name=out_fol..sat_name
file=io.open(file_name, 'w')
if not file then
    print('ERROR - failed to open file ('..file_name..')')
    os.exit(-1)
end

print(sat_name)
print(file_name)

step=.00001

t=tle.parse('data/tle/'..sat_name)
print(#t..' TLEs')

work=sgp4_work.new()

for i=2, #t, 1 do
    epoch_diff=(jdate_to_unix(t[i].epoch)-jdate_to_unix(t[i-1].epoch))/60

    sgp4.init(sgp4.WGS72, t[i-1], work)
    -- first position for integrator
    p, v=sgp4.single(t[i-1], work, 0)
    -- sgp4 first tle
    sp1, sv1=sgp4.single(t[i-1], work, epoch_diff)

    -- sgp4 second tle
    sgp4.init(sgp4.WGS72, t[i], work)
    sp2, sv2=sgp4.single(t[i], work, 0)

    -- orbit integrator
    p, v=orb_int.run(p, v, epoch_diff*60, step)

    fprintf(file, '%7d %12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f % 12.6f\n',
                    i, epoch_diff, sp1.x, sp1.y, sp1.z, p.x, p.y, p.z, sp2.x, sp2.y, sp2.z)
end

file:close()

