reset

#set term png size 1200,800 font times
#set output 'kep_init_diffs_km.png'

set multiplot layout 1,2 rowsfirst

#set log xy
unset key

set grid
set size square
#set xtics 1
#set ytics 1
#set mxtics 2
#set mytics 2

# position
# x-y
#set term wxt 0
set xlabel 'x (km)'
set ylabel 'y (km)'
plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 3:4 title columnheader(1)
# z-y
#set term wxt 1
set xlabel 'z (km)'
plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 5:4 title columnheader(1)

## velocity
## x-y
#set xlabel 'x (km/s)'
#set ylabel 'y (km/s)'
##set yrange[-.0085:.0085]
#plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 6:7 title columnheader(1)
## z-y
#set xlabel 'z (km/s)'
#plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 8:7 title columnheader(1)

unset multiplot
