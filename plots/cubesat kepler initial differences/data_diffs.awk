BEGIN{
    # mins
    px_min=1e100;
    py_min=1e100;
    pz_min=1e100;
    vx_min=1e100;
    vy_min=1e100;
    vz_min=1e100;
    # maxs
    px_max=1e-100;
    py_max=1e-100;
    pz_max=1e-100;
    vx_max=1e-100;
    vy_max=1e-100;
    vz_max=1e-100;
}

{
    if(NF==14)
    {
        # averages
        total+=1;
        px_avg+=$9;
        py_avg+=$10;
        pz_avg+=$11;
        vx_avg+=$12;
        vy_avg+=$13;
        vz_avg+=$14;

        # mins
        if($9<px_min) px_min=$9;
        if($10<py_min) py_min=$10;
        if($11<pz_min) pz_min=$11;
        if($12<vx_min) vx_min=$12;
        if($13<vy_min) vy_min=$13;
        if($14<vz_min) vz_min=$14;

        # maxs
        if($9>px_max) px_max=$9;
        if($10>py_max) py_max=$10;
        if($11>pz_max) pz_max=$11;
        if($12>vx_max) vx_max=$12;
        if($13>vy_max) vy_max=$13;
        if($14>vz_max) vz_max=$14;
    }
}

END{
    printf "\tmin\t\tavg\t\tmax\n";
    printf "px\t% le\t% le\t% le\n", px_min, px_avg/total, px_max;
    printf "py\t% le\t% le\t% le\n", py_min, py_avg/total, py_max;
    printf "pz\t% le\t% le\t% le\n", pz_min, pz_avg/total, pz_max;
    printf "vx\t% le\t% le\t% le\n", vx_min, vx_avg/total, vx_max;
    printf "vy\t% le\t% le\t% le\n", vy_min, vy_avg/total, vy_max;
    printf "vz\t% le\t% le\t% le\n", vz_min, vz_avg/total, vz_max;
}
