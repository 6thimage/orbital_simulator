reset

set multiplot layout 2,2 rowsfirst

set log xy
unset key

# position
# x-y
set xlabel 'x'
set ylabel 'y'
plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 9:10 title columnheader(1)
# y-z
set xlabel 'z'
plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 11:10 title columnheader(1)

# velocity
# x-y
set xlabel 'x'
set ylabel 'y'
plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 12:13 title columnheader(1)
# y-z
set xlabel 'z'
plot for [ID=0:57] './cubesat_kep_init_diffs.dat' index ID using 14:13 title columnheader(1)

unset multiplot