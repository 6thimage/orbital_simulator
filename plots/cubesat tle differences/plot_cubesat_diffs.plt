reset
unset key

set multiplot layout 2,2 rowsfirst

set title 'p xy'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using 3:4 w points title columnheader(1)

set title 'p yz'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using 4:5 w points title columnheader(1)

set title 'v xy'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using 6:7 w points title columnheader(1)

set title 'v yz'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using 7:8 w points title columnheader(1)

unset multiplot
