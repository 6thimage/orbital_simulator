reset
unset key
set log xy

set multiplot layout 2,2 rowsfirst

set title 'p xy'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using (abs($9)):(abs($10)) w points title columnheader(1)

set title 'p yz'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using (abs($10)):(abs($11)) w points title columnheader(1)

set title 'v xy'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using (abs($12)):(abs($13)) w points title columnheader(1)

set title 'v yz'
plot for [ID=0:57] './cubesat_tle_diffs.dat' index ID using (abs($13)):(abs($14)) w points title columnheader(1)

unset multiplot
