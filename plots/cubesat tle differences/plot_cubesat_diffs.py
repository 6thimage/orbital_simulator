#! /usr/bin/env python

import pylab as pl
import numpy as np
from matplotlib.ticker import LogLocator
from scipy import interpolate

# load data
data_o=np.genfromtxt('cubesat_tle_diffs.dat.reduced')

for plot_num in range(1,4):
    # only the column we want in percent
    data=data_o[:,9+plot_num]*100
    data=np.absolute(data)
    pl.subplot(2,2,plot_num)
    if plot_num==1:
        pl.title('x')
    elif plot_num==2:
        pl.title('y')
    else:
        pl.title('z')
    # plot the histogram - bins and data is multiplied by a scale factor
    # so that the labels are in the middle of the bins
    scale=3.16228 # log10(scale)=0.5
    bins=np.array([1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1e0,1e1,1e2,1e3,1e4,1e5,1e6])
    pl.hist(data/scale, log=True, bins=bins*scale, color='.85')
    pl.gca().set_xscale('log')
    pl.gca().get_xaxis().set_minor_locator(LogLocator(10))
    pl.xlim(1e-6,1e5)
    
    # plot line
    y,x=np.histogram(data,bins)
    # cummulative
    c=0
    y.resize(13)
    for i in range(0,13):
        c=c+y[i]
        y[i]=c
    # smooth
    xs=np.logspace(-5,4,100)
    ys=interpolate.UnivariateSpline(x,y)
    pl.plot(xs,ys(xs),'--')
    
    # labels
    pl.gca().set_xlabel('Error in positions (%)')
    pl.gca().set_ylabel('Number (out of '+str(len(data))+')') 

# plot
#mng=pl.get_current_fig_manager()
#mng.resize(*mng.window.maxsize())
pl.show()
